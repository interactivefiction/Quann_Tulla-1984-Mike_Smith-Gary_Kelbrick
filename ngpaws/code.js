// This file is (C) Carlos Sanchez 2014, released under the MIT license


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// GLOBAL VARIABLES AND CONSTANTS ///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// CONSTANTS 
var VOCABULARY_ID = 0;
var VOCABULARY_WORD = 1;
var VOCABULARY_TYPE = 2;

var WORDTYPE_VERB = 0;
var WORDTYPE_NOUN = 1
var WORDTYPE_ADJECT = 2;
var WORDTYPE_ADVERB = 3;
var WORDTYPE_PRONOUN = 4;
var WORDTYPE_CONJUNCTION = 5;
var WORDTYPE_PREPOSITION = 6;

var TIMER_MILLISECONDS  = 40;

var RESOURCE_TYPE_IMG = 1;
var RESOURCE_TYPE_SND = 2;

var PROCESS_RESPONSE = 0;
var PROCESS_DESCRIPTION = 1;
var PROCESS_TURN = 2;

var DIV_TEXT_SCROLL_STEP = 40;


// Aux
var SET_VALUE = 255; // Value assigned by SET condact
var EMPTY_WORD = 255; // Value for word types when no match is found (as for  sentences without adjective or name)
var MAX_WORD_LENGHT = 10;  // Number of characters considered per word
var FLAG_COUNT = 256;  // Number of flags
var NUM_CONNECTION_VERBS = 14; // Number of verbs used as connection, from 0 to N - 1
var NUM_CONVERTIBLE_NOUNS = 20;
var NUM_PROPER_NOUNS = 50; // Number of proper nouns, can't be used as pronoun reference
var EMPTY_OBJECT = 255; // To remark there is no object when the action requires a objno parameter
var NO_EXIT = 255;  // If an exit does not exist, its value is this value
var MAX_CHANNELS = 17; // Number of SFX channels
var RESOURCES_DIR='dat/';


//Attributes
var ATTR_LIGHT=0;			// Object produces light
var ATTR_WEARABLE=1;		// Object is wearable
var ATTR_CONTAINER=2;       // Object is a container
var ATTR_NPC=3;             // Object is actually an NPC
var ATTR_CONCEALED = 4; /// Present but not visible
var ATTR_EDIBLE = 5;   /// Can be eaten
var ATTR_DRINKABLE=6;
var ATTR_ENTERABLE = 7;
var ATTR_FEMALE = 8;
var ATTR_LOCKABLE = 9;
var ATTR_LOCKED = 10;
var ATTR_MALE = 11;
var ATTR_NEUTER=12;
var ATTR_OPENABLE =13;
var ATTR_OPEN=14;
var ATTR_PLURALNAME = 15;
var ATTR_TRANSPARENT=16;
var ATTR_SCENERY=17;
var ATTR_SUPPORTER = 18;
var ATTR_SWITCHABLE=19;
var ATTR_ON  =20;
var ATTR_STATIC  =21;



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// INTERNAL STRINGS ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General messages & strings
var STR_NEWLINE = '<br />';
var STR_PROMPT_START = '<span class="feedback">&gt; ';
var STR_PROMPT_END = '</span>';
var STR_RAMSAVE_FILENAME = 'RAMSAVE_SAVEGAME';



// Runtime error messages
var STR_WRONG_SYSMESS = 'WARNING: System message requested does not exist.'; 
var STR_WRONG_LOCATION = 'WARNING: Location requested does not exist.'; 
var STR_WRONG_MESSAGE = 'WARNING: Message requested does not exist.'; 
var STR_WRONG_PROCESS = 'WARNING: Process requested does not exist.' 
var STR_RAMLOAD_ERROR= 'WARNING: You can\'t restore game as it has not yet been saved.'; 
var STR_RUNTIME_VERSION  = 'ngPAWS runtime (C) 2014 Carlos Sanchez.  Released under {URL|http://www.opensource.org/licenses/MIT| MIT license}.\nBuzz sound libray (C) Jay Salvat. Released under the {URL|http://www.opensource.org/licenses/MIT| MIT license} \n jQuery (C) jQuery Foundation. Released under the {URL|https://jquery.org/license/| MIT license}.';
var STR_TRANSCRIPT = 'To copy the transcript to your clipboard, press Ctrl+C, then press Enter';

var STR_INVALID_TAG_SEQUENCE = 'Invalid tag sequence: ';
var STR_INVALID_TAG_SEQUENCE_EMPTY = 'Invalid tag sequence.';
var STR_INVALID_TAG_SEQUENCE_BADPARAMS = 'Invalid tag sequence: bad parameters.';
var STR_INVALID_TAG_SEQUENCE_BADTAG = 'Invalid tag sequence: unknown tag.';
var STR_BADIE = 'You are using a very old version of Internet Explorer. Some features of this product won\'t be avaliable, and other may not work properly. For a better experience please upgrade your browser or install some other one like Firefox, Chrome or Opera.\n\nIt\'s up to you to continue but be warned your experience may be affected.';
var STR_INVALID_OBJECT = 'WARNING: Trying to access object that does not exist'


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////     FLAGS     ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


var FLAG_LIGHT = 0;
var FLAG_OBJECTS_CARRIED_COUNT = 1;
var FLAG_AUTODEC2 = 2; 
var FLAG_AUTODEC3 = 3;
var FLAG_AUTODEC4 = 4;
var FLAG_AUTODEC5 = 5;
var FLAG_AUTODEC6 = 6;
var FLAG_AUTODEC7 = 7;
var FLAG_AUTODEC8 = 8;
var FLAG_AUTODEC9 = 9;
var FLAG_AUTODEC10 = 10;
var FLAG_ESCAPE = 11;
var FLAG_PARSER_SETTINGS = 12;
var FLAG_PICTURE_SETTINGS = 29
var FLAG_SCORE = 30;
var FLAG_TURNS_LOW = 31;
var FLAG_TURNS_HIGH = 32;
var FLAG_VERB = 33;
var FLAG_NOUN1 =34;
var FLAG_ADJECT1 = 35;
var FLAG_ADVERB = 36;
var FLAG_MAXOBJECTS_CARRIED = 37;
var FLAG_LOCATION = 38;
var FLAG_TOPLINE = 39;   // deprecated
var FLAG_MODE = 40;  // deprecated
var FLAG_PROTECT = 41;   // deprecated
var FLAG_PROMPT = 42; 
var FLAG_PREP = 43;
var FLAG_NOUN2 = 44;
var FLAG_ADJECT2 = 45;
var FLAG_PRONOUN = 46;
var FLAG_PRONOUN_ADJECT = 47;
var FLAG_TIMEOUT_LENGTH = 48;
var FLAG_TIMEOUT_SETTINGS = 49; 
var FLAG_DOALL_LOC = 50;
var FLAG_REFERRED_OBJECT = 51;
var FLAG_MAXWEIGHT_CARRIED = 52;
var FLAG_OBJECT_LIST_FORMAT = 53;
var FLAG_REFERRED_OBJECT_LOCATION = 54;
var FLAG_REFERRED_OBJECT_WEIGHT = 55;
var FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES = 56;
var FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES = 57;
var FLAG_EXPANSION1 = 58;
var FLAG_EXPANSION2 = 59;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// SPECIAL LOCATIONS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var LOCATION_WORN = 253;
var LOCATION_CARRIED = 254;
var LOCATION_NONCREATED = 252;
var LOCATION_HERE = 255;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////  SYSTEM MESSAGES  ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



var SYSMESS_ISDARK = 0;
var SYSMESS_YOUCANSEE = 1;
var SYSMESS_PROMPT0 = 2;
var SYSMESS_PROMPT1 = 3;
var SYSMESS_PROMPT2 = 4
var SYSMESS_PROMPT3= 5;
var SYSMESS_IDONTUNDERSTAND = 6;
var SYSMESS_WRONGDIRECTION = 7
var SYSMESS_CANTDOTHAT = 8;
var SYSMESS_YOUARECARRYING = 9;
var SYSMESS_WORN = 10;
var SYSMESS_CARRYING_NOTHING = 11;
var SYSMESS_AREYOUSURE = 12;
var SYSMESS_PLAYAGAIN = 13;
var SYSMESS_FAREWELL = 14;
var SYSMESS_OK = 15;
var SYSMESS_PRESSANYKEY = 16;
var SYSMESS_TURNS_START = 17;
var SYSMESS_TURNS_CONTINUE = 18;
var SYSMESS_TURNS_PLURAL = 19;
var SYSMESS_TURNS_END = 20;
var SYSMESS_SCORE_START= 21;
var SYSMESS_SCORE_END =22;
var SYSMESS_YOURENOTWEARINGTHAT = 23;
var SYSMESS_YOUAREALREADYWEARINGTHAT = 24;
var SYSMESS_YOUALREADYHAVEOBJECT = 25;
var SYSMESS_CANTSEETHAT = 26;
var SYSMESS_CANTCARRYANYMORE = 27;
var SYSMESS_YOUDONTHAVETHAT = 28;
var SYSMESS_YOUAREALREADYWAERINGOBJECT = 29;
var SYSMESS_YES = 30;
var SYSMESS_NO = 31;
var SYSMESS_MORE = 32;
var SYSMESS_CARET = 33;
var SYSMESS_TIMEOUT=35;
var SYSMESS_YOUTAKEOBJECT = 36;
var SYSMESS_YOUWEAROBJECT = 37;
var SYSMESS_YOUREMOVEOBJECT = 38;
var SYSMESS_YOUDROPOBJECT = 39;
var SYSMESS_YOUCANTWEAROBJECT = 40;
var SYSMESS_YOUCANTREMOVEOBJECT = 41;
var SYSMESS_CANTREMOVE_TOOMANYOBJECTS = 42;
var SYSMESS_WEIGHSTOOMUCH = 43;
var SYSMESS_YOUPUTOBJECTIN = 44;
var SYSMESS_YOUCANTTAKEOBJECTOUTOF = 45;
var SYSMESS_LISTSEPARATOR = 46;
var SYSMESS_LISTLASTSEPARATOR = 47;
var SYSMESS_LISTEND = 48;
var SYSMESS_YOUDONTHAVEOBJECT = 49;
var SYSMESS_YOUARENOTWEARINGOBJECT = 50;
var SYSMESS_PUTINTAKEOUTTERMINATION = 51;
var SYSMESS_THATISNOTIN = 52;
var SYSMESS_EMPTYOBJECTLIST = 53;
var SYSMESS_FILENOTFOUND = 54;
var SYSMESS_CORRUPTFILE = 55;
var SYSMESS_IOFAILURE = 56;
var SYSMESS_DIRECTORYFULL = 57;
var SYSMESS_LOADFILE = 58;
var SYSMESS_FILENOTFOUND = 59;
var SYSMESS_SAVEFILE = 60;
var SYSMESS_SORRY = 61;
var SYSMESS_NONSENSE_SENTENCE = 62;
var SYSMESS_NPCLISTSTART = 63;
var SYSMESS_NPCLISTCONTINUE = 64;
var SYSMESS_NPCLISTCONTINUE_PLURAL = 65;
var SYSMESS_INSIDE_YOUCANSEE = 66;
var SYSMESS_OVER_YOUCANSEE = 67;
var SYSMESS_YOUPUTOBJECTON = 68;
var SYSMESS_YOUCANTTAKEOBJECTFROM = 69;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// GLOBAL VARS //////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Parser vars
var last_player_orders = [];   // Store last player orders, to be able to restore it when pressing arrow up
var last_player_orders_pointer = 0;
var parser_word_found;
var player_order_buffer = '';
var player_order = ''; // Current player order
var previous_verb = EMPTY_WORD;
var previous_noun = EMPTY_WORD;
var previous_adject = EMPTY_WORD;
var pronoun_suffixes = [];


//Settings
var graphicsON = true; 
var soundsON = true; 
var interruptDisabled = false;
var showWarnings = true;

// waitkey commands callback function
var waitkey_callback_function = [];

//PAUSE
var inPause=false;
var pauseRemainingTime = 0;



// Transcript
var inTranscript = false;
var transcript = '';


// Block
var inBlock = false;
var unblock_process = null;


// END
var inEND = false;

//QUIT
var inQUIT = false;

//ANYKEY
var inAnykey = false;

//GETKEY
var inGetkey = false;
var getkey_return_flag = null;

// Status flags
var done_flag;
var describe_location_flag;
var in_response;
var success;

// doall control
var doall_flag;
var process_in_doall;
var entry_for_doall	= '';
var current_process;


var timeout_progress = 0;
var ramsave_value = null;
var num_objects;


// The flags
var flags = new Array();


// The sound channels
var soundChannels = [];
var soundLoopCount = [];

//The last free object attribute
var nextFreeAttr = 22;

//Autocomplete array
var autocomplete = new Array();
var autocompleteStep = 0;
var autocompleteBaseWord = '';
// PROCESSES

interruptProcessExists = false;

function pro000()
{
process_restart=true;
pro000_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p000e0000:
	{
 		if (skipdoall('p000e0000')) break p000e0000;
 		ACChook(0);
		if (done_flag) break pro000_restart;
		{}

	}

	// _ _
	p000e0001:
	{
 		if (skipdoall('p000e0001')) break p000e0001;
 		ACChook(1);
		if (done_flag) break pro000_restart;
		{}

	}

	// N _
	p000e0002:
	{
 		if (skipdoall('p000e0002')) break p000e0002;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0002;
 		}
		if (!CNDat(87)) break p000e0002;
		if (!CNDpresent(39)) break p000e0002;
 		ACCwriteln(2);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// N _
	p000e0003:
	{
 		if (skipdoall('p000e0003')) break p000e0003;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0003;
 		}
		if (!CNDat(87)) break p000e0003;
		if (!CNDpresent(54)) break p000e0003;
 		ACCwriteln(3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// N _
	p000e0004:
	{
 		if (skipdoall('p000e0004')) break p000e0004;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0004;
 		}
		if (!CNDat(87)) break p000e0004;
		if (!CNDabsent(54)) break p000e0004;
		if (!CNDabsent(39)) break p000e0004;
 		ACCgoto(88);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// N _
	p000e0005:
	{
 		if (skipdoall('p000e0005')) break p000e0005;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0005;
 		}
		if (!CNDat(119)) break p000e0005;
		if (!CNDnotcarr(29)) break p000e0005;
 		ACCwriteln(4);
		{}

	}

	// N _
	p000e0006:
	{
 		if (skipdoall('p000e0006')) break p000e0006;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0006;
 		}
		if (!CNDat(119)) break p000e0006;
		if (!CNDcarried(29)) break p000e0006;
 		ACCgoto(120);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S CIGA
	p000e0007:
	{
 		if (skipdoall('p000e0007')) break p000e0007;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0007;
			if (!CNDnoun1(15)) break p000e0007;
 		}
		if (!CNDcarried(1)) break p000e0007;
 		ACCwriteln(5);
		{}

	}

	// S _
	p000e0008:
	{
 		if (skipdoall('p000e0008')) break p000e0008;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0008;
 		}
		if (!CNDat(61)) break p000e0008;
		if (!CNDeq(24,0)) break p000e0008;
 		ACCwriteln(6);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// S _
	p000e0009:
	{
 		if (skipdoall('p000e0009')) break p000e0009;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0009;
 		}
		if (!CNDat(61)) break p000e0009;
		if (!CNDnotzero(24)) break p000e0009;
 		ACCgoto(62);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0010:
	{
 		if (skipdoall('p000e0010')) break p000e0010;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0010;
 		}
		if (!CNDat(21)) break p000e0010;
		if (!CNDpresent(58)) break p000e0010;
 		ACCwriteln(7);
		{}

	}

	// E _
	p000e0011:
	{
 		if (skipdoall('p000e0011')) break p000e0011;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0011;
 		}
		if (!CNDat(21)) break p000e0011;
		if (!CNDabsent(58)) break p000e0011;
 		ACCgoto(29);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0012:
	{
 		if (skipdoall('p000e0012')) break p000e0012;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0012;
 		}
		if (!CNDat(55)) break p000e0012;
		if (!CNDpresent(62)) break p000e0012;
 		ACCwriteln(8);
		{}

	}

	// E _
	p000e0013:
	{
 		if (skipdoall('p000e0013')) break p000e0013;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0013;
 		}
		if (!CNDat(55)) break p000e0013;
		if (!CNDabsent(62)) break p000e0013;
 		ACCgoto(56);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ASCE LADD
	p000e0014:
	{
 		if (skipdoall('p000e0014')) break p000e0014;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0014;
			if (!CNDnoun1(20)) break p000e0014;
 		}
		if (!CNDat(22)) break p000e0014;
		if (!CNDpresent(6)) break p000e0014;
 		ACCwriteln(9);
 		ACCgoto(26);
 		ACCpause(150);
 		function anykey00000() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00000);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// ASCE _
	p000e0015:
	{
 		if (skipdoall('p000e0015')) break p000e0015;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0015;
 		}
		if (!CNDat(22)) break p000e0015;
		if (!CNDabsent(6)) break p000e0015;
 		ACCwriteln(10);
		{}

	}

	// ASCE _
	p000e0016:
	{
 		if (skipdoall('p000e0016')) break p000e0016;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0016;
 		}
		if (!CNDat(22)) break p000e0016;
		if (!CNDpresent(6)) break p000e0016;
		if (!CNDnotcarr(6)) break p000e0016;
 		ACCwriteln(11);
 		ACCpause(150);
 		function anykey00001() 
		{
 		ACCgoto(26);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00001);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// LIGH CIGA
	p000e0017:
	{
 		if (skipdoall('p000e0017')) break p000e0017;
 		if (in_response)
		{
			if (!CNDverb(16)) break p000e0017;
			if (!CNDnoun1(15)) break p000e0017;
 		}
		if (!CNDpresent(12)) break p000e0017;
		if (!CNDpresent(1)) break p000e0017;
 		ACCswap(1,53);
 		ACCok();
		break pro000_restart;
		{}

	}

	// LIGH CIGA
	p000e0018:
	{
 		if (skipdoall('p000e0018')) break p000e0018;
 		if (in_response)
		{
			if (!CNDverb(16)) break p000e0018;
			if (!CNDnoun1(15)) break p000e0018;
 		}
		if (!CNDcarried(1)) break p000e0018;
		if (!CNDnotcarr(12)) break p000e0018;
 		ACCwriteln(12);
		{}

	}

	// LIGH CIGA
	p000e0019:
	{
 		if (skipdoall('p000e0019')) break p000e0019;
 		if (in_response)
		{
			if (!CNDverb(16)) break p000e0019;
			if (!CNDnoun1(15)) break p000e0019;
 		}
		if (!CNDpresent(53)) break p000e0019;
 		ACCwriteln(13);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BLOW BLOWPIPE
	p000e0020:
	{
 		if (skipdoall('p000e0020')) break p000e0020;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0020;
			if (!CNDnoun1(207)) break p000e0020;
 		}
		if (!CNDat(65)) break p000e0020;
		if (!CNDworn(24)) break p000e0020;
		if (!CNDcarried(17)) break p000e0020;
		if (!CNDpresent(40)) break p000e0020;
 		ACCwriteln(14);
 		ACCdestroy(40);
 		ACCdestroy(24);
 		ACCdestroy(68);
 		ACCplus(30,5);
		{}

	}

	// BLOW BLOWPIPE
	p000e0021:
	{
 		if (skipdoall('p000e0021')) break p000e0021;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0021;
			if (!CNDnoun1(207)) break p000e0021;
 		}
		if (!CNDnotat(65)) break p000e0021;
		if (!CNDcarried(17)) break p000e0021;
 		ACCwriteln(15);
		{}

	}

	// DRIN FLAS
	p000e0022:
	{
 		if (skipdoall('p000e0022')) break p000e0022;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0022;
			if (!CNDnoun1(41)) break p000e0022;
 		}
		if (!CNDcarried(27)) break p000e0022;
 		ACCdestroy(27);
 		ACCset(28);
 		ACCok();
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0023:
	{
 		if (skipdoall('p000e0023')) break p000e0023;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0023;
			if (!CNDnoun1(67)) break p000e0023;
 		}
		if (!CNDat(4)) break p000e0023;
 		ACCwriteln(16);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0024:
	{
 		if (skipdoall('p000e0024')) break p000e0024;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0024;
			if (!CNDnoun1(67)) break p000e0024;
 		}
		if (!CNDat(85)) break p000e0024;
 		ACCwriteln(17);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES IGNI
	p000e0025:
	{
 		if (skipdoall('p000e0025')) break p000e0025;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0025;
			if (!CNDnoun1(68)) break p000e0025;
 		}
		if (!CNDat(4)) break p000e0025;
 		ACCwriteln(18);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES IGNI
	p000e0026:
	{
 		if (skipdoall('p000e0026')) break p000e0026;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0026;
			if (!CNDnoun1(68)) break p000e0026;
 		}
		if (!CNDat(90)) break p000e0026;
 		ACCwriteln(19);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BLUE
	p000e0027:
	{
 		if (skipdoall('p000e0027')) break p000e0027;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0027;
			if (!CNDnoun1(69)) break p000e0027;
 		}
		if (!CNDat(4)) break p000e0027;
		if (!CNDzero(21)) break p000e0027;
 		ACCcreate(56);
 		ACCset(21);
 		ACCwriteln(20);
 		ACCpause(254);
 		function anykey00002() 
		{
 		ACCplus(30,1);
 		ACCdesc();
		return;
 		ACCdone();
		return;
		}
 		waitKey(anykey00002);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLUE
	p000e0028:
	{
 		if (skipdoall('p000e0028')) break p000e0028;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0028;
			if (!CNDnoun1(69)) break p000e0028;
 		}
		if (!CNDat(4)) break p000e0028;
		if (!CNDnotzero(21)) break p000e0028;
 		ACCwriteln(21);
 		ACCdone();
		break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BLUE
	p000e0029:
	{
 		if (skipdoall('p000e0029')) break p000e0029;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0029;
			if (!CNDnoun1(69)) break p000e0029;
 		}
		if (!CNDat(85)) break p000e0029;
 		ACCwriteln(22);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0030:
	{
 		if (skipdoall('p000e0030')) break p000e0030;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0030;
			if (!CNDnoun1(70)) break p000e0030;
 		}
		if (!CNDat(4)) break p000e0030;
 		ACCwriteln(23);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES WHIT
	p000e0031:
	{
 		if (skipdoall('p000e0031')) break p000e0031;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0031;
			if (!CNDnoun1(79)) break p000e0031;
 		}
		if (!CNDat(85)) break p000e0031;
 		ACCwriteln(24);
 		ACCpause(254);
 		function anykey00003() 
		{
 		ACCgoto(27);
 		ACCdesc();
		return;
 		ACCdone();
		return;
		}
 		waitKey(anykey00003);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES YELL
	p000e0032:
	{
 		if (skipdoall('p000e0032')) break p000e0032;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0032;
			if (!CNDnoun1(82)) break p000e0032;
 		}
		if (!CNDat(85)) break p000e0032;
 		ACCwriteln(25);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES _
	p000e0033:
	{
 		if (skipdoall('p000e0033')) break p000e0033;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0033;
 		}
 		ACCwriteln(26);
		{}

	}

	// OPEN DOOR
	p000e0034:
	{
 		if (skipdoall('p000e0034')) break p000e0034;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0034;
			if (!CNDnoun1(26)) break p000e0034;
 		}
		if (!CNDat(97)) break p000e0034;
		if (!CNDnotcarr(5)) break p000e0034;
 		ACCwriteln(27);
		{}

	}

	// OPEN DOOR
	p000e0035:
	{
 		if (skipdoall('p000e0035')) break p000e0035;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0035;
			if (!CNDnoun1(26)) break p000e0035;
 		}
		if (!CNDat(97)) break p000e0035;
		if (!CNDcarried(5)) break p000e0035;
 		ACCwriteln(28);
 		ACCgoto(96);
		{}

	}

	// OPEN DOOR
	p000e0036:
	{
 		if (skipdoall('p000e0036')) break p000e0036;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0036;
			if (!CNDnoun1(26)) break p000e0036;
 		}
		if (!CNDat(23)) break p000e0036;
 		ACCwriteln(29);
		{}

	}

	// OPEN BOX
	p000e0037:
	{
 		if (skipdoall('p000e0037')) break p000e0037;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0037;
			if (!CNDnoun1(63)) break p000e0037;
 		}
		if (!CNDat(3)) break p000e0037;
		if (!CNDpresent(57)) break p000e0037;
		if (!CNDpresent(5)) break p000e0037;
 		ACCdestroy(57);
 		ACCcreate(0);
 		ACCcreate(1);
 		ACCcreate(2);
 		ACCwriteln(30);
 		ACCplus(30,2);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN BOX
	p000e0038:
	{
 		if (skipdoall('p000e0038')) break p000e0038;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0038;
			if (!CNDnoun1(63)) break p000e0038;
 		}
		if (!CNDat(3)) break p000e0038;
		if (!CNDabsent(5)) break p000e0038;
 		ACCwriteln(31);
		{}

	}

	// OPEN BOX
	p000e0039:
	{
 		if (skipdoall('p000e0039')) break p000e0039;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0039;
			if (!CNDnoun1(63)) break p000e0039;
 		}
		if (!CNDat(3)) break p000e0039;
		if (!CNDpresent(57)) break p000e0039;
		if (!CNDpresent(5)) break p000e0039;
 		ACCwriteln(32);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HURL BALL
	p000e0040:
	{
 		if (skipdoall('p000e0040')) break p000e0040;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0040;
			if (!CNDnoun1(17)) break p000e0040;
 		}
		if (!CNDat(18)) break p000e0040;
		if (!CNDcarried(3)) break p000e0040;
		if (!CNDpresent(42)) break p000e0040;
 		ACCdestroy(42);
 		ACCdestroy(3);
 		ACCwriteln(33);
 		ACCplus(30,4);
		{}

	}

	// HURL BALL
	p000e0041:
	{
 		if (skipdoall('p000e0041')) break p000e0041;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0041;
			if (!CNDnoun1(17)) break p000e0041;
 		}
		if (!CNDnotat(18)) break p000e0041;
		if (!CNDcarried(3)) break p000e0041;
 		ACCwriteln(34);
		{}

	}

	// SQUE GLUE
	p000e0042:
	{
 		if (skipdoall('p000e0042')) break p000e0042;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0042;
			if (!CNDnoun1(23)) break p000e0042;
 		}
		if (!CNDcarried(9)) break p000e0042;
 		ACCwriteln(35);
		{}

	}

	// SQUE GLUE
	p000e0043:
	{
 		if (skipdoall('p000e0043')) break p000e0043;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0043;
			if (!CNDnoun1(23)) break p000e0043;
 		}
		if (!CNDat(16)) break p000e0043;
		if (!CNDpresent(37)) break p000e0043;
		if (!CNDcarried(9)) break p000e0043;
 		ACCwriteln(36);
 		ACCdestroy(37);
 		ACCplus(30,4);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SQUE SPON
	p000e0044:
	{
 		if (skipdoall('p000e0044')) break p000e0044;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0044;
			if (!CNDnoun1(33)) break p000e0044;
 		}
		if (!CNDat(74)) break p000e0044;
		if (!CNDpresent(43)) break p000e0044;
		if (!CNDcarried(19)) break p000e0044;
 		ACCdestroy(43);
 		ACCwriteln(37);
 		ACCplus(30,5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SQUE SPON
	p000e0045:
	{
 		if (skipdoall('p000e0045')) break p000e0045;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0045;
			if (!CNDnoun1(33)) break p000e0045;
 		}
		if (!CNDnotat(74)) break p000e0045;
 		ACCwriteln(38);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SMOK CIGA
	p000e0046:
	{
 		if (skipdoall('p000e0046')) break p000e0046;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0046;
			if (!CNDnoun1(15)) break p000e0046;
 		}
		if (!CNDcarried(53)) break p000e0046;
 		ACCwriteln(39);
		{}

	}

	// SMOK CIGA
	p000e0047:
	{
 		if (skipdoall('p000e0047')) break p000e0047;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0047;
			if (!CNDnoun1(15)) break p000e0047;
 		}
		if (!CNDcarried(1)) break p000e0047;
 		ACCwriteln(40);
		{}

	}

	// GIVE SLI
	p000e0048:
	{
 		if (skipdoall('p000e0048')) break p000e0048;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0048;
			if (!CNDnoun1(39)) break p000e0048;
 		}
		if (!CNDat(89)) break p000e0048;
		if (!CNDpresent(25)) break p000e0048;
 		ACCdestroy(35);
 		ACCdestroy(25);
 		ACCwriteln(41);
 		ACCplus(30,11);
		{}

	}

	// GIVE SLI
	p000e0049:
	{
 		if (skipdoall('p000e0049')) break p000e0049;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0049;
			if (!CNDnoun1(39)) break p000e0049;
 		}
		if (!CNDcarried(25)) break p000e0049;
		if (!CNDnotat(89)) break p000e0049;
 		ACCwriteln(42);
		{}

	}

	// FIRE BOOS
	p000e0050:
	{
 		if (skipdoall('p000e0050')) break p000e0050;
 		if (in_response)
		{
			if (!CNDverb(77)) break p000e0050;
			if (!CNDnoun1(18)) break p000e0050;
 		}
		if (!CNDnotat(58)) break p000e0050;
		if (!CNDworn(4)) break p000e0050;
 		ACCwriteln(43);
		{}

	}

	// FIRE BLAS
	p000e0051:
	{
 		if (skipdoall('p000e0051')) break p000e0051;
 		if (in_response)
		{
			if (!CNDverb(77)) break p000e0051;
			if (!CNDnoun1(43)) break p000e0051;
 		}
		if (!CNDcarried(29)) break p000e0051;
 		ACCwriteln(44);
		{}

	}

	// PUSH DROI
	p000e0052:
	{
 		if (skipdoall('p000e0052')) break p000e0052;
 		if (in_response)
		{
			if (!CNDverb(78)) break p000e0052;
			if (!CNDnoun1(55)) break p000e0052;
 		}
		if (!CNDat(87)) break p000e0052;
		if (!CNDpresent(54)) break p000e0052;
		if (!CNDworn(16)) break p000e0052;
 		ACCwriteln(45);
 		ACCdestroy(54);
 		ACCdestroy(67);
 		ACCplus(30,7);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PUSH DROI
	p000e0053:
	{
 		if (skipdoall('p000e0053')) break p000e0053;
 		if (in_response)
		{
			if (!CNDverb(78)) break p000e0053;
			if (!CNDnoun1(55)) break p000e0053;
 		}
		if (!CNDat(87)) break p000e0053;
		if (!CNDpresent(39)) break p000e0053;
 		ACCwriteln(46);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PUSH DROI
	p000e0054:
	{
 		if (skipdoall('p000e0054')) break p000e0054;
 		if (in_response)
		{
			if (!CNDverb(78)) break p000e0054;
			if (!CNDnoun1(55)) break p000e0054;
 		}
		if (!CNDat(87)) break p000e0054;
		if (!CNDpresent(54)) break p000e0054;
		if (!CNDnotworn(16)) break p000e0054;
 		ACCwriteln(47);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PUSH _
	p000e0055:
	{
 		if (skipdoall('p000e0055')) break p000e0055;
 		if (in_response)
		{
			if (!CNDverb(78)) break p000e0055;
 		}
 		ACCwriteln(48);
		{}

	}

	// PULL LEVE
	p000e0056:
	{
 		if (skipdoall('p000e0056')) break p000e0056;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0056;
			if (!CNDnoun1(81)) break p000e0056;
 		}
		if (!CNDat(83)) break p000e0056;
 		ACCwriteln(49);
 		ACCpause(254);
 		function anykey00004() 
		{
 		ACCgoto(90);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00004);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// INSE CARD
	p000e0057:
	{
 		if (skipdoall('p000e0057')) break p000e0057;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0057;
			if (!CNDnoun1(14)) break p000e0057;
 		}
		if (!CNDat(24)) break p000e0057;
		if (!CNDcarried(0)) break p000e0057;
		if (!CNDpresent(32)) break p000e0057;
 		ACCdestroy(32);
 		ACCwriteln(50);
 		ACCplus(30,4);
 		ACCwriteln(51);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INSE CARD
	p000e0058:
	{
 		if (skipdoall('p000e0058')) break p000e0058;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0058;
			if (!CNDnoun1(14)) break p000e0058;
 		}
		if (!CNDat(47)) break p000e0058;
		if (!CNDpresent(33)) break p000e0058;
		if (!CNDcarried(0)) break p000e0058;
 		ACCdestroy(33);
 		ACCwriteln(52);
 		ACCdestroy(62);
 		ACCcreate(63);
 		ACCclear(26);
 		ACCplus(30,2);
 		ACCwriteln(53);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INSE CARD
	p000e0059:
	{
 		if (skipdoall('p000e0059')) break p000e0059;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0059;
			if (!CNDnoun1(14)) break p000e0059;
 		}
		if (!CNDat(75)) break p000e0059;
		if (!CNDcarried(0)) break p000e0059;
		if (!CNDpresent(34)) break p000e0059;
 		ACCdestroy(34);
 		ACCwriteln(54);
 		ACCplus(30,2);
 		ACCset(24);
 		ACCwriteln(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INSE CARD
	p000e0060:
	{
 		if (skipdoall('p000e0060')) break p000e0060;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0060;
			if (!CNDnoun1(14)) break p000e0060;
 		}
		if (!CNDcarried(0)) break p000e0060;
		if (!CNDnotat(24)) break p000e0060;
		if (!CNDnotat(47)) break p000e0060;
		if (!CNDnotat(75)) break p000e0060;
 		ACCwriteln(56);
 		ACCdestroy(0);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INSE CRED
	p000e0061:
	{
 		if (skipdoall('p000e0061')) break p000e0061;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0061;
			if (!CNDnoun1(22)) break p000e0061;
 		}
		if (!CNDat(21)) break p000e0061;
		if (!CNDpresent(8)) break p000e0061;
		if (!CNDpresent(36)) break p000e0061;
 		ACCwriteln(57);
 		ACCpause(50);
 		function anykey00005() 
		{
 		ACCdestroy(8);
 		ACCdestroy(58);
 		ACCcreate(59);
 		ACCplus(30,4);
 		ACCdone();
		return;
		}
 		waitKey(anykey00005);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// INSE BATT
	p000e0062:
	{
 		if (skipdoall('p000e0062')) break p000e0062;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0062;
			if (!CNDnoun1(28)) break p000e0062;
 		}
		if (!CNDpresent(39)) break p000e0062;
		if (!CNDcarried(14)) break p000e0062;
 		ACCdestroy(39);
 		ACCcreate(54);
 		ACCdestroy(14);
 		ACCok();
		break pro000_restart;
		{}

	}

	// INSE NOTE
	p000e0063:
	{
 		if (skipdoall('p000e0063')) break p000e0063;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0063;
			if (!CNDnoun1(37)) break p000e0063;
 		}
		if (!CNDcarried(23)) break p000e0063;
 		ACCwriteln(58);
 		ACCdestroy(23);
		{}

	}

	// INSE PASS
	p000e0064:
	{
 		if (skipdoall('p000e0064')) break p000e0064;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0064;
			if (!CNDnoun1(40)) break p000e0064;
 		}
 		ACCwriteln(59);
 		ACCdestroy(26);
		{}

	}

	// INSE CLIP
	p000e0065:
	{
 		if (skipdoall('p000e0065')) break p000e0065;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0065;
			if (!CNDnoun1(44)) break p000e0065;
 		}
		if (!CNDat(46)) break p000e0065;
		if (!CNDcarried(30)) break p000e0065;
 		ACCwriteln(60);
		{}

	}

	// INSE PACK
	p000e0066:
	{
 		if (skipdoall('p000e0066')) break p000e0066;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0066;
			if (!CNDnoun1(45)) break p000e0066;
 		}
		if (!CNDat(111)) break p000e0066;
		if (!CNDpresent(31)) break p000e0066;
 		ACCwriteln(61);
 		ACCplus(30,10);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// SEAR TENT
	p000e0067:
	{
 		if (skipdoall('p000e0067')) break p000e0067;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0067;
			if (!CNDnoun1(88)) break p000e0067;
 		}
		if (!CNDat(113)) break p000e0067;
		if (!CNDzero(20)) break p000e0067;
 		ACCcreate(29);
 		ACCset(20);
 		ACCplus(30,2);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SEAR TENT
	p000e0068:
	{
 		if (skipdoall('p000e0068')) break p000e0068;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0068;
			if (!CNDnoun1(88)) break p000e0068;
 		}
		if (!CNDat(113)) break p000e0068;
		if (!CNDnotzero(20)) break p000e0068;
 		ACCwriteln(62);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR PANE
	p000e0069:
	{
 		if (skipdoall('p000e0069')) break p000e0069;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0069;
			if (!CNDnoun1(89)) break p000e0069;
 		}
		if (!CNDat(103)) break p000e0069;
		if (!CNDnotzero(19)) break p000e0069;
 		ACCwriteln(63);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR CHAI
	p000e0070:
	{
 		if (skipdoall('p000e0070')) break p000e0070;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0070;
			if (!CNDnoun1(90)) break p000e0070;
 		}
		if (!CNDat(49)) break p000e0070;
		if (!CNDzero(18)) break p000e0070;
 		ACCcreate(26);
 		ACCset(18);
 		ACCplus(30,2);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SEAR CHAI
	p000e0071:
	{
 		if (skipdoall('p000e0071')) break p000e0071;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0071;
			if (!CNDnoun1(90)) break p000e0071;
 		}
		if (!CNDat(49)) break p000e0071;
		if (!CNDnotzero(18)) break p000e0071;
 		ACCwriteln(64);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR OIL
	p000e0072:
	{
 		if (skipdoall('p000e0072')) break p000e0072;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0072;
			if (!CNDnoun1(91)) break p000e0072;
 		}
		if (!CNDat(78)) break p000e0072;
		if (!CNDzero(17)) break p000e0072;
 		ACCcreate(25);
 		ACCset(17);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SEAR OIL
	p000e0073:
	{
 		if (skipdoall('p000e0073')) break p000e0073;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0073;
			if (!CNDnoun1(91)) break p000e0073;
 		}
		if (!CNDat(78)) break p000e0073;
		if (!CNDnotzero(17)) break p000e0073;
 		ACCwriteln(65);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR CANT
	p000e0074:
	{
 		if (skipdoall('p000e0074')) break p000e0074;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0074;
			if (!CNDnoun1(92)) break p000e0074;
 		}
		if (!CNDat(64)) break p000e0074;
		if (!CNDzero(116)) break p000e0074;
 		ACCcreate(24);
 		ACCset(116);
 		ACCplus(30,2);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SEAR CANT
	p000e0075:
	{
 		if (skipdoall('p000e0075')) break p000e0075;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0075;
			if (!CNDnoun1(92)) break p000e0075;
 		}
		if (!CNDat(64)) break p000e0075;
		if (!CNDnotzero(116)) break p000e0075;
 		ACCwriteln(66);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR CUPB
	p000e0076:
	{
 		if (skipdoall('p000e0076')) break p000e0076;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0076;
			if (!CNDnoun1(93)) break p000e0076;
 		}
		if (!CNDat(122)) break p000e0076;
		if (!CNDzero(115)) break p000e0076;
 		ACCcreate(22);
 		ACCset(115);
 		ACCplus(30,2);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SEAR CUPB
	p000e0077:
	{
 		if (skipdoall('p000e0077')) break p000e0077;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0077;
			if (!CNDnoun1(93)) break p000e0077;
 		}
		if (!CNDat(122)) break p000e0077;
		if (!CNDnotzero(115)) break p000e0077;
 		ACCwriteln(67);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR DUST
	p000e0078:
	{
 		if (skipdoall('p000e0078')) break p000e0078;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0078;
			if (!CNDnoun1(94)) break p000e0078;
 		}
		if (!CNDat(69)) break p000e0078;
		if (!CNDzero(114)) break p000e0078;
 		ACCcreate(19);
 		ACCset(114);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SEAR DUST
	p000e0079:
	{
 		if (skipdoall('p000e0079')) break p000e0079;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0079;
			if (!CNDnoun1(94)) break p000e0079;
 		}
		if (!CNDat(69)) break p000e0079;
		if (!CNDnotzero(114)) break p000e0079;
 		ACCwriteln(68);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR DRAW
	p000e0080:
	{
 		if (skipdoall('p000e0080')) break p000e0080;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0080;
			if (!CNDnoun1(95)) break p000e0080;
 		}
		if (!CNDat(85)) break p000e0080;
		if (!CNDzero(113)) break p000e0080;
 		ACCcreate(17);
 		ACCset(113);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SEAR DRAW
	p000e0081:
	{
 		if (skipdoall('p000e0081')) break p000e0081;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0081;
			if (!CNDnoun1(95)) break p000e0081;
 		}
		if (!CNDat(85)) break p000e0081;
		if (!CNDnotzero(113)) break p000e0081;
 		ACCwriteln(69);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR COGS
	p000e0082:
	{
 		if (skipdoall('p000e0082')) break p000e0082;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0082;
			if (!CNDnoun1(96)) break p000e0082;
 		}
		if (!CNDat(51)) break p000e0082;
		if (!CNDzero(112)) break p000e0082;
 		ACCcreate(14);
 		ACCset(112);
 		ACCplus(30,2);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SEAR COGS
	p000e0083:
	{
 		if (skipdoall('p000e0083')) break p000e0083;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0083;
			if (!CNDnoun1(96)) break p000e0083;
 		}
		if (!CNDat(51)) break p000e0083;
		if (!CNDnotzero(112)) break p000e0083;
 		ACCwriteln(70);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR LOCK
	p000e0084:
	{
 		if (skipdoall('p000e0084')) break p000e0084;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0084;
			if (!CNDnoun1(97)) break p000e0084;
 		}
		if (!CNDat(44)) break p000e0084;
		if (!CNDzero(111)) break p000e0084;
 		ACCcreate(10);
 		ACCset(111);
 		ACCplus(30,2);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SEAR LOCK
	p000e0085:
	{
 		if (skipdoall('p000e0085')) break p000e0085;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0085;
			if (!CNDnoun1(97)) break p000e0085;
 		}
		if (!CNDat(44)) break p000e0085;
		if (!CNDnotzero(111)) break p000e0085;
 		ACCwriteln(71);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR _
	p000e0086:
	{
 		if (skipdoall('p000e0086')) break p000e0086;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0086;
 		}
 		ACCwriteln(72);
		{}

	}

	// EXAM TELE
	p000e0087:
	{
 		if (skipdoall('p000e0087')) break p000e0087;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0087;
			if (!CNDnoun1(13)) break p000e0087;
 		}
		if (!CNDpresent(45)) break p000e0087;
 		ACCwriteln(73);
		{}

	}

	// EXAM TELE
	p000e0088:
	{
 		if (skipdoall('p000e0088')) break p000e0088;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0088;
			if (!CNDnoun1(13)) break p000e0088;
 		}
		if (!CNDpresent(46)) break p000e0088;
 		ACCwriteln(74);
		{}

	}

	// EXAM CARD
	p000e0089:
	{
 		if (skipdoall('p000e0089')) break p000e0089;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0089;
			if (!CNDnoun1(14)) break p000e0089;
 		}
		if (!CNDpresent(0)) break p000e0089;
 		ACCwriteln(75);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CIGA
	p000e0090:
	{
 		if (skipdoall('p000e0090')) break p000e0090;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0090;
			if (!CNDnoun1(15)) break p000e0090;
 		}
		if (!CNDpresent(1)) break p000e0090;
 		ACCwriteln(76);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CIGA
	p000e0091:
	{
 		if (skipdoall('p000e0091')) break p000e0091;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0091;
			if (!CNDnoun1(15)) break p000e0091;
 		}
		if (!CNDpresent(53)) break p000e0091;
 		ACCwriteln(77);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LIGHT
	p000e0092:
	{
 		if (skipdoall('p000e0092')) break p000e0092;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0092;
			if (!CNDnoun1(16)) break p000e0092;
 		}
		if (!CNDpresent(2)) break p000e0092;
 		ACCwriteln(78);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LIGHT
	p000e0093:
	{
 		if (skipdoall('p000e0093')) break p000e0093;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0093;
			if (!CNDnoun1(16)) break p000e0093;
 		}
		if (!CNDpresent(12)) break p000e0093;
 		ACCwriteln(79);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BALL
	p000e0094:
	{
 		if (skipdoall('p000e0094')) break p000e0094;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0094;
			if (!CNDnoun1(17)) break p000e0094;
 		}
		if (!CNDpresent(3)) break p000e0094;
 		ACCwriteln(80);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOOS
	p000e0095:
	{
 		if (skipdoall('p000e0095')) break p000e0095;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0095;
			if (!CNDnoun1(18)) break p000e0095;
 		}
		if (!CNDpresent(4)) break p000e0095;
 		ACCwriteln(81);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COMB
	p000e0096:
	{
 		if (skipdoall('p000e0096')) break p000e0096;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0096;
			if (!CNDnoun1(19)) break p000e0096;
 		}
		if (!CNDpresent(5)) break p000e0096;
 		ACCwriteln(82);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LADD
	p000e0097:
	{
 		if (skipdoall('p000e0097')) break p000e0097;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0097;
			if (!CNDnoun1(20)) break p000e0097;
 		}
		if (!CNDpresent(6)) break p000e0097;
 		ACCwriteln(83);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SULP
	p000e0098:
	{
 		if (skipdoall('p000e0098')) break p000e0098;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0098;
			if (!CNDnoun1(21)) break p000e0098;
 		}
		if (!CNDpresent(7)) break p000e0098;
 		ACCwriteln(84);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CRED
	p000e0099:
	{
 		if (skipdoall('p000e0099')) break p000e0099;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0099;
			if (!CNDnoun1(22)) break p000e0099;
 		}
		if (!CNDpresent(8)) break p000e0099;
 		ACCwriteln(85);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GLUE
	p000e0100:
	{
 		if (skipdoall('p000e0100')) break p000e0100;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0100;
			if (!CNDnoun1(23)) break p000e0100;
 		}
		if (!CNDpresent(9)) break p000e0100;
 		ACCwriteln(86);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DISP
	p000e0101:
	{
 		if (skipdoall('p000e0101')) break p000e0101;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0101;
			if (!CNDnoun1(24)) break p000e0101;
 		}
		if (!CNDpresent(10)) break p000e0101;
 		ACCwriteln(87);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM MANU
	p000e0102:
	{
 		if (skipdoall('p000e0102')) break p000e0102;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0102;
			if (!CNDnoun1(25)) break p000e0102;
 		}
		if (!CNDpresent(11)) break p000e0102;
 		ACCwriteln(88);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DISR
	p000e0103:
	{
 		if (skipdoall('p000e0103')) break p000e0103;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0103;
			if (!CNDnoun1(27)) break p000e0103;
 		}
		if (!CNDpresent(13)) break p000e0103;
 		ACCwriteln(89);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BATT
	p000e0104:
	{
 		if (skipdoall('p000e0104')) break p000e0104;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0104;
			if (!CNDnoun1(28)) break p000e0104;
 		}
		if (!CNDpresent(14)) break p000e0104;
 		ACCwriteln(90);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CAPE
	p000e0105:
	{
 		if (skipdoall('p000e0105')) break p000e0105;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0105;
			if (!CNDnoun1(29)) break p000e0105;
 		}
		if (!CNDpresent(15)) break p000e0105;
 		ACCwriteln(91);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CUFF
	p000e0106:
	{
 		if (skipdoall('p000e0106')) break p000e0106;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0106;
			if (!CNDnoun1(30)) break p000e0106;
 		}
		if (!CNDpresent(16)) break p000e0106;
 		ACCwriteln(92);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BLOWPIPE
	p000e0107:
	{
 		if (skipdoall('p000e0107')) break p000e0107;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0107;
			if (!CNDnoun1(207)) break p000e0107;
 		}
		if (!CNDpresent(17)) break p000e0107;
 		ACCwriteln(93);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOOT
	p000e0108:
	{
 		if (skipdoall('p000e0108')) break p000e0108;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0108;
			if (!CNDnoun1(32)) break p000e0108;
 		}
		if (!CNDpresent(18)) break p000e0108;
 		ACCwriteln(94);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SPON
	p000e0109:
	{
 		if (skipdoall('p000e0109')) break p000e0109;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0109;
			if (!CNDnoun1(33)) break p000e0109;
 		}
		if (!CNDpresent(19)) break p000e0109;
 		ACCwriteln(95);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BRAC
	p000e0110:
	{
 		if (skipdoall('p000e0110')) break p000e0110;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0110;
			if (!CNDnoun1(34)) break p000e0110;
 		}
		if (!CNDpresent(20)) break p000e0110;
 		ACCwriteln(96);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM AIR
	p000e0111:
	{
 		if (skipdoall('p000e0111')) break p000e0111;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0111;
			if (!CNDnoun1(35)) break p000e0111;
 		}
		if (!CNDpresent(21)) break p000e0111;
 		ACCwriteln(97);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM AQUA
	p000e0112:
	{
 		if (skipdoall('p000e0112')) break p000e0112;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0112;
			if (!CNDnoun1(36)) break p000e0112;
 		}
		if (!CNDpresent(22)) break p000e0112;
 		ACCwriteln(98);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM NOTE
	p000e0113:
	{
 		if (skipdoall('p000e0113')) break p000e0113;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0113;
			if (!CNDnoun1(37)) break p000e0113;
 		}
		if (!CNDpresent(23)) break p000e0113;
 		ACCwriteln(99);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ELEC
	p000e0114:
	{
 		if (skipdoall('p000e0114')) break p000e0114;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0114;
			if (!CNDnoun1(38)) break p000e0114;
 		}
		if (!CNDpresent(24)) break p000e0114;
 		ACCwriteln(100);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SLI
	p000e0115:
	{
 		if (skipdoall('p000e0115')) break p000e0115;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0115;
			if (!CNDnoun1(39)) break p000e0115;
 		}
		if (!CNDpresent(25)) break p000e0115;
 		ACCwriteln(101);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PASS
	p000e0116:
	{
 		if (skipdoall('p000e0116')) break p000e0116;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0116;
			if (!CNDnoun1(40)) break p000e0116;
 		}
		if (!CNDpresent(26)) break p000e0116;
 		ACCwriteln(102);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM FLAS
	p000e0117:
	{
 		if (skipdoall('p000e0117')) break p000e0117;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0117;
			if (!CNDnoun1(41)) break p000e0117;
 		}
		if (!CNDpresent(27)) break p000e0117;
 		ACCwriteln(103);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOMB
	p000e0118:
	{
 		if (skipdoall('p000e0118')) break p000e0118;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0118;
			if (!CNDnoun1(42)) break p000e0118;
 		}
		if (!CNDpresent(28)) break p000e0118;
 		ACCwriteln(104);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BLAS
	p000e0119:
	{
 		if (skipdoall('p000e0119')) break p000e0119;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0119;
			if (!CNDnoun1(43)) break p000e0119;
 		}
		if (!CNDpresent(29)) break p000e0119;
 		ACCwriteln(105);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CLIP
	p000e0120:
	{
 		if (skipdoall('p000e0120')) break p000e0120;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0120;
			if (!CNDnoun1(44)) break p000e0120;
 		}
		if (!CNDpresent(30)) break p000e0120;
 		ACCwriteln(106);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PACK
	p000e0121:
	{
 		if (skipdoall('p000e0121')) break p000e0121;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0121;
			if (!CNDnoun1(45)) break p000e0121;
 		}
		if (!CNDpresent(31)) break p000e0121;
 		ACCwriteln(107);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM POST
	p000e0122:
	{
 		if (skipdoall('p000e0122')) break p000e0122;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0122;
			if (!CNDnoun1(46)) break p000e0122;
 		}
		if (!CNDat(45)) break p000e0122;
 		ACCwriteln(108);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DATA
	p000e0123:
	{
 		if (skipdoall('p000e0123')) break p000e0123;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0123;
			if (!CNDnoun1(50)) break p000e0123;
 		}
		if (!CNDpresent(32)) break p000e0123;
 		ACCwriteln(109);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DATA
	p000e0124:
	{
 		if (skipdoall('p000e0124')) break p000e0124;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0124;
			if (!CNDnoun1(50)) break p000e0124;
 		}
		if (!CNDpresent(33)) break p000e0124;
 		ACCwriteln(110);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DATA
	p000e0125:
	{
 		if (skipdoall('p000e0125')) break p000e0125;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0125;
			if (!CNDnoun1(50)) break p000e0125;
 		}
		if (!CNDpresent(34)) break p000e0125;
 		ACCwriteln(111);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COMP
	p000e0126:
	{
 		if (skipdoall('p000e0126')) break p000e0126;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0126;
			if (!CNDnoun1(51)) break p000e0126;
 		}
		if (!CNDpresent(35)) break p000e0126;
 		ACCwriteln(112);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SPEA
	p000e0127:
	{
 		if (skipdoall('p000e0127')) break p000e0127;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0127;
			if (!CNDnoun1(52)) break p000e0127;
 		}
		if (!CNDpresent(36)) break p000e0127;
 		ACCwriteln(113);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM MAIN
	p000e0128:
	{
 		if (skipdoall('p000e0128')) break p000e0128;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0128;
			if (!CNDnoun1(53)) break p000e0128;
 		}
		if (!CNDpresent(37)) break p000e0128;
 		ACCwriteln(114);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HYGI
	p000e0129:
	{
 		if (skipdoall('p000e0129')) break p000e0129;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0129;
			if (!CNDnoun1(54)) break p000e0129;
 		}
		if (!CNDpresent(38)) break p000e0129;
 		ACCwriteln(115);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DROI
	p000e0130:
	{
 		if (skipdoall('p000e0130')) break p000e0130;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0130;
			if (!CNDnoun1(55)) break p000e0130;
 		}
		if (!CNDpresent(39)) break p000e0130;
 		ACCwriteln(116);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DROI
	p000e0131:
	{
 		if (skipdoall('p000e0131')) break p000e0131;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0131;
			if (!CNDnoun1(55)) break p000e0131;
 		}
		if (!CNDpresent(54)) break p000e0131;
 		ACCwriteln(117);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HIGH
	p000e0132:
	{
 		if (skipdoall('p000e0132')) break p000e0132;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0132;
			if (!CNDnoun1(56)) break p000e0132;
 		}
		if (!CNDpresent(40)) break p000e0132;
 		ACCwriteln(118);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LATT
	p000e0133:
	{
 		if (skipdoall('p000e0133')) break p000e0133;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0133;
			if (!CNDnoun1(57)) break p000e0133;
 		}
		if (!CNDpresent(41)) break p000e0133;
 		ACCwriteln(119);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM AUTO
	p000e0134:
	{
 		if (skipdoall('p000e0134')) break p000e0134;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0134;
			if (!CNDnoun1(58)) break p000e0134;
 		}
		if (!CNDpresent(42)) break p000e0134;
 		ACCwriteln(120);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SHAR
	p000e0135:
	{
 		if (skipdoall('p000e0135')) break p000e0135;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0135;
			if (!CNDnoun1(59)) break p000e0135;
 		}
		if (!CNDpresent(43)) break p000e0135;
 		ACCwriteln(121);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CODE
	p000e0136:
	{
 		if (skipdoall('p000e0136')) break p000e0136;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0136;
			if (!CNDnoun1(60)) break p000e0136;
 		}
		if (!CNDpresent(44)) break p000e0136;
 		ACCwriteln(122);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GUAR
	p000e0137:
	{
 		if (skipdoall('p000e0137')) break p000e0137;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0137;
			if (!CNDnoun1(61)) break p000e0137;
 		}
		if (!CNDpresent(47)) break p000e0137;
 		ACCwriteln(123);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GUAR
	p000e0138:
	{
 		if (skipdoall('p000e0138')) break p000e0138;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0138;
			if (!CNDnoun1(61)) break p000e0138;
 		}
		if (!CNDpresent(48)) break p000e0138;
 		ACCwriteln(124);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM FIEL
	p000e0139:
	{
 		if (skipdoall('p000e0139')) break p000e0139;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0139;
			if (!CNDnoun1(62)) break p000e0139;
 		}
		if (!CNDpresent(49)) break p000e0139;
 		ACCwriteln(125);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM FIEL
	p000e0140:
	{
 		if (skipdoall('p000e0140')) break p000e0140;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0140;
			if (!CNDnoun1(62)) break p000e0140;
 		}
		if (!CNDpresent(52)) break p000e0140;
 		ACCwriteln(126);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOX
	p000e0141:
	{
 		if (skipdoall('p000e0141')) break p000e0141;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0141;
			if (!CNDnoun1(63)) break p000e0141;
 		}
		if (!CNDat(30)) break p000e0141;
 		ACCwriteln(127);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOX
	p000e0142:
	{
 		if (skipdoall('p000e0142')) break p000e0142;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0142;
			if (!CNDnoun1(63)) break p000e0142;
 		}
		if (!CNDat(3)) break p000e0142;
		if (!CNDpresent(57)) break p000e0142;
 		ACCwriteln(128);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GRIL
	p000e0143:
	{
 		if (skipdoall('p000e0143')) break p000e0143;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0143;
			if (!CNDnoun1(64)) break p000e0143;
 		}
		if (!CNDpresent(51)) break p000e0143;
 		ACCwriteln(129);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GAS
	p000e0144:
	{
 		if (skipdoall('p000e0144')) break p000e0144;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0144;
			if (!CNDnoun1(65)) break p000e0144;
 		}
		if (!CNDpresent(12)) break p000e0144;
 		ACCwriteln(130);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BADG
	p000e0145:
	{
 		if (skipdoall('p000e0145')) break p000e0145;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0145;
			if (!CNDnoun1(71)) break p000e0145;
 		}
		if (!CNDpresent(56)) break p000e0145;
 		ACCwriteln(131);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DISC
	p000e0146:
	{
 		if (skipdoall('p000e0146')) break p000e0146;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0146;
			if (!CNDnoun1(83)) break p000e0146;
 		}
		if (!CNDpresent(69)) break p000e0146;
 		ACCwriteln(132);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM _
	p000e0147:
	{
 		if (skipdoall('p000e0147')) break p000e0147;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0147;
 		}
 		ACCwriteln(133);
		{}

	}

	// GET CARD
	p000e0148:
	{
 		if (skipdoall('p000e0148')) break p000e0148;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0148;
			if (!CNDnoun1(14)) break p000e0148;
 		}
 		ACCget(0);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET CARD
	p000e0149:
	{
 		if (skipdoall('p000e0149')) break p000e0149;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0149;
			if (!CNDnoun1(14)) break p000e0149;
 		}
 		ACCget(0);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET CIGA
	p000e0150:
	{
 		if (skipdoall('p000e0150')) break p000e0150;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0150;
			if (!CNDnoun1(15)) break p000e0150;
 		}
		if (!CNDpresent(1)) break p000e0150;
 		ACCget(1);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET CIGA
	p000e0151:
	{
 		if (skipdoall('p000e0151')) break p000e0151;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0151;
			if (!CNDnoun1(15)) break p000e0151;
 		}
		if (!CNDpresent(53)) break p000e0151;
 		ACCget(53);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET LIGHT
	p000e0152:
	{
 		if (skipdoall('p000e0152')) break p000e0152;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0152;
			if (!CNDnoun1(16)) break p000e0152;
 		}
 		ACCget(2);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BALL
	p000e0153:
	{
 		if (skipdoall('p000e0153')) break p000e0153;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0153;
			if (!CNDnoun1(17)) break p000e0153;
 		}
 		ACCget(3);
		if (!success) break pro000_restart;
 		ACCwriteln(134);
		{}

	}

	// GET BOOS
	p000e0154:
	{
 		if (skipdoall('p000e0154')) break p000e0154;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0154;
			if (!CNDnoun1(18)) break p000e0154;
 		}
 		ACCget(4);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET COMB
	p000e0155:
	{
 		if (skipdoall('p000e0155')) break p000e0155;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0155;
			if (!CNDnoun1(19)) break p000e0155;
 		}
 		ACCget(5);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET LADD
	p000e0156:
	{
 		if (skipdoall('p000e0156')) break p000e0156;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0156;
			if (!CNDnoun1(20)) break p000e0156;
 		}
 		ACCget(6);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET SULP
	p000e0157:
	{
 		if (skipdoall('p000e0157')) break p000e0157;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0157;
			if (!CNDnoun1(21)) break p000e0157;
 		}
 		ACCget(7);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET CRED
	p000e0158:
	{
 		if (skipdoall('p000e0158')) break p000e0158;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0158;
			if (!CNDnoun1(22)) break p000e0158;
 		}
 		ACCget(8);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET GLUE
	p000e0159:
	{
 		if (skipdoall('p000e0159')) break p000e0159;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0159;
			if (!CNDnoun1(23)) break p000e0159;
 		}
 		ACCget(9);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET DISP
	p000e0160:
	{
 		if (skipdoall('p000e0160')) break p000e0160;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0160;
			if (!CNDnoun1(24)) break p000e0160;
 		}
 		ACCget(10);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET MANU
	p000e0161:
	{
 		if (skipdoall('p000e0161')) break p000e0161;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0161;
			if (!CNDnoun1(25)) break p000e0161;
 		}
 		ACCget(11);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET DISR
	p000e0162:
	{
 		if (skipdoall('p000e0162')) break p000e0162;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0162;
			if (!CNDnoun1(27)) break p000e0162;
 		}
 		ACCget(13);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BATT
	p000e0163:
	{
 		if (skipdoall('p000e0163')) break p000e0163;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0163;
			if (!CNDnoun1(28)) break p000e0163;
 		}
 		ACCget(14);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET CAPE
	p000e0164:
	{
 		if (skipdoall('p000e0164')) break p000e0164;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0164;
			if (!CNDnoun1(29)) break p000e0164;
 		}
 		ACCget(15);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET CUFF
	p000e0165:
	{
 		if (skipdoall('p000e0165')) break p000e0165;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0165;
			if (!CNDnoun1(30)) break p000e0165;
 		}
 		ACCget(16);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BLOWPIPE
	p000e0166:
	{
 		if (skipdoall('p000e0166')) break p000e0166;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0166;
			if (!CNDnoun1(207)) break p000e0166;
 		}
 		ACCget(17);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BOOT
	p000e0167:
	{
 		if (skipdoall('p000e0167')) break p000e0167;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0167;
			if (!CNDnoun1(32)) break p000e0167;
 		}
 		ACCget(18);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET SPON
	p000e0168:
	{
 		if (skipdoall('p000e0168')) break p000e0168;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0168;
			if (!CNDnoun1(33)) break p000e0168;
 		}
 		ACCget(19);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BRAC
	p000e0169:
	{
 		if (skipdoall('p000e0169')) break p000e0169;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0169;
			if (!CNDnoun1(34)) break p000e0169;
 		}
 		ACCget(20);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET AIR
	p000e0170:
	{
 		if (skipdoall('p000e0170')) break p000e0170;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0170;
			if (!CNDnoun1(35)) break p000e0170;
 		}
 		ACCget(21);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET AQUA
	p000e0171:
	{
 		if (skipdoall('p000e0171')) break p000e0171;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0171;
			if (!CNDnoun1(36)) break p000e0171;
 		}
 		ACCget(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET NOTE
	p000e0172:
	{
 		if (skipdoall('p000e0172')) break p000e0172;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0172;
			if (!CNDnoun1(37)) break p000e0172;
 		}
 		ACCget(23);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET ELEC
	p000e0173:
	{
 		if (skipdoall('p000e0173')) break p000e0173;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0173;
			if (!CNDnoun1(38)) break p000e0173;
 		}
 		ACCget(24);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET SLI
	p000e0174:
	{
 		if (skipdoall('p000e0174')) break p000e0174;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0174;
			if (!CNDnoun1(39)) break p000e0174;
 		}
 		ACCget(25);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET PASS
	p000e0175:
	{
 		if (skipdoall('p000e0175')) break p000e0175;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0175;
			if (!CNDnoun1(40)) break p000e0175;
 		}
 		ACCget(26);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET FLAS
	p000e0176:
	{
 		if (skipdoall('p000e0176')) break p000e0176;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0176;
			if (!CNDnoun1(41)) break p000e0176;
 		}
 		ACCget(27);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BOMB
	p000e0177:
	{
 		if (skipdoall('p000e0177')) break p000e0177;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0177;
			if (!CNDnoun1(42)) break p000e0177;
 		}
 		ACCget(28);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BLAS
	p000e0178:
	{
 		if (skipdoall('p000e0178')) break p000e0178;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0178;
			if (!CNDnoun1(43)) break p000e0178;
 		}
		if (!CNDpresent(29)) break p000e0178;
 		ACCget(29);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET CLIP
	p000e0179:
	{
 		if (skipdoall('p000e0179')) break p000e0179;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0179;
			if (!CNDnoun1(44)) break p000e0179;
 		}
 		ACCget(30);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET PACK
	p000e0180:
	{
 		if (skipdoall('p000e0180')) break p000e0180;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0180;
			if (!CNDnoun1(45)) break p000e0180;
 		}
 		ACCget(31);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET GAS
	p000e0181:
	{
 		if (skipdoall('p000e0181')) break p000e0181;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0181;
			if (!CNDnoun1(65)) break p000e0181;
 		}
 		ACCget(12);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BADG
	p000e0182:
	{
 		if (skipdoall('p000e0182')) break p000e0182;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0182;
			if (!CNDnoun1(71)) break p000e0182;
 		}
 		ACCget(56);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET DISC
	p000e0183:
	{
 		if (skipdoall('p000e0183')) break p000e0183;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0183;
			if (!CNDnoun1(83)) break p000e0183;
 		}
 		ACCget(69);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET I
	p000e0184:
	{
 		if (skipdoall('p000e0184')) break p000e0184;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0184;
			if (!CNDnoun1(11)) break p000e0184;
 		}
 		ACCinven();
		break pro000_restart;
		{}

	}

	// DROP CARD
	p000e0185:
	{
 		if (skipdoall('p000e0185')) break p000e0185;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0185;
			if (!CNDnoun1(14)) break p000e0185;
 		}
 		ACCdrop(0);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP CIGA
	p000e0186:
	{
 		if (skipdoall('p000e0186')) break p000e0186;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0186;
			if (!CNDnoun1(15)) break p000e0186;
 		}
		if (!CNDpresent(53)) break p000e0186;
 		ACCdrop(53);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP CIGA
	p000e0187:
	{
 		if (skipdoall('p000e0187')) break p000e0187;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0187;
			if (!CNDnoun1(15)) break p000e0187;
 		}
		if (!CNDpresent(1)) break p000e0187;
 		ACCdrop(1);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP LIGHT
	p000e0188:
	{
 		if (skipdoall('p000e0188')) break p000e0188;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0188;
			if (!CNDnoun1(16)) break p000e0188;
 		}
		if (!CNDeq(22,0)) break p000e0188;
		if (!CNDcarried(2)) break p000e0188;
 		ACCwriteln(135);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// DROP LIGHT
	p000e0189:
	{
 		if (skipdoall('p000e0189')) break p000e0189;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0189;
			if (!CNDnoun1(16)) break p000e0189;
 		}
		if (!CNDcarried(2)) break p000e0189;
		if (!CNDnotzero(22)) break p000e0189;
 		ACCwriteln(136);
 		ACCdestroy(2);
 		ACCclear(22);
 		ACCplus(30,4);
		{}

	}

	// DROP BALL
	p000e0190:
	{
 		if (skipdoall('p000e0190')) break p000e0190;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0190;
			if (!CNDnoun1(17)) break p000e0190;
 		}
 		ACCdrop(3);
		if (!success) break pro000_restart;
 		ACCwriteln(137);
		{}

	}

	// DROP BALL
	p000e0191:
	{
 		if (skipdoall('p000e0191')) break p000e0191;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0191;
			if (!CNDnoun1(17)) break p000e0191;
 		}
		if (!CNDat(18)) break p000e0191;
		if (!CNDpresent(42)) break p000e0191;
		if (!CNDnotcarr(3)) break p000e0191;
 		ACCwriteln(138);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// DROP BOOS
	p000e0192:
	{
 		if (skipdoall('p000e0192')) break p000e0192;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0192;
			if (!CNDnoun1(18)) break p000e0192;
 		}
 		ACCdrop(4);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP COMB
	p000e0193:
	{
 		if (skipdoall('p000e0193')) break p000e0193;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0193;
			if (!CNDnoun1(19)) break p000e0193;
 		}
 		ACCdrop(5);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP LADD
	p000e0194:
	{
 		if (skipdoall('p000e0194')) break p000e0194;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0194;
			if (!CNDnoun1(20)) break p000e0194;
 		}
 		ACCdrop(6);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP SULP
	p000e0195:
	{
 		if (skipdoall('p000e0195')) break p000e0195;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0195;
			if (!CNDnoun1(21)) break p000e0195;
 		}
		if (!CNDat(32)) break p000e0195;
		if (!CNDcarried(7)) break p000e0195;
 		ACCdestroy(51);
 		ACCdestroy(7);
 		ACCwriteln(139);
 		ACCplus(30,4);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP SULP
	p000e0196:
	{
 		if (skipdoall('p000e0196')) break p000e0196;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0196;
			if (!CNDnoun1(21)) break p000e0196;
 		}
 		ACCdrop(7);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP CRED
	p000e0197:
	{
 		if (skipdoall('p000e0197')) break p000e0197;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0197;
			if (!CNDnoun1(22)) break p000e0197;
 		}
 		ACCdrop(8);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP GLUE
	p000e0198:
	{
 		if (skipdoall('p000e0198')) break p000e0198;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0198;
			if (!CNDnoun1(23)) break p000e0198;
 		}
 		ACCdrop(9);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP DISP
	p000e0199:
	{
 		if (skipdoall('p000e0199')) break p000e0199;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0199;
			if (!CNDnoun1(24)) break p000e0199;
 		}
 		ACCdrop(10);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP MANU
	p000e0200:
	{
 		if (skipdoall('p000e0200')) break p000e0200;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0200;
			if (!CNDnoun1(25)) break p000e0200;
 		}
 		ACCdrop(11);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP DISR
	p000e0201:
	{
 		if (skipdoall('p000e0201')) break p000e0201;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0201;
			if (!CNDnoun1(27)) break p000e0201;
 		}
 		ACCdrop(13);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BATT
	p000e0202:
	{
 		if (skipdoall('p000e0202')) break p000e0202;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0202;
			if (!CNDnoun1(28)) break p000e0202;
 		}
 		ACCdrop(14);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP CAPE
	p000e0203:
	{
 		if (skipdoall('p000e0203')) break p000e0203;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0203;
			if (!CNDnoun1(29)) break p000e0203;
 		}
 		ACCdrop(15);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP CUFF
	p000e0204:
	{
 		if (skipdoall('p000e0204')) break p000e0204;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0204;
			if (!CNDnoun1(30)) break p000e0204;
 		}
 		ACCdrop(16);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BLOWPIPE
	p000e0205:
	{
 		if (skipdoall('p000e0205')) break p000e0205;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0205;
			if (!CNDnoun1(207)) break p000e0205;
 		}
 		ACCdrop(17);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BOOT
	p000e0206:
	{
 		if (skipdoall('p000e0206')) break p000e0206;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0206;
			if (!CNDnoun1(32)) break p000e0206;
 		}
 		ACCdrop(18);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP SPON
	p000e0207:
	{
 		if (skipdoall('p000e0207')) break p000e0207;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0207;
			if (!CNDnoun1(33)) break p000e0207;
 		}
 		ACCdrop(19);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BRAC
	p000e0208:
	{
 		if (skipdoall('p000e0208')) break p000e0208;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0208;
			if (!CNDnoun1(34)) break p000e0208;
 		}
 		ACCdrop(20);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP AIR
	p000e0209:
	{
 		if (skipdoall('p000e0209')) break p000e0209;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0209;
			if (!CNDnoun1(35)) break p000e0209;
 		}
 		ACCdrop(21);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP AQUA
	p000e0210:
	{
 		if (skipdoall('p000e0210')) break p000e0210;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0210;
			if (!CNDnoun1(36)) break p000e0210;
 		}
 		ACCdrop(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP NOTE
	p000e0211:
	{
 		if (skipdoall('p000e0211')) break p000e0211;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0211;
			if (!CNDnoun1(37)) break p000e0211;
 		}
 		ACCdrop(23);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP ELEC
	p000e0212:
	{
 		if (skipdoall('p000e0212')) break p000e0212;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0212;
			if (!CNDnoun1(38)) break p000e0212;
 		}
 		ACCdrop(24);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP SLI
	p000e0213:
	{
 		if (skipdoall('p000e0213')) break p000e0213;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0213;
			if (!CNDnoun1(39)) break p000e0213;
 		}
 		ACCdrop(25);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP PASS
	p000e0214:
	{
 		if (skipdoall('p000e0214')) break p000e0214;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0214;
			if (!CNDnoun1(40)) break p000e0214;
 		}
 		ACCdrop(26);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP FLAS
	p000e0215:
	{
 		if (skipdoall('p000e0215')) break p000e0215;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0215;
			if (!CNDnoun1(41)) break p000e0215;
 		}
 		ACCdrop(27);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BOMB
	p000e0216:
	{
 		if (skipdoall('p000e0216')) break p000e0216;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0216;
			if (!CNDnoun1(42)) break p000e0216;
 		}
 		ACCdrop(28);
		if (!success) break pro000_restart;
 		ACCwriteln(140);
		{}

	}

	// DROP BLAS
	p000e0217:
	{
 		if (skipdoall('p000e0217')) break p000e0217;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0217;
			if (!CNDnoun1(43)) break p000e0217;
 		}
 		ACCdrop(29);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP CLIP
	p000e0218:
	{
 		if (skipdoall('p000e0218')) break p000e0218;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0218;
			if (!CNDnoun1(44)) break p000e0218;
 		}
 		ACCdrop(30);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP PACK
	p000e0219:
	{
 		if (skipdoall('p000e0219')) break p000e0219;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0219;
			if (!CNDnoun1(45)) break p000e0219;
 		}
 		ACCdrop(31);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP GAS
	p000e0220:
	{
 		if (skipdoall('p000e0220')) break p000e0220;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0220;
			if (!CNDnoun1(65)) break p000e0220;
 		}
 		ACCdrop(12);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BADG
	p000e0221:
	{
 		if (skipdoall('p000e0221')) break p000e0221;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0221;
			if (!CNDnoun1(71)) break p000e0221;
 		}
 		ACCdrop(56);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP DISC
	p000e0222:
	{
 		if (skipdoall('p000e0222')) break p000e0222;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0222;
			if (!CNDnoun1(83)) break p000e0222;
 		}
 		ACCdrop(69);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO BOOS
	p000e0223:
	{
 		if (skipdoall('p000e0223')) break p000e0223;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0223;
			if (!CNDnoun1(18)) break p000e0223;
 		}
 		ACCremove(4);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO CAPE
	p000e0224:
	{
 		if (skipdoall('p000e0224')) break p000e0224;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0224;
			if (!CNDnoun1(29)) break p000e0224;
 		}
 		ACCremove(15);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO CUFF
	p000e0225:
	{
 		if (skipdoall('p000e0225')) break p000e0225;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0225;
			if (!CNDnoun1(30)) break p000e0225;
 		}
 		ACCremove(16);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO BOOT
	p000e0226:
	{
 		if (skipdoall('p000e0226')) break p000e0226;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0226;
			if (!CNDnoun1(32)) break p000e0226;
 		}
 		ACCremove(18);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO BRAC
	p000e0227:
	{
 		if (skipdoall('p000e0227')) break p000e0227;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0227;
			if (!CNDnoun1(34)) break p000e0227;
 		}
 		ACCremove(20);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO AIR
	p000e0228:
	{
 		if (skipdoall('p000e0228')) break p000e0228;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0228;
			if (!CNDnoun1(35)) break p000e0228;
 		}
 		ACCremove(21);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO AQUA
	p000e0229:
	{
 		if (skipdoall('p000e0229')) break p000e0229;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0229;
			if (!CNDnoun1(36)) break p000e0229;
 		}
 		ACCremove(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO ELEC
	p000e0230:
	{
 		if (skipdoall('p000e0230')) break p000e0230;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0230;
			if (!CNDnoun1(38)) break p000e0230;
 		}
 		ACCremove(24);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO BADG
	p000e0231:
	{
 		if (skipdoall('p000e0231')) break p000e0231;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0231;
			if (!CNDnoun1(71)) break p000e0231;
 		}
 		ACCremove(56);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR BOOS
	p000e0232:
	{
 		if (skipdoall('p000e0232')) break p000e0232;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0232;
			if (!CNDnoun1(18)) break p000e0232;
 		}
 		ACCwear(4);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR CAPE
	p000e0233:
	{
 		if (skipdoall('p000e0233')) break p000e0233;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0233;
			if (!CNDnoun1(29)) break p000e0233;
 		}
 		ACCwear(15);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR CUFF
	p000e0234:
	{
 		if (skipdoall('p000e0234')) break p000e0234;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0234;
			if (!CNDnoun1(30)) break p000e0234;
 		}
 		ACCwear(16);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR BOOT
	p000e0235:
	{
 		if (skipdoall('p000e0235')) break p000e0235;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0235;
			if (!CNDnoun1(32)) break p000e0235;
 		}
 		ACCwear(18);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR BRAC
	p000e0236:
	{
 		if (skipdoall('p000e0236')) break p000e0236;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0236;
			if (!CNDnoun1(34)) break p000e0236;
 		}
 		ACCwear(20);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR AIR
	p000e0237:
	{
 		if (skipdoall('p000e0237')) break p000e0237;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0237;
			if (!CNDnoun1(35)) break p000e0237;
 		}
		if (!CNDzero(29)) break p000e0237;
 		ACCwear(21);
		if (!success) break pro000_restart;
 		ACCclear(9);
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR AQUA
	p000e0238:
	{
 		if (skipdoall('p000e0238')) break p000e0238;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0238;
			if (!CNDnoun1(36)) break p000e0238;
 		}
 		ACCwear(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR ELEC
	p000e0239:
	{
 		if (skipdoall('p000e0239')) break p000e0239;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0239;
			if (!CNDnoun1(38)) break p000e0239;
 		}
 		ACCwear(24);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR BADG
	p000e0240:
	{
 		if (skipdoall('p000e0240')) break p000e0240;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0240;
			if (!CNDnoun1(71)) break p000e0240;
 		}
 		ACCwear(56);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// INVE _
	p000e0241:
	{
 		if (skipdoall('p000e0241')) break p000e0241;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0241;
 		}
 		ACCinven();
		break pro000_restart;
		{}

	}

	// LOOK _
	p000e0242:
	{
 		if (skipdoall('p000e0242')) break p000e0242;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0242;
 		}
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// QUIT _
	p000e0243:
	{
 		if (skipdoall('p000e0243')) break p000e0243;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0243;
 		}
 		ACCquit();
 		function anykey00006() 
		{
 		ACCturns();
 		ACCend();
		return;
		}
 		waitKey(anykey00006);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SAVE _
	p000e0244:
	{
 		if (skipdoall('p000e0244')) break p000e0244;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0244;
 		}
 		ACCsave();
		break pro000_restart;
		{}

	}

	// LOAD _
	p000e0245:
	{
 		if (skipdoall('p000e0245')) break p000e0245;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0245;
 		}
 		ACCload();
		break pro000_restart;
		{}

	}

	// ENTE TELE
	p000e0246:
	{
 		if (skipdoall('p000e0246')) break p000e0246;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0246;
			if (!CNDnoun1(13)) break p000e0246;
 		}
		if (!CNDat(98)) break p000e0246;
		if (!CNDworn(20)) break p000e0246;
		if (!CNDnotzero(25)) break p000e0246;
 		ACCgoto(99);
 		ACCdesc();
		break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE TELE
	p000e0247:
	{
 		if (skipdoall('p000e0247')) break p000e0247;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0247;
			if (!CNDnoun1(13)) break p000e0247;
 		}
		if (!CNDat(36)) break p000e0247;
		if (!CNDworn(20)) break p000e0247;
 		ACCwriteln(141);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE TELE
	p000e0248:
	{
 		if (skipdoall('p000e0248')) break p000e0248;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0248;
			if (!CNDnoun1(13)) break p000e0248;
 		}
		if (!CNDat(36)) break p000e0248;
		if (!CNDnotworn(20)) break p000e0248;
 		ACCwriteln(142);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE TELE
	p000e0249:
	{
 		if (skipdoall('p000e0249')) break p000e0249;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0249;
			if (!CNDnoun1(13)) break p000e0249;
 		}
		if (!CNDat(98)) break p000e0249;
		if (!CNDnotworn(20)) break p000e0249;
 		ACCwriteln(143);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE TELE
	p000e0250:
	{
 		if (skipdoall('p000e0250')) break p000e0250;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0250;
			if (!CNDnoun1(13)) break p000e0250;
 		}
		if (!CNDat(98)) break p000e0250;
		if (!CNDeq(25,0)) break p000e0250;
 		ACCwriteln(144);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE XXXX
	p000e0251:
	{
 		if (skipdoall('p000e0251')) break p000e0251;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0251;
			if (!CNDnoun1(110)) break p000e0251;
 		}
		if (!CNDat(19)) break p000e0251;
		if (!CNDpresent(44)) break p000e0251;
 		ACCcreate(11);
 		ACCdestroy(44);
 		ACCwriteln(145);
 		ACCpause(250);
 		function anykey00007() 
		{
 		ACCdesc();
		return;
 		ACCdone();
		return;
		}
 		waitKey(anykey00007);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// _ _
	p000e0252:
	{
 		if (skipdoall('p000e0252')) break p000e0252;
 		ACChook(146);
		if (done_flag) break pro000_restart;
		{}

	}

	// ENTE _
	p000e0253:
	{
 		if (skipdoall('p000e0253')) break p000e0253;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0253;
 		}
 		ACCwriteln(147);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ATTA _
	p000e0254:
	{
 		if (skipdoall('p000e0254')) break p000e0254;
 		if (in_response)
		{
			if (!CNDverb(115)) break p000e0254;
 		}
 		ACCwriteln(148);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HELP _
	p000e0255:
	{
 		if (skipdoall('p000e0255')) break p000e0255;
 		if (in_response)
		{
			if (!CNDverb(116)) break p000e0255;
 		}
 		ACCwriteln(149);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SCOR _
	p000e0256:
	{
 		if (skipdoall('p000e0256')) break p000e0256;
 		if (in_response)
		{
			if (!CNDverb(117)) break p000e0256;
 		}
 		ACCscore();
		{}

	}

	// CUNT _
	p000e0257:
	{
 		if (skipdoall('p000e0257')) break p000e0257;
 		if (in_response)
		{
			if (!CNDverb(200)) break p000e0257;
 		}
 		ACCwriteln(150);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TURN _
	p000e0258:
	{
 		if (skipdoall('p000e0258')) break p000e0258;
 		if (in_response)
		{
			if (!CNDverb(202)) break p000e0258;
 		}
 		ACCturns();
		{}

	}

	// READ _
	p000e0259:
	{
 		if (skipdoall('p000e0259')) break p000e0259;
 		if (in_response)
		{
			if (!CNDverb(205)) break p000e0259;
 		}
 		ACCwriteln(151);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// WASH _
	p000e0260:
	{
 		if (skipdoall('p000e0260')) break p000e0260;
 		if (in_response)
		{
			if (!CNDverb(206)) break p000e0260;
 		}
 		ACCwriteln(152);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0261:
	{
 		if (skipdoall('p000e0261')) break p000e0261;
 		ACChook(153);
		if (done_flag) break pro000_restart;
		{}

	}


}
}

function pro001()
{
process_restart=true;
pro001_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p001e0000:
	{
 		if (skipdoall('p001e0000')) break p001e0000;
 		ACChook(154);
		if (done_flag) break pro001_restart;
		{}

	}

	// _ _
	p001e0001:
	{
 		if (skipdoall('p001e0001')) break p001e0001;
		if (!CNDat(0)) break p001e0001;
 		ACCbclear(12,5);
		{}

	}

	// _ _
	p001e0002:
	{
 		if (skipdoall('p001e0002')) break p001e0002;
		if (!CNDislight()) break p001e0002;
 		ACClistobj();
 		ACClistnpc(getFlag(38));
		{}

	}


}
}

function pro002()
{
process_restart=true;
pro002_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p002e0000:
	{
 		if (skipdoall('p002e0000')) break p002e0000;
		if (!CNDeq(31,0)) break p002e0000;
		if (!CNDeq(32,0)) break p002e0000;
 		ACCability(4,4);
		{}

	}

	// _ _
	p002e0001:
	{
 		if (skipdoall('p002e0001')) break p002e0001;
 		ACChook(155);
		if (done_flag) break pro002_restart;
		{}

	}

	// _ _
	p002e0002:
	{
 		if (skipdoall('p002e0002')) break p002e0002;
		if (!CNDat(30)) break p002e0002;
		if (!CNDeq(27,0)) break p002e0002;
 		ACCset(26);
 		ACCwriteln(156);
 		ACCset(27);
		{}

	}

	// _ _
	p002e0003:
	{
 		if (skipdoall('p002e0003')) break p002e0003;
		if (!CNDnotzero(26)) break p002e0003;
 		ACCminus(26,1);
 		ACCwriteln(157);
		{}

	}

	// _ _
	p002e0004:
	{
 		if (skipdoall('p002e0004')) break p002e0004;
		if (!CNDeq(26,246)) break p002e0004;
 		ACCwriteln(158);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0005:
	{
 		if (skipdoall('p002e0005')) break p002e0005;
		if (!CNDeq(9,5)) break p002e0005;
 		ACCwriteln(159);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0006:
	{
 		if (skipdoall('p002e0006')) break p002e0006;
		if (!CNDat(32)) break p002e0006;
		if (!CNDabsent(51)) break p002e0006;
 		ACCwriteln(160);
 		ACCpause(150);
 		function anykey00008() 
		{
 		ACCgoto(33);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00008);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0007:
	{
 		if (skipdoall('p002e0007')) break p002e0007;
		if (!CNDat(107)) break p002e0007;
		if (!CNDpresent(21)) break p002e0007;
 		ACCwriteln(161);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0008:
	{
 		if (skipdoall('p002e0008')) break p002e0008;
		if (!CNDat(107)) break p002e0008;
		if (!CNDpresent(49)) break p002e0008;
		if (!CNDnotworn(22)) break p002e0008;
 		ACCwriteln(162);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0009:
	{
 		if (skipdoall('p002e0009')) break p002e0009;
		if (!CNDat(107)) break p002e0009;
		if (!CNDworn(22)) break p002e0009;
		if (!CNDabsent(21)) break p002e0009;
 		ACCwriteln(163);
		{}

	}

	// _ _
	p002e0010:
	{
 		if (skipdoall('p002e0010')) break p002e0010;
		if (!CNDat(62)) break p002e0010;
		if (!CNDpresent(65)) break p002e0010;
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0011:
	{
 		if (skipdoall('p002e0011')) break p002e0011;
		if (!CNDat(65)) break p002e0011;
		if (!CNDnotworn(24)) break p002e0011;
		if (!CNDpresent(40)) break p002e0011;
		if (!CNDchance(90)) break p002e0011;
 		ACCwriteln(164);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0012:
	{
 		if (skipdoall('p002e0012')) break p002e0012;
		if (!CNDat(65)) break p002e0012;
		if (!CNDworn(24)) break p002e0012;
		if (!CNDpresent(40)) break p002e0012;
 		ACCwriteln(165);
		{}

	}

	// _ _
	p002e0013:
	{
 		if (skipdoall('p002e0013')) break p002e0013;
		if (!CNDat(66)) break p002e0013;
		if (!CNDpresent(68)) break p002e0013;
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0014:
	{
 		if (skipdoall('p002e0014')) break p002e0014;
		if (!CNDatgt(98)) break p002e0014;
		if (!CNDzero(28)) break p002e0014;
 		ACCwriteln(166);
		{}

	}

	// _ _
	p002e0015:
	{
 		if (skipdoall('p002e0015')) break p002e0015;
		if (!CNDatgt(101)) break p002e0015;
		if (!CNDzero(28)) break p002e0015;
 		ACCwriteln(167);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0016:
	{
 		if (skipdoall('p002e0016')) break p002e0016;
		if (!CNDatgt(124)) break p002e0016;
 		ACCset(28);
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0017:
	{
 		if (skipdoall('p002e0017')) break p002e0017;
		if (!CNDat(18)) break p002e0017;
		if (!CNDnotcarr(3)) break p002e0017;
		if (!CNDpresent(42)) break p002e0017;
 		ACCwriteln(168);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0018:
	{
 		if (skipdoall('p002e0018')) break p002e0018;
		if (!CNDat(74)) break p002e0018;
		if (!CNDnotcarr(19)) break p002e0018;
		if (!CNDpresent(43)) break p002e0018;
 		ACCwriteln(169);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0019:
	{
 		if (skipdoall('p002e0019')) break p002e0019;
		if (!CNDat(74)) break p002e0019;
		if (!CNDcarried(19)) break p002e0019;
		if (!CNDpresent(43)) break p002e0019;
 		ACCwriteln(170);
		{}

	}

	// _ _
	p002e0020:
	{
 		if (skipdoall('p002e0020')) break p002e0020;
		if (!CNDat(74)) break p002e0020;
		if (!CNDcarried(19)) break p002e0020;
		if (!CNDpresent(43)) break p002e0020;
		if (!CNDpresent(16)) break p002e0020;
 		ACCwriteln(171);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0021:
	{
 		if (skipdoall('p002e0021')) break p002e0021;
		if (!CNDat(28)) break p002e0021;
		if (!CNDcarried(53)) break p002e0021;
		if (!CNDpresent(38)) break p002e0021;
 		ACCdestroy(38);
 		ACCwriteln(172);
 		ACCplus(30,4);
		{}

	}

	// _ _
	p002e0022:
	{
 		if (skipdoall('p002e0022')) break p002e0022;
		if (!CNDat(94)) break p002e0022;
		if (!CNDnotcarr(26)) break p002e0022;
 		ACCwriteln(173);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0023:
	{
 		if (skipdoall('p002e0023')) break p002e0023;
		if (!CNDat(94)) break p002e0023;
		if (!CNDcarried(26)) break p002e0023;
 		ACCwriteln(174);
		{}

	}

	// _ _
	p002e0024:
	{
 		if (skipdoall('p002e0024')) break p002e0024;
		if (!CNDat(58)) break p002e0024;
		if (!CNDworn(4)) break p002e0024;
		if (!CNDpresent(41)) break p002e0024;
 		ACCdestroy(41);
 		ACCdestroy(4);
 		ACCwriteln(175);
 		ACCplus(30,5);
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0025:
	{
 		if (skipdoall('p002e0025')) break p002e0025;
		if (!CNDat(58)) break p002e0025;
		if (!CNDnotworn(4)) break p002e0025;
		if (!CNDpresent(41)) break p002e0025;
 		ACCwriteln(176);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0026:
	{
 		if (skipdoall('p002e0026')) break p002e0026;
		if (!CNDat(120)) break p002e0026;
		if (!CNDnotcarr(29)) break p002e0026;
		if (!CNDpresent(48)) break p002e0026;
 		ACCwriteln(177);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0027:
	{
 		if (skipdoall('p002e0027')) break p002e0027;
		if (!CNDat(120)) break p002e0027;
		if (!CNDcarried(29)) break p002e0027;
		if (!CNDpresent(48)) break p002e0027;
 		ACCwriteln(178);
 		ACCdestroy(48);
		{}

	}

	// _ _
	p002e0028:
	{
 		if (skipdoall('p002e0028')) break p002e0028;
		if (!CNDat(28)) break p002e0028;
		if (!CNDpresent(53)) break p002e0028;
		if (!CNDpresent(38)) break p002e0028;
 		ACCwriteln(179);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0029:
	{
 		if (skipdoall('p002e0029')) break p002e0029;
		if (!CNDat(28)) break p002e0029;
		if (!CNDpresent(38)) break p002e0029;
		if (!CNDcarried(7)) break p002e0029;
 		ACCwriteln(180);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0030:
	{
 		if (skipdoall('p002e0030')) break p002e0030;
		if (!CNDat(16)) break p002e0030;
		if (!CNDcarried(8)) break p002e0030;
		if (!CNDpresent(37)) break p002e0030;
 		ACCwriteln(181);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0031:
	{
 		if (skipdoall('p002e0031')) break p002e0031;
		if (!CNDcarried(13)) break p002e0031;
		if (!CNDcarried(28)) break p002e0031;
 		ACCdestroy(13);
 		ACCdestroy(28);
 		ACCcreate(69);
 		ACCget(69);
		if (!success) break pro002_restart;
 		ACCwriteln(182);
		{}

	}

	// _ _
	p002e0032:
	{
 		if (skipdoall('p002e0032')) break p002e0032;
		if (!CNDat(18)) break p002e0032;
		if (!CNDpresent(42)) break p002e0032;
		if (!CNDcarried(3)) break p002e0032;
 		ACCwriteln(183);
		{}

	}

	// _ _
	p002e0033:
	{
 		if (skipdoall('p002e0033')) break p002e0033;
		if (!CNDat(54)) break p002e0033;
		if (!CNDcarried(10)) break p002e0033;
		if (!CNDpresent(52)) break p002e0033;
 		ACCwriteln(184);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0034:
	{
 		if (skipdoall('p002e0034')) break p002e0034;
		if (!CNDatlt(99)) break p002e0034;
		if (!CNDeq(5,1)) break p002e0034;
 		ACCwriteln(185);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0035:
	{
 		if (skipdoall('p002e0035')) break p002e0035;
		if (!CNDat(96)) break p002e0035;
		if (!CNDcarried(69)) break p002e0035;
 		ACCdestroy(69);
 		ACCwriteln(186);
 		ACCplus(30,10);
 		ACClet(5,11);
 		ACCset(25);
		{}

	}

	// _ _
	p002e0036:
	{
 		if (skipdoall('p002e0036')) break p002e0036;
		if (!CNDatlt(99)) break p002e0036;
		if (!CNDeq(5,5)) break p002e0036;
 		ACCwriteln(187);
		{}

	}

	// _ _
	p002e0037:
	{
 		if (skipdoall('p002e0037')) break p002e0037;
		if (!CNDatlt(99)) break p002e0037;
		if (!CNDeq(5,2)) break p002e0037;
 		ACCwriteln(188);
		{}

	}

	// _ _
	p002e0038:
	{
 		if (skipdoall('p002e0038')) break p002e0038;
		if (!CNDat(9)) break p002e0038;
		if (!CNDnotworn(15)) break p002e0038;
 		ACCwriteln(189);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0039:
	{
 		if (skipdoall('p002e0039')) break p002e0039;
		if (!CNDat(9)) break p002e0039;
		if (!CNDworn(15)) break p002e0039;
 		ACCwriteln(190);
		{}

	}

	// _ _
	p002e0040:
	{
 		if (skipdoall('p002e0040')) break p002e0040;
		if (!CNDat(71)) break p002e0040;
		if (!CNDnotworn(18)) break p002e0040;
 		ACCwriteln(191);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0041:
	{
 		if (skipdoall('p002e0041')) break p002e0041;
		if (!CNDat(71)) break p002e0041;
		if (!CNDworn(18)) break p002e0041;
 		ACCwriteln(192);
		{}

	}

	// _ _
	p002e0042:
	{
 		if (skipdoall('p002e0042')) break p002e0042;
		if (!CNDatgt(6)) break p002e0042;
		if (!CNDnotzero(29)) break p002e0042;
 		ACCwriteln(193);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0043:
	{
 		if (skipdoall('p002e0043')) break p002e0043;
		if (!CNDatgt(98)) break p002e0043;
 		ACCclear(29);
		{}

	}

	// _ _
	p002e0044:
	{
 		if (skipdoall('p002e0044')) break p002e0044;
		if (!CNDatgt(6)) break p002e0044;
		if (!CNDatlt(99)) break p002e0044;
		if (!CNDnotworn(21)) break p002e0044;
 		ACCset(29);
		{}

	}

	// _ _
	p002e0045:
	{
 		if (skipdoall('p002e0045')) break p002e0045;
		if (!CNDatlt(8)) break p002e0045;
		if (!CNDnotworn(21)) break p002e0045;
 		ACCwriteln(194);
 		ACCplus(9,1);
		{}

	}

	// _ _
	p002e0046:
	{
 		if (skipdoall('p002e0046')) break p002e0046;
		if (!CNDat(6)) break p002e0046;
		if (!CNDnotworn(56)) break p002e0046;
 		ACCwriteln(195);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0047:
	{
 		if (skipdoall('p002e0047')) break p002e0047;
		if (!CNDat(6)) break p002e0047;
		if (!CNDworn(56)) break p002e0047;
 		ACCwriteln(196);
		{}

	}

	// _ _
	p002e0048:
	{
 		if (skipdoall('p002e0048')) break p002e0048;
		if (!CNDat(105)) break p002e0048;
		if (!CNDeq(23,0)) break p002e0048;
 		ACCset(22);
 		ACCset(23);
 		ACCwriteln(197);
		{}

	}

	// _ _
	p002e0049:
	{
 		if (skipdoall('p002e0049')) break p002e0049;
		if (!CNDeq(22,250)) break p002e0049;
 		ACCwriteln(198);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0050:
	{
 		if (skipdoall('p002e0050')) break p002e0050;
		if (!CNDnotzero(22)) break p002e0050;
 		ACCminus(22,1);
 		ACCwriteln(199);
		{}

	}


}
}

last_process = 2;
// This file is (C) Carlos Sanchez 2014, released under the MIT license

// This function is called first by the start() function that runs when the game starts for the first time
var h_init = function()
{
}


// This function is called last by the start() function that runs when the game starts for the first time
var h_post =  function()
{
}

// This function is called when the engine tries to write any text
var h_writeText =  function (text)
{
	return text;
}

//This function is called every time the user types any order
var h_playerOrder = function(player_order)
{
	return player_order;
}

// This function is called every time a location is described, just after the location text is written
var h_description_init =  function ()
{
}

// This function is called every time a location is described, just after the process 1 is executed
var h_description_post = function()
{
}


// this function is called when the savegame object has been created, in order to be able to add more custom properties
var h_saveGame = function(savegame_object)
{
	return savegame_object;
}


// this function is called after the restore game function has restored the standard information in savegame, in order to restore any additional data included in a patched (by h_saveGame) savegame.
var h_restoreGame = function(savegame_object)
{
}

// this funcion is called before writing a message about player order beeing impossible to understand
var h_invalidOrder = function(player_order)
{
}

// this function is called when a sequence tag is found giving a chance for any hook library to provide a response
// tagparams receives the params inside the tag as an array  {XXXX|nn|mm|yy} => ['XXXX', 'nn', 'mm', 'yy']
var h_sequencetag = function (tagparams)
{
	return '';
}

// this function is called from certain points in the response or process tables via the HOOK condact. Depending on the string received it can do something or not.
// it's designed to allow direct javascript code to take control in the start database just installing a plugin library (avoiding the wirter need to enter code to activate the library)
var h_code = function(str)
{
	return false;
}


// this function is called from the keydown evente handler used by block and other functions to emulate a pause or waiting for a keypress. It is designed to allow plugin condacts or
// libraries to attend those key presses and react accordingly. In case a hook function decides that the standard keydown functions should not be processed, the hook function should return false.
// Also, any h_keydown replacement should probably do the same.
var h_keydown = function (event)
{
	return true;
}


// this function is called every time a process is called,  either by the internall loop of by the PROCESS condact, just before running it.
var h_preProcess = function(procno)
{

}

// this function is called every time a process is called just after the process exits (no matter which DONE status it has), either by the internall loop of by the PROCESS condact
var h_postProcess= function (procno)
{

}// This file is (C) Carlos Sanchez 2014, and is released under the MIT license
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function ACCdesc()
{
	describe_location_flag = true;
	ACCbreak(); // Cancel doall loop
}


function ACCdone()
{
	done_flag = true;
}

function CNDat(locno)
{
  return (loc_here()==locno);
}

function CNDnotat(locno)
{
	 return (loc_here()!=locno);
}


function CNDatgt(locno)
{
	 return (loc_here()>locno);
}


function CNDatlt(locno)
{
	 return (loc_here()<locno);
}

function CNDpresent(objno)
{
	var loc = getObjectLocation(objno);
	if (loc == loc_here()) return true;
	if (loc == LOCATION_WORN) return true;
	if (loc == LOCATION_CARRIED) return true;
	if ( (!bittest(getFlag(FLAG_PARSER_SETTINGS),7)) && (objectIsContainer(loc) || objectIsSupporter(loc))  &&  (loc<=last_object_number)  && (CNDpresent(loc)) )  // Extended context and object in another object that is present
	{
		if (objectIsSupporter(loc)) return true;  // On supporter
		if ( objectIsContainer(loc) && objectIsAttr(loc, ATTR_OPENABLE) && objectIsAttr(loc, ATTR_OPEN)) return true; // In a openable & open container
		if ( objectIsContainer(loc) && (!objectIsAttr(loc, ATTR_OPENABLE)) ) return true; // In a not openable container
	}
	return false;
}

function CNDabsent(objno)
{
	return !CNDpresent(objno);
}

function CNDworn(objno)
{
	return (getObjectLocation(objno) == LOCATION_WORN);
}

function CNDnotworn(objno)
{
	return !CNDworn(objno);
}

function CNDcarried(objno)
{
	return (getObjectLocation(objno) == LOCATION_CARRIED);	
}

function CNDnotcarr(objno)
{
	return !CNDcarried(objno);
}


function CNDchance(percent)
{
	 var val = Math.floor((Math.random()*101));
	 return (val<=percent);
}

function CNDzero(flagno)
{
	return (getFlag(flagno) == 0);
}

function CNDnotzero(flagno)
{
	 return !CNDzero(flagno)
}


function CNDeq(flagno, value)
{
	return (getFlag(flagno) == value);
}

function CNDnoteq(flagno,value)
{
	return !CNDeq(flagno, value);
}

function CNDgt(flagno, value)
{
	return (getFlag(flagno) > value);
}

function CNDlt(flagno, value)
{
	return (getFlag(flagno) < value);
}


function CNDadject1(wordno)
{
	return (getFlag(FLAG_ADJECT1) == wordno);
}

function CNDadverb(wordno)
{
	return (getFlag(FLAG_ADVERB) == wordno);
}


function CNDtimeout()
{
	 return bittest(getFlag(FLAG_TIMEOUT_SETTINGS),7);
}


function CNDisat(objno, locno)
{
	return (getObjectLocation(objno) == locno);

}


function CNDisnotat(objno, locno)
{
	return !CNDisat(objno, locno);
}



function CNDprep(wordno)
{
	return (getFlag(FLAG_PREP) == wordno);
}




function CNDnoun2(wordno)
{
	return (getFlag(FLAG_NOUN2) == wordno);
}

function CNDadject2(wordno)
{
	return (getFlag(FLAG_ADJECT2) == wordno);
}

function CNDsame(flagno1,flagno2)
{
	return (getFlag(flagno1) == getFlag(flagno2));
}


function CNDnotsame(flagno1,flagno2)
{
	return (getFlag(flagno1) != getFlag(flagno2));
}

function ACCinven()
{
	var count = 0;
	writeSysMessage(SYSMESS_YOUARECARRYING);
	ACCnewline();
	var listnpcs_with_objects = !bittest(getFlag(FLAG_PARSER_SETTINGS),3);
	var i;
	for (i=0;i<num_objects;i++)
	{
		if ((getObjectLocation(i)) == LOCATION_CARRIED)
		{
			
			if ((listnpcs_with_objects) || (!objectIsNPC(i)))
			{
				writeObject(i);
				if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
				ACCnewline();
				count++;
			}
		}
		if (getObjectLocation(i) == LOCATION_WORN)
		{
			if (listnpcs_with_objects || (!objectIsNPC(i)))
			{
				writeObject(i);
				writeSysMessage(SYSMESS_WORN);
				count++;
				ACCnewline();
			}
		}
	}
	if (!count) 
	{
		 writeSysMessage(SYSMESS_CARRYING_NOTHING);
		 ACCnewline();
	}

	if (!listnpcs_with_objects)
	{
		var numNPC = getNPCCountAt(LOCATION_CARRIED);
		if (numNPC)	ACClistnpc(LOCATION_CARRIED);
	}
	done_flag = true;
}

function desc()
{
	describe_location_flag = true;
}


function ACCquit()
{
	inQUIT = true;
	writeSysMessage(SYSMESS_AREYOUSURE);
}


function ACCend()
{
	$('.input').hide();
	inEND = true;
	writeSysMessage(SYSMESS_PLAYAGAIN);
	done_flag = true;
}


function done()
{
	done_flag = true;
}

function ACCok()
{
	writeSysMessage(SYSMESS_OK);
	done_flag = true;
}



function ACCramsave()
{
	ramsave_value = getSaveGameObject();
	var savegame_object = getSaveGameObject();	
	savegame =   JSON.stringify(savegame_object);
	localStorage.setItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME, savegame);
}

function ACCramload()
{
	if (ramsave_value==null) 
	{
		var json_str = localStorage.getItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME);
		if (json_str)
		{
			savegame_object = JSON.parse(json_str.trim());
			restoreSaveGameObject(savegame_object);
			ACCdesc();
			focusInput();
			return;
		}
		else
		{
			writeText (STR_RAMLOAD_ERROR);
			ACCnewline();
			done_flag = true;
			return;
		}
	}
	restoreSaveGameObject(ramsave_value);
	ACCdesc();
}

function ACCsave()
{
	var savegame_object = getSaveGameObject();	
	savegame =   JSON.stringify(savegame_object);
	filename = prompt(getSysMessageText(SYSMESS_SAVEFILE),'');
	if ( filename !== null ) localStorage.setItem('ngpaws_savegame_' + filename.toUpperCase(), savegame);
	ACCok();
}

 
function ACCload() 	
{
	var json_str;
	filename = prompt(getSysMessageText(SYSMESS_LOADFILE),'');
	if ( filename !== null ) json_str = localStorage.getItem('ngpaws_savegame_' + filename.toUpperCase());
	if (json_str)
	{
		savegame_object = JSON.parse(json_str.trim());
		restoreSaveGameObject(savegame_object);
	}
	else
	{
		writeSysMessage(SYSMESS_FILENOTFOUND);
		ACCnewline();
		done_flag = true; return;
	}
	ACCdesc();
	focusInput();
}



function ACCturns()
{
	var turns = getFlag(FLAG_TURNS_HIGH) * 256 +  getFlag(FLAG_TURNS_LOW);
	writeSysMessage(SYSMESS_TURNS_START);
	writeText(turns + '');
	writeSysMessage(SYSMESS_TURNS_CONTINUE);
	if (turns > 1) writeSysMessage(SYSMESS_TURNS_PLURAL);
	writeSysMessage(SYSMESS_TURNS_END);
}

function ACCscore()
{
	var score = getFlag(FLAG_SCORE);
	writeSysMessage(SYSMESS_SCORE_START);
	writeText(score + '');
	writeSysMessage(SYSMESS_SCORE_END);
}


function ACCcls()
{
	clearScreen();
}

function ACCdropall()
{
	// Done in two different loops cause PAW did it like that, just a question of retro compatibility
	var i;
	for (i=0;i<num_objects;i++)	if (getObjectLocation(i) == LOCATION_CARRIED)setObjectLocation(i, getFlag(FLAG_LOCATION));
	for (i=0;i<num_objects;i++)	if (getObjectLocation(i) == LOCATION_WORN)setObjectLocation(i, getFlag(FLAG_LOCATION));
}


function ACCautog()
{
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	if (!bittest(getFlag(FLAG_PARSER_SETTINGS),7))  // Extended context for objects
	for (var i=0; i<num_objects;i++) // Try to find it in present containers/supporters
	{
		if (CNDpresent(i) && (isAccesibleContainer(i) || objectIsAttr(i, ATTR_SUPPORTER)) )  // If there is another object present that is an accesible container or a supporter
		{
			objno =findMatchingObject(i);
			if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
		}
	}
	success = false;
	writeSysMessage(SYSMESS_CANTSEETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautod()
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };  
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautow()
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautor()
{
	var objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOURENOTWEARINGTHAT);
	ACCnewtext();
	ACCdone();
}



function ACCpause(value)
{
 if (value == 0) value = 256;
 pauseRemainingTime = Math.floor(value /50 * 1000);	
 inPause = true;
 showAnykeyLayer();
} 

function ACCgoto(locno)
{
 	setFlag(FLAG_LOCATION,locno);
}

function ACCmessage(mesno)
{
	writeMessage(mesno);
	ACCnewline();
}


function ACCremove(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_CARRIED:  
		case loc_here():
			writeSysMessage(SYSMESS_YOUARENOTWEARINGOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case LOCATION_WORN:
			if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
			{
				writeSysMessage(SYSMESS_CANTREMOVE_TOOMANYOBJECTS);
				ACCnewtext();
				ACCdone();
				return;
			}
			setObjectLocation(objno, LOCATION_CARRIED);
			writeSysMessage(SYSMESS_YOUREMOVEOBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUARENOTWEARINGTHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}


function trytoGet(objno)  // auxiliaty function for ACCget
{
	if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
	{
		writeSysMessage(SYSMESS_CANTCARRYANYMORE);
		ACCnewtext();
		ACCdone();
		doall_flag = false;
		return;
	}
	var weight = 0;
	weight += getObjectWeight(objno);
	weight +=  getLocationObjectsWeight(LOCATION_CARRIED);
	weight +=  getLocationObjectsWeight(LOCATION_WORN);
	if (weight > getFlag(FLAG_MAXWEIGHT_CARRIED))
	{
		writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
		ACCnewtext();
		ACCdone();
		return;
	}
	setObjectLocation(objno, LOCATION_CARRIED);
	writeSysMessage(SYSMESS_YOUTAKEOBJECT);
	success = true;
}


 function ACCget(objno)
 {
 	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_CARRIED:  
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():
			trytoGet(objno);
			break;

		default: 
			if  ((locno<=last_object_number) && (CNDpresent(locno)))    // If it's not here, carried or worn but it present, that means that bit 7 of flag 12 is cleared, thus you can get objects from present containers/supporters
			{
				trytoGet(objno);
			}
			else
			{
				writeSysMessage(SYSMESS_CANTSEETHAT);
				ACCnewtext();
				ACCdone();
				return;
				break;
		    }
	}
 }

function ACCdrop(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():  
			writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;


		case LOCATION_CARRIED:  
			setObjectLocation(objno, loc_here());
			writeSysMessage(SYSMESS_YOUDROPOBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}

function ACCwear(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUAREALREADYWAERINGOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():  
			writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;


		case LOCATION_CARRIED:  
			if (!objectIsWearable(objno))
			{
				writeSysMessage(SYSMESS_YOUCANTWEAROBJECT);
				ACCnewtext();
				ACCdone();
				return;
			}
			setObjectLocation(objno, LOCATION_WORN);
			writeSysMessage(SYSMESS_YOUWEAROBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}



function ACCdestroy(objno)
{
	setObjectLocation(objno, LOCATION_NONCREATED);
}


function ACCcreate(objno)
{
	setObjectLocation(objno, loc_here());
}


function ACCswap(objno1,objno2)
{
	var locno1 = getObjectLocation (objno1);
	var locno2 = getObjectLocation (objno2);
	ACCplace (objno1,locno2);
	ACCplace (objno2,locno1);
	setReferredObject(objno2);
}


function ACCplace(objno, locno)
{
	setObjectLocation(objno, locno);
}

function ACCset(flagno)
{
	setFlag(flagno, SET_VALUE);
}

function ACCclear(flagno)
{
	setFlag(flagno,0);
}

function ACCplus(flagno,value)
{
	var newval = getFlag(flagno) + value;
	setFlag(flagno, newval);
}

function ACCminus(flagno,value)
{
    var newval = getFlag(flagno) - value;
    if (newval < 0) newval = 0;
	setFlag(flagno, newval);
}

function ACClet(flagno,value)
{
	setFlag(flagno,value);
}

function ACCnewline()
{
	writeText(STR_NEWLINE);
}

function ACCprint(flagno)
{
	writeText(getFlag(flagno) +'');
}

function ACCsysmess(sysno)
{
	writeSysMessage(sysno);
}

function ACCcopyof(objno,flagno)
{
	setFlag(flagno, getObjectLocation(objno))
}

function ACCcopyoo(objno1, objno2)
{
	setObjectLocation(objno2,getObjectLocation(objno1));
	setReferredObject(objno2);
}

function ACCcopyfo(flagno,objno)
{
	setObjectLocation(objno, getFlag(flagno));
}

function ACCcopyff(flagno1, flagno2)
{
	setFlag(flagno2, getFlag(flagno1));
}

function ACCadd(flagno1, flagno2)
{
	var newval = getFlag(flagno1) + getFlag(flagno2);
	setFlag(flagno2, newval);
}

function ACCsub(flagno1,flagno2)
{
	var newval = getFlag(flagno2) - getFlag(flagno1);
	if (newval < 0) newval = 0;
	setFlag(flagno2, newval);
}


function CNDparse()
{
	return (!getLogicSentence());
}


function ACClistat(locno, container_objno)   // objno is a container/suppoter number, used to list contents of objects
{
  var listingContainer = false;
  if (arguments.length > 1) listingContainer = true;
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);  
  objscount = objscount - concealed_or_scenery_objcount;
  if (!listingContainer) setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),7)); 
  if (!objscount) return;
  var continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  if (listingContainer) 
  	{
  		writeText(' (');
  		if (objectIsAttr(container_objno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_OVER_YOUCANSEE); else if (objectIsAttr(container_objno, ATTR_CONTAINER)) writeSysMessage(SYSMESS_INSIDE_YOUCANSEE);
  		continouslisting = true;  // listing contents of container always continuous
  	}
  
  if (!listingContainer)
  {
    setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),7)); 
    if (!continouslisting) ACCnewline();
  }
  var progresscount = 0;
  for (var i=0;i<num_objects;i++)
  {
  	if (getObjectLocation(i) == locno)
  		if  ( ((!objectIsNPC(i)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)))  && (!objectIsAttr(i,ATTR_CONCEALED)) && (!objectIsAttr(i,ATTR_SCENERY))   ) // if not an NPC or parser setting say NPCs are considered objects, and object is not concealed nor scenery
  		  { 
  		     writeText(getObjectText(i)); 
  		     if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
  		     progresscount++
  		     if (continouslisting)
  		     {
		  			if (progresscount <= objscount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  					if (progresscount == objscount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
  					if (!listingContainer) if (progresscount == objscount ) writeSysMessage(SYSMESS_LISTEND);
  			 } else ACCnewline();
  		  }; 
  }
  if (arguments.length > 1) writeText(')');
}


function ACClistnpc(locno)
{
  var npccount =  getNPCCountAt(locno);
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),5)); 
  if (!npccount) return;
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),5)); 
  continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  writeSysMessage(SYSMESS_NPCLISTSTART);
  if (!continouslisting) ACCnewline();
  if (npccount==1)  writeSysMessage(SYSMESS_NPCLISTCONTINUE); else writeSysMessage(SYSMESS_NPCLISTCONTINUE_PLURAL);
  var progresscount = 0;
  var i;
  for (i=0;i<num_objects;i++)
  {
  	if (getObjectLocation(i) == locno)
  		if ( (objectIsNPC(i)) && (!objectIsAttr(i,ATTR_CONCEALED)) ) // only NPCs not concealed
  		  { 
  		     writeText(getObjectText(i)); 
  		     progresscount++
  		     if (continouslisting)
  		     {
		  	 	if (progresscount <= npccount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  			 	if (progresscount == npccount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
  			 	if (progresscount == npccount ) writeSysMessage(SYSMESS_LISTEND);
  			 } else ACCnewline();
  		  }; 
  }
}


function ACClistobj()
{
  var locno = loc_here();
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);

  objscount = objscount - concealed_or_scenery_objcount;
  if (objscount)
  {
	  writeSysMessage(SYSMESS_YOUCANSEE);
      ACClistat(loc_here());
  }
}

function ACCprocess(procno)
{
	if (procno > last_process) 
	{
		writeText(STR_WRONG_PROCESS);
		ACCnewtext();
		ACCdone();
	}
	callProcess(procno);
    if (describe_location_flag) done_flag = true;
}

function ACCmes(mesno)
{
	writeMessage(mesno);
}

function ACCmode(mode)
{
	setFlag(FLAG_MODE, mode);
}

function ACCtime(length, settings)
{
	setFlag(FLAG_TIMEOUT_LENGTH, length);
	setFlag(FLAG_TIMEOUT_SETTINGS, settings);
}

function ACCdoall(locno)
{
	doall_flag = true;
	if (locno == LOCATION_HERE) locno = loc_here();
	// Each object will be considered for doall loop if is at locno and it's not the object specified by the NOUN2/ADJECT2 pair and it's not a NPC (or setting to consider NPCs as objects is set)
	setFlag(FLAG_DOALL_LOC, locno);
	var doall_obj;
	doall_loop:
	for (doall_obj=0;(doall_obj<num_objects) && (doall_flag);doall_obj++)  
	{
		if (getObjectLocation(doall_obj) == locno)
			if ((!objectIsNPC(doall_obj)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3))) 
 			 if (!objectIsAttr(doall_obj, ATTR_CONCEALED)) 
 			  if (!objectIsAttr(doall_obj, ATTR_SCENERY)) 
				if (!( (objectsNoun[doall_obj]==getFlag(FLAG_NOUN2))  &&    ((objectsAdjective[doall_obj]==getFlag(FLAG_ADJECT2)) || (objectsAdjective[doall_obj]==EMPTY_WORD)) ) ) // implements "TAKE ALL EXCEPT BIG SWORD"
				{
					setFlag(FLAG_NOUN1, objectsNoun[doall_obj]);
					setFlag(FLAG_ADJECT1, objectsAdjective[doall_obj]);
					setReferredObject(doall_obj);
					callProcess(process_in_doall);
					if (describe_location_flag) 
						{
							doall_flag = false;
							entry_for_doall = '';
							break doall_loop;
						}
				}
	}
	doall_flag = false;
	entry_for_doall = '';
	if (describe_location_flag) descriptionLoop();
}

function ACCprompt(value)  // deprecated
{
	setFlag(FLAG_PROMPT, value);
	setInputPlaceHolder();
}


function ACCweigh(objno, flagno)
{
	var weight = getObjectWeight(objno);
	setFlag(flagno, weight);
}

function ACCputin(objno, locno)
{
	success = false;
	setReferredObject(objno);
	if (getObjectLocation(objno) == LOCATION_WORN)
	{
		writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == loc_here())
	{
		writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == LOCATION_CARRIED)
	{
		setObjectLocation(objno, locno);
		if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUPUTOBJECTON); else writeSysMessage(SYSMESS_YOUPUTOBJECTIN);
		writeText(getObjectFixArticles(locno));
		writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
		success = true;
		return;
	}

	writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
	ACCnewtext();
	ACCdone();
}


function ACCtakeout(objno, locno)
{
	success = false;
	setReferredObject(objno);
	if ((getObjectLocation(objno) == LOCATION_WORN) || (getObjectLocation(objno) == LOCATION_CARRIED))
	{
		writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == loc_here())
	{
		if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
		writeText(getObjectFixArticles(locno));
		writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectWeight(objno) + getLocationObjectsWeight(LOCATION_WORN) + getLocationObjectsWeight(LOCATION_CARRIED) >  getFlag(FLAG_MAXWEIGHT_CARRIED))
	{
		writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
	{		
		writeSysMessage(SYSMESS_CANTCARRYANYMORE);
		ACCnewtext();
		ACCdone();
		return;
	}

	setObjectLocation(objno, LOCATION_CARRIED);
	writeSysMessage(SYSMESS_YOUTAKEOBJECT);
	success = true;


}
function ACCnewtext()
{
	player_order_buffer = '';
}

function ACCability(maxObjectsCarried, maxWeightCarried)
{
	setFlag(FLAG_MAXOBJECTS_CARRIED, maxObjectsCarried);
	setFlag(FLAG_MAXWEIGHT_CARRIED, maxWeightCarried);
}

function ACCweight(flagno)
{
	var weight_carried = getLocationObjectsWeight(LOCATION_CARRIED);
	var weight_worn = getLocationObjectsWeight(LOCATION_WORN);
	var total_weight = weight_worn + weight_carried;
	setFlag(flagno, total_weight);
}


function ACCrandom(flagno)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*100)));
}

function ACCwhato()
{
	var whatofound = getReferredObject();
	if (whatofound != EMPTY_OBJECT) setReferredObject(whatofound);
}

function ACCputo(locno)
{
	setObjectLocation(getFlag(FLAG_REFERRED_OBJECT), locno);
}

function ACCnotdone()
{
	done_flag = false;
}

function ACCautop(locno)
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno = findMatchingObject(null); // anywhere
	if (objno != EMPTY_OBJECT) 
		{ 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return; 
		};

	success = false;
	writeSysMessage(SYSMESS_CANTDOTHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautot(locno)
{

	var objno =findMatchingObject(locno);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };

	objno = findMatchingObject(null); // anywhere
	if (objno != EMPTY_OBJECT) 
		{ 
			if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
			writeText(getObjectFixArticles(locno));
			writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION)
			ACCnewtext();
			ACCdone();
			return; 
		};

	success = false;
	writeSysMessage(SYSMESS_CANTDOTHAT);
	ACCnewtext();
	ACCdone();
	
}


function CNDmove(flagno)
{
	var locno = getFlag(flagno);
	var dirno = getFlag(FLAG_VERB);
	var destination = getConnection( locno,  dirno);
	if (destination != -1) 
		{
			 setFlag(flagno, destination);
			 return true;
		}
	return false;
}


function ACCextern(writeno)
{
	eval(writemessages[writeno]);
}


function ACCpicture(picno)
{
	drawPicture(picno);
}



function ACCgraphic(option)
{
	graphicsON = (option==1);  
	if (!graphicsON) hideGraphicsWindow();	
}

function ACCbeep(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'play');
}

function ACCsound(value)
{
	soundsON = (value==1);  
	if (!soundsON) sfxstopall();
}

function CNDozero(objno, attrno)
{
	if (attrno > 63) return false;
	return !objectIsAttr(objno, attrno);

}

function CNDonotzero(objno, attrno)
{
	return objectIsAttr(objno, attrno);
}

function ACCoset(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		attrs = getObjectLowAttributes(objno);
		var attrs = bitset(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitset(attrs, attrno);
	setObjectHighAttributes(objno, attrs);

}

function ACCoclear(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitclear(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitclear(attrs, attrno);
	setObjectHighAttributes(objno, attrs);

}


function CNDislight()
{
	if (!isDarkHere()) return true;
	return lightObjectsPresent();
}



function CNDisnotlight()
{
	return ! CNDislight();
}

function ACCversion()
{
	writeText(filterText(STR_RUNTIME_VERSION));
}


function ACCwrite(writeno)
{
	writeWriteMessage(writeno);
}

function ACCwriteln(writeno)
{
	writeWriteMessage(writeno);
	ACCnewline();
}

function ACCrestart()
{
  process_restart = true;
}


function ACCtranscript()
{
	$('#transcript_area').html(transcript);
	$('.transcript_layer').show();
	inTranscript = true;
}

function ACCanykey()
{
	writeSysMessage(SYSMESS_PRESSANYKEY);
	inAnykey = true;
}

function ACCgetkey(flagno)
{
	getkey_return_flag = flagno;
	inGetkey = true;
}


//////////////////
//   LEGACY     //
//////////////////

// From PAW PC
function ACCbell()
{
 	// Empty, PAW PC legacy, just does nothing 
}


// From PAW Spectrum
function ACCreset()
{
	// Legacy condact, does nothing now
}


function ACCpaper(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCink(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCborder(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCcharset(value)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCline(lineno)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCinput()
{
	// Legacy condact, does nothing now
}

function ACCsaveat()
{
	// Legacy condact, does nothing now
}

function ACCbackat()
{
	// Legacy condact, does nothing now
}

function ACCprintat()
{
	// Legacy condact, does nothing now
}

function ACCprotect()
{
	// Legacy condact, does nothing now
}

// From Superglus


function ACCdebug()
{
	// Legacy condact, does nothing now		
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS FOR COMPILER //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function CNDverb(wordno)
{
	return (getFlag(FLAG_VERB) == wordno);
}


function CNDnoun1(wordno)
{
	return (getFlag(FLAG_NOUN1) == wordno);
}

//   PLUGINS    ;

//CND RNDWRITELN A 14 14 14 0

function ACCrndwriteln(writeno1,writeno2,writeno3)
{
	ACCrndwrite(writeno1,writeno2,writeno3);
	ACCnewline();
}
//CND SYNONYM A 15 13 0 0

function ACCsynonym(wordno1, wordno2)
{
   if (wordno1!=EMPTY_WORD) setFlag(FLAG_VERB, wordno1);
   if (wordno2!=EMPTY_WORD)	setFlag(FLAG_NOUN1, wordno2);
}
//CND RNDWRITE A 14 14 14 0

function ACCrndwrite(writeno1,writeno2,writeno3)
{
	var val = Math.floor((Math.random()*3));
	switch (val)
	{
		case 0 : writeWriteMessage(writeno1);break;
		case 1 : writeWriteMessage(writeno2);break;
		case 2 : writeWriteMessage(writeno3);break;
	}
}
//CND SETWEIGHT A 4 2 0 0

function ACCsetweight(objno, value)
{
   objectsWeight[objno] = value;
}

//CND BSET A 1 2 0 0

function ACCbset(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitset(getFlag(flagno),bitno));
}
//CND ISNOTMOV C 0 0 0 0

function CNDisnotmov()
{
	return !CNDismov();	
}

//CND VOLUME A 2 2 0 0

function ACCvolume(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxvolume(channelno, value);
}

//CND ISVIDEO C 0 0 0 0

function CNDisvideo()
{
	if (typeof videoElement == 'undefined') return false;
	if (!videoLoopCount) return false;
	if (videoElement.paused) return false;
	return true;
}

//CND GE C 1 2 0 0

function CNDge(flagno, valor)
{
	return (getFlag(flagno)>=valor);
}
//CND ISNOTDOALL C 0 0 0 0

function CNDisnotdoall()
{
	return !CNDisdoall();
}

//CND RESUMEVIDEO A 0 0 0 0


function ACCresumevideo()
{
	if (typeof videoElement != 'undefined') 
		if (videoElement.paused)
		  videoElement.play();
}

//CND PAUSEVIDEO A 0 0 0 0


function ACCpausevideo()
{
	if (typeof videoElement != 'undefined') 
		if (!videoElement.ended) 
		if (!videoElement.paused)
		   videoElement.pause();
}

//CND BZERO C 1 2 0 0

function CNDbzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (!bittest(getFlag(flagno), bitno));
}
//CND BNEG A 1 2 0 0

function ACCbneg(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitneg(getFlag(flagno),bitno));
}
//CND ISDOALL C 0 0 0 0

function CNDisdoall()
{
	return doall_flag;	
}

//CND ONEG A 4 2 0 0

function ACConeg(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitneg(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitneg(attrs, attrno);
	setObjectHighAttributes(objno, attrs);
}

//CND ISDONE C 0 0 0 0

function CNDisdone()
{
	return done_flag;	
}

//CND BLOCK A 14 2 2 0

function ACCblock(writeno, picno, procno)
{
   inBlock = true;
   disableInterrupt();
   $('.block_layer').hide();
   var text = getWriteMessageText(writeno);
   $('.block_text').html(text);
   
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var imgsrc = '<img class="block_picture" src="' + filename + '" />';
		$('.block_graphics').html(imgsrc);
	}
    if (procno == 0 ) unblock_process = null; else unblock_process = procno;
    $('.block_layer').show();

}

//CND HELP A 0 0 0 0

function ACChelp()
{
	if (getLang()=='EN') EnglishHelp(); else SpanishHelp();
}	

function EnglishHelp()
{
	writeText('HOW DO I SEND COMMANDS TO THE PC?');
	writeText(STR_NEWLINE);
	writeText('Use simple orders: OPEN DOOR, TAKE KEY, GO UP, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I MOVE IN THE MAP?');
	writeText(STR_NEWLINE);
	writeText('Usually you will have to use compass directions as north (shortcut: "N"), south (S), east (E), west (W) or other directions (up, down, enter, leave, etc.). Some games allow complex order like "go to well". Usually you would be able to know avaliable exits by location description, some games also provide the "EXITS" command.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK MY INVENTORY?');
	writeText(STR_NEWLINE);
	writeText('type INVENTORY (shortcut "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I USE THE OBJECTS?');
	writeText(STR_NEWLINE);
	writeText('Use the proper verb, that is, instead of USE KEY type OPEN.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK SOMETHING CLOSELY?');
	writeText(STR_NEWLINE);
	writeText('Use "examine" verb: EXAMINE DISH. (shortcut: EX)');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SEE AGAIN THE CURRENT LOCATION DSCRIPTION?');
	writeText(STR_NEWLINE);
	writeText('Type LOOK (shortcut "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I TALK TO OTHER CHARACTERS?');
	writeText(STR_NEWLINE);
	writeText('Most common methods are [CHARACTER, SENTENCE] or [SAY CHARACTER "SENTENCE"]. For instance: [JOHN, HELLO] o [SAY JOHN "HELLO"]. Some games also allow just [TALK TO JOHN]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING IN A CONTAINER, HOW CAN I TAKE SOMETHING OUT?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY IN BOX. TAKE KEY OUT OF BOX. INSERT KEY IN BOX. EXTRACT KEY FROM BOX.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING ON SOMETHING ELSE?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY ON TABLE. TAKE KEY FROM TABLE');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SAVE/RESTORE MY GAME?');
	writeText(STR_NEWLINE);
	writeText('Use SAVE/LOAD commands.');
	writeText(STR_NEWLINE + STR_NEWLINE);

}

function SpanishHelp()
{
	writeText('¿CÓMO DOY ORDENES AL PERSONAJE?');
	writeText(STR_NEWLINE);
	writeText('Utiliza órdenes en imperativo o infinitivo: ABRE PUERTA, COGER LLAVE, SUBIR, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO ME MUEVO POR EL JUEGO?');
	writeText(STR_NEWLINE);
	writeText('Por regla general, mediante los puntos cardinales como norte (abreviado "N"), sur (S), este (E), oeste (O) o direcciones espaciales (arriba, abajo, bajar, subir, entrar, salir, etc.). Algunas aventuras permiten también cosas como "ir a pozo". Normalmente podrás saber en qué dirección puedes ir por la descripción del sitio, aunque algunos juegos facilitan el comando "SALIDAS" que te dirá exactamente cuáles hay.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO SABER QUE OBJETOS LLEVO?');
	writeText(STR_NEWLINE);
	writeText('Teclea INVENTARIO (abreviado "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO USO LOS OBJETOS?');
	writeText(STR_NEWLINE);
	writeText('Utiliza el verbo correcto, en lugar de USAR ESCOBA escribe BARRER.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO MIRAR DE CERCA UN OBJETO U OBSERVARLO MÁS DETALLADAMENTE?');
	writeText(STR_NEWLINE);
	writeText('Con el verbo examinar: EXAMINAR PLATO. Generalmente se puede usar la abreviatura "EX": EX PLATO.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO VER DE NUEVO LA DESCRIPCIÓN DEL SITIO DONDE ESTOY?');
	writeText(STR_NEWLINE);
	writeText('Escribe MIRAR (abreviado "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO HABLO CON LOS PERSONAJES?');
	writeText(STR_NEWLINE);
	writeText('Los modos más comunes son [PERSONAJE, FRASE] o [DECIR A PERSONAJE "FRASE"]. Por ejemplo: [LUIS, HOLA] o [DECIR A LUIS "HOLA"]. En algunas aventuras también se puede utilizar el formato [HABLAR A LUIS]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO METO ALGO EN UN CONTENEDOR? ¿CÓMO LO SACO?');
	writeText(STR_NEWLINE);
	writeText('METER LLAVE EN CAJA. SACAR LLAVE DE CAJA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PONGO ALGO SOBRE ALGO? ¿CÓMO LO QUITO?');
	writeText(STR_NEWLINE);
	writeText('PONER LLAVE EN MESA. COGER LLAVE DE MESA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO GRABO Y CARGO LA PARTIDA?');
	writeText(STR_NEWLINE);
	writeText('Usa las órdenes SAVE y LOAD, o GRABAR y CARGAR.');
	writeText(STR_NEWLINE + STR_NEWLINE);
}

//CND BNOTZERO C 1 2 0 0

function CNDbnotzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (bittest(getFlag(flagno), bitno));
}

//CND PLAYVIDEO A 14 2 2 0

var videoLoopCount;
var videoEscapable;
var videoElement;

function ACCplayvideo(strno, loopCount, settings)
{
	videoEscapable = settings & 1; // if bit 0 of settings is 1, video can be interrupted with ESC key
	if (loopCount == 0) loopCount = -1;
	videoLoopCount = loopCount;

	str = '<video id="videoframe" height="100%">';
	str = str + '<source src="dat/' + writemessages[strno] + '.mp4" type="video/mp4" codecs="avc1.4D401E, mp4a.40.2">';
	str = str + '<source src="dat/' + writemessages[strno] + '.webm" type="video/webm" codecs="vp8.0, vorbis">';
	str = str + '<source src="dat/' + writemessages[strno] + '.ogg" type="video/ogg" codecs="theora, vorbis">';
	str = str + '</video>';
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#videoframe').css('height','100%');
	$('#videoframe').css('display','block');
	$('#videoframe').css('margin-left','auto');
	$('#videoframe').css('margin-right','auto');
	$('#graphics').show();
	videoElement = document.getElementById('videoframe');
	videoElement.onended = function() 
	{
    	if (videoLoopCount == -1) videoElement.play();
    	else
    	{
    		videoLoopCount--;
    		if (videoLoopCount) videoElement.play();
    	}
	};
	videoElement.play();

}

// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_video_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#videoframe").length > 0) $("#videoframe").remove();	
	old_video_h_description_init();
}

// Hook into keypress to cancel video playing if ESC is pressed and video is skippable

var old_video_h_keydown =  h_keydown;
h_keydown = function (event)
{
 	if ((event.keyCode == 27) && (typeof videoElement != 'undefined') && (!videoElement.ended) && (videoEscapable)) 
 	{
 		videoElement.pause(); 
 		return false;  // we've finished attending ESC press
 	}
 	else return old_video_h_keydown(event);
}




//CND TEXTPIC A 2 2 0 0

function ACCtextpic(picno, align)
{
	var style = '';
	var post = '';
	var pre = '';
	switch(align)
	{
		case 0: post='<br style="clear:left">';break;
		case 1: style = 'float:left'; break;
		case 2: style = 'float:right'; break;
		case 3: pre='<center>';post='</center><br style="clear:left">';break;
	}
	filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var texto = pre + "<img alt='' class='textpic' style='"+style+"' src='"+filename+"' />" + post;
		writeText(texto);
		$(".text").scrollTop($(".text")[0].scrollHeight);
	}
}
//CND OBJFOUND C 2 9 0 0

function CNDobjfound(attrno, locno)
{

	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return true; }
	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return false;
}

//CND PICTUREAT A 2 2 2 0

/*
In order to determine the actual size of both background image and pictureat image they should be loaded, thus two chained "onload" are needed. That is, 
background image is loaded to determine its size, then pictureat image is loaded to determine its size. Size of currently displayed background image cannot
be used as it may have been already stretched.
*/

function ACCpictureat(x,y,picno)
{
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (!filename) return;

	// Check location has a picture, otherwise exit
	var currentBackgroundScreenImage = $('.location_picture');
	if (!currentBackgroundScreenImage) return;

	// Create a new image with the contents of current background image, to be able to calculate original height of image
	var virtualBackgroundImage = new Image();
	// Pass required data as image properties in order to be avaliable at "onload" event
	virtualBackgroundImage.bg_data=[];
	virtualBackgroundImage.bg_data.filename = filename; 
	virtualBackgroundImage.bg_data.x = x;
	virtualBackgroundImage.bg_data.y = y;
	virtualBackgroundImage.bg_data.picno = picno;
	virtualBackgroundImage.bg_data.currentBackgroundScreenImage = currentBackgroundScreenImage;


	// Event triggered when virtual background image is loaded
	virtualBackgroundImage.onload = function()
		{
			var originalBackgroundImageHeight = this.height;
			var scale = this.bg_data.currentBackgroundScreenImage.height() / originalBackgroundImageHeight;

			// Create a new image with the contents of picture to show with PICTUREAT, to be able to calculate height of image
			var virtualPictureAtImage = new Image();
			// Also pass data from background image as property so they are avaliable in the onload event
			virtualPictureAtImage.pa_data = [];
			virtualPictureAtImage.pa_data.x = this.bg_data.x;
			virtualPictureAtImage.pa_data.y = this.bg_data.y;
			virtualPictureAtImage.pa_data.picno = this.bg_data.picno;
			virtualPictureAtImage.pa_data.filename = this.bg_data.filename;
			virtualPictureAtImage.pa_data.scale = scale;
			virtualPictureAtImage.pa_data.currentBackgroundImageWidth = this.bg_data.currentBackgroundScreenImage.width();
			
			// Event triggered when virtual PCITUREAT image is loaded
			virtualPictureAtImage.onload = function ()
			{
		    		var imageHeight = this.height; 
					var x = Math.floor(this.pa_data.x * this.pa_data.scale);
					var y = Math.floor(this.pa_data.y * this.pa_data.scale);
					var newimageHeight = Math.floor(imageHeight * this.pa_data.scale);
					var actualBackgroundImageX = Math.floor((parseInt($('.graphics').width()) - this.pa_data.currentBackgroundImageWidth)/2);;
					var id = 'pictureat_' + this.pa_data.picno;

					// Add new image, notice we are not using the virtual image, but creating a new one
					$('.graphics').append('<img  alt="" id="'+id+'" style="display:none" />');				
					$('#' + id).css('position','absolute');
					$('#' + id).css('left', actualBackgroundImageX + x  + 'px');
					$('#' + id).css('top',y + 'px');
					$('#' + id).css('z-index','100');
					$('#' + id).attr('src', this.pa_data.filename);
					$('#' + id).css('height',newimageHeight + 'px');
					$('#' + id).show();
			}

			// Assign the virtual pictureat image the destinationsrc to trigger the "onload" event
			virtualPictureAtImage.src = this.bg_data.filename;
			};

	// Assign the virtual background image same src as current background to trigger the "onload" event
	virtualBackgroundImage.src = currentBackgroundScreenImage.attr("src");

}

//CND ISNOTRESP C 0 0 0 0

function CNDisnotresp()
{
	return !in_response;	
}

//CND ISSOUND C 1 0 0 0

function CNDissound(channelno)
{
	if ((channelno <1 ) || (channelno > MAX_CHANNELS)) return false;
    return channelActive(channelno);
}
//CND ZONE C 8 8 0 0

function CNDzone(locno1, locno2)
{

	if (loc_here()<locno1) return false;
	if (loc_here()>locno2) return false;
	return true;
}
//CND FADEOUT A 2 2 0 0

function ACCfadeout(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxfadeout(channelno, value);
}
//CND CLEAREXIT A 2 0 0 0

function ACCclearexit(wordno)
{
	if ((wordno >= NUM_CONNECTION_VERBS) || (wordno< 0 )) return;
	setConnection(loc_here(),wordno, -1);
}
//CND WHATOX2 A 1 0 0 0

function ACCwhatox2(flagno)
{	
	var auxNoun = getFlag(FLAG_NOUN1);
	var auxAdj = getFlag(FLAG_ADJECT1);
	setFlag(FLAG_NOUN1, getFlag(FLAG_NOUN2));
	setFlag(FLAG_ADJECT1, getFlag(FLAG_ADJECT2));
	var whatox2found = getReferredObject();
	setFlag(flagno,whatox2found);
	setFlag(FLAG_NOUN1, auxNoun);
	setFlag(FLAG_ADJECT1, auxAdj);
}
//CND COMMAND A 2 0 0 0

function ACCcommand(value)
{
	if (value) {$('.input').show();$('.input').focus();} else $('.input').hide();
}
//CND TITLE A 14 0 0 0

function ACCtitle(writeno)
{
	document.title = writemessages[writeno];
}
//CND LE C 1 2 0 0

function CNDle(flagno, valor)
{
	return (getFlag(flagno) <= valor);
}
//CND WARNINGS A 2 0 0 0

function ACCwarnings(value)
{
	if (value) showWarnings = true; else showWarnings = false;
}
//CND BCLEAR A 1 2 0 0

function ACCbclear(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitclear(getFlag(flagno), bitno));
}
//CND DIV A 1 2 0 0

function ACCdiv(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) / valor));
}
//CND OBJAT A 9 1 0 0

function ACCobjat(locno, flagno)
{
	setFlag(flagno, getObjectCountAt(locno));
}
//CND SILENCE A 2 0 0 0

function ACCsilence(channelno)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxstop(channelno);
}
//CND SETEXIT A 2 2 0 0

function ACCsetexit(value, locno)
{
	if (value < NUM_CONNECTION_VERBS) setConnection(loc_here(), value, locno);
}
//CND EXITS A 8 5 0 0

function ACCexits(locno,mesno)
{
  writeText(getExitsText(locno,mesno));
}

//CND HOOK A 14 0 0 5

function ACChook(writeno)
{
	h_code(writemessages[writeno]);
}
//CND RANDOMX A 1 2 0 0

function ACCrandomx(flagno, value)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*value)));
}
//CND ISNOTDONE C 0 0 0 0

function CNDisnotdone()
{
	return !CNDisdone();
}

//CND ATGE C 8 0 0 0

function CNDatge(locno)
{
	return (getFlag(FLAG_LOCATION) >= locno);
}

//CND ATLE C 8 0 0 0

function CNDatle(locno)
{
	return (getFlag(FLAG_LOCATION) <= locno);
}

//CND RESP A 0 0 0 0

function ACCresp()
{
	in_response = true;
}	

//CND ISNOTSOUND C 1 0 0 0

function CNDisnotsound(channelno)
{
  if ((channelno <1) || (channelno >MAX_CHANNELS)) return false;
  return !(CNDissound(channelno));
}
//CND ASK W 14 14 1 0

// Global vars for ASK


var inAsk = false;
var ask_responses = null;
var ask_flagno = null;



function ACCask(writeno, writenoOptions, flagno)
{
	inAsk = true;
	writeWriteMessage(writeno);
	ask_responses = getWriteMessageText(writenoOptions);
	ask_flagno = flagno;
}



// hook replacement
var old_ask_h_keydown  = h_keydown;
h_keydown  = function (event)
{
	if (inAsk)
	{
		var keyCodeAsChar = String.fromCharCode(event.keyCode).toLowerCase();
		if (ask_responses.indexOf(keyCodeAsChar)!= -1)
		{
			setFlag(ask_flagno, ask_responses.indexOf(keyCodeAsChar));
			inAsk = false;
			event.preventDefault();
            $('.input').show();
		    $('.input').focus();
		    hideBlock();
			waitKeyCallback();
		};
		return false; // if we are in ASK condact, no keypress should be considered other than ASK response
	} else return old_ask_h_keydown(event);
}

//CND MUL A 1 2 0 0

function ACCmul(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) * valor));
}
//CND NPCAT A 9 1 0 0

function ACCnpcat(locno, flagno)
{
	setFlag(flagno,getNPCCountAt(locno));
}

//CND SOFTBLOCK A 2 0 0 0

function ACCsoftblock(procno)
{
   inBlock = true;
   disableInterrupt();

   $('.block_layer').css('display','none');
   $('.block_text').html('');
   $('.block_graphics').html('');
   $('.block_layer').css('background','transparent');
   if (procno == 0 ) unblock_process = null; else unblock_process = procno;
   $('.block_layer').css('display','block');
}

//CND LISTCONTENTS A 9 0 0 0

function ACClistcontents(locno)
{
   ACClistat(locno, locno)
}
//CND SPACE A 0 0 0 0

function ACCspace()
{
	writeText(' ');
}
//CND ISNOTMUSIC C 0 0 0 0

function CNDisnotmusic()
{
  return !CNDismusic();
}

//CND BREAK A 0 0 0 0

function ACCbreak()
{
	doall_flag = false; 
	entry_for_doall = '';
}
//CND NORESP A 0 0 0 0

function ACCnoresp()
{
	in_response = false;
}	

//CND ISMUSIC C 0 0 0 0

function CNDismusic()
{
	return (CNDissound(0));	
}

//CND ISRESP C 0 0 0 0

function CNDisresp()
{
	return in_response;	
}

//CND MOD A 1 2 0 0

function ACCmod(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) % valor));
}
//CND FADEIN A 2 2 2 0

function ACCfadein(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'fadein');
}
//CND WHATOX A 1 0 0 0

function ACCwhatox(flagno)
{
	var whatoxfound = getReferredObject();
	setFlag(flagno,whatoxfound);
}

//CND GETEXIT A 2 2 0 0

function ACCgetexit(value,flagno)
{
	if (value >= NUM_CONNECTION_VERBS) 
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	var locno = getConnection(loc_here(),value);
	if (locno == -1)
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	setFlag(flagno,locno);
}
//CND LOG A 14 0 0 0

function ACClog(writeno)
{
  console_log(writemessages[writeno]);
}
//CND VOLUMEVIDEO A 2 0 0 0


function ACCvolumevideo(value)
{
	if (typeof videoElement != 'undefined') 
		videoElement.volume = value  / 65535;
}

//CND OBJNOTFOUND C 2 9 0 0

function CNDobjnotfound(attrno, locno)
{
	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return false; }

	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return true;
}
//CND ISMOV C 0 0 0 0

function CNDismov()
{
	if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)==EMPTY_WORD)) return true;

	if ((getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_VERB)==EMPTY_WORD)) return true;

    if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS)) return true;
    
    return false;
}

//CND YOUTUBE A 14 0 0 0

function ACCyoutube(strno)
{

	var str = '<iframe id="youtube" width="560" height="315" src="http://www.youtube.com/embed/' + writemessages[strno] + '?autoplay=1&controls=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>'
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#youtube').css('height','100%');
	$('#youtube').css('display','block');
	$('#youtube').css('margin-left','auto');
	$('#youtube').css('margin-right','auto');
	$('.graphics').show();
}


// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_youtube_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#youtube").length > 0) $("#youtube").remove();	
	old_youtube_h_description_init();
}
//CND LISTSAVEDGAMES A 0 0 0 0

function ACClistsavedgames()
{
  var numberofgames = 0;
  for(var savedgames in localStorage)
  {
    gamePrefix = savedgames.substring(0,16); // takes out ngpaws_savegame_
    if (gamePrefix == "ngpaws_savegame_")
    {
      gameName = savedgames.substring(16);
      writelnText(gameName);
      numberofgames++;
    }
  }
  if (numberofgames == 0) 
  {
     if (getLang()=='EN') writelnText("No saved games found."); else writelnText("No hay ninguna partida guardada.");
  }
}


// This file is (C) Carlos Sanchez 2014, released under the MIT license


// IMPORTANT: Please notice this file must be encoded with the same encoding the index.html file is, so the "normalize" function works properly.
//            As currently the ngpwas compiler generates utf-8, and the index.html is using utf-8 also, this file must be using that encoding.


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                         Auxiliary functions                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General functions
String.prototype.rights= function(n){
    if (n <= 0)
       return "";
    else if (n > String(this).length)
       return this;
    else {
       var iLen = String(this).length;
       return String(this).substring(iLen, iLen - n);
    }
}


String.prototype.firstToLower= function()
{
	return  this.charAt(0).toLowerCase() + this.slice(1);	
}


// Returns true if using Internet Explorer 9 or below, where some features are not supported
function isBadIE () {
  var myNav = navigator.userAgent.toLowerCase();
  if (myNav.indexOf('msie') == -1) return false;
  ieversion =  parseInt(myNav.split('msie')[1]);
  return (ieversion<10);
}


function runningLocal()
{
	return (window.location.protocol == 'file:');
}


// Levenshtein function

function getLevenshteinDistance (a, b)
{
  if(a.length == 0) return b.length; 
  if(b.length == 0) return a.length; 
 
  var matrix = [];
 
  // increment along the first column of each row
  var i;
  for(i = 0; i <= b.length; i++){
    matrix[i] = [i];
  }
 
  // increment each column in the first row
  var j;
  for(j = 0; j <= a.length; j++){
    matrix[0][j] = j;
  }
 
  // Fill in the rest of the matrix
  for(i = 1; i <= b.length; i++){
    for(j = 1; j <= a.length; j++){
      if(b.charAt(i-1) == a.charAt(j-1)){
        matrix[i][j] = matrix[i-1][j-1];
      } else {
        matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                                Math.min(matrix[i][j-1] + 1, // insertion
                                         matrix[i-1][j] + 1)); // deletion
      }
    }
  }
 
  return matrix[b.length][a.length];
};

// waitKey helper for all key-wait condacts

function waitKey(callbackFunction)
{
	waitkey_callback_function.push(callbackFunction);
	showAnykeyLayer();
}

function waitKeyCallback()
{
 	var callback = waitkey_callback_function.pop();
	if ( callback ) callback();
	if (describe_location_flag) descriptionLoop();  		
}


// Check DOALL entry

function skipdoall(entry)
{
	return  ((doall_flag==true) && (entry_for_doall!='') && (current_process==process_in_doall) && (entry_for_doall > entry));
}

// Dynamic attribute use functions
function getNextFreeAttribute()
{
	var value = nextFreeAttr;
	nextFreeAttr++;
	return value;
}


// Gender functions

function getSimpleGender(objno)  // Simple, for english
{
 	isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
 	if (isPlural) return "P";
 	isFemale = objectIsAttr(objno, ATTR_FEMALE);
 	if (isFemale) return "F";
 	isMale = objectIsAttr(objno, ATTR_MALE);
 	if (isMale) return "M";
    return "N"; // Neuter
}

function getAdvancedGender(objno)  // Complex, for spanish
{
 	var isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
 	var isFemale = objectIsAttr(objno, ATTR_FEMALE);
 	var isMale = objectIsAttr(objno, ATTR_MALE);

 	if (!isPlural) 
 	{
	 	if (isFemale) return "F";
	 	if (isMale) return "M";
	    return "N"; // Neuter
 	}
 	else
 	{
	 	if (isFemale) return "PF";
	 	if (isMale) return "PM";
	 	return "PN"; // Neuter plural
 	}

}

function getLang()
{
	var value = bittest(getFlag(FLAG_PARSER_SETTINGS),5);
	if (value) return "ES"; else return "EN";
}

function getObjectFixArticles(objno)
{
	var object_text = getObjectText(objno);
	var object_words = object_text.split(' ');
	if (object_words.length == 1) return object_text;
	var candidate = object_words[0];
	object_words.splice(0, 1);
	if (getLang()=='EN')
	{
		if ((candidate!='an') && (candidate!='a') && (candidate!='some')) return object_text;
		return 'the ' + object_words.join(' ');
	}
	else
	{
		if ( (candidate!='un') && (candidate!='una') && (candidate!='unos') && (candidate!='unas') && (candidate!='alguna') && (candidate!='algunos') && (candidate!='algunas') && (candidate!='algun')) return object_text;
		var gender = getAdvancedGender(objno);
		if (gender == 'F') return 'la ' + object_words.join(' ');
		if (gender == 'M') return 'el ' + object_words.join(' ');
		if (gender == 'N') return 'el ' + object_words.join(' ');
		if (gender == 'PF') return 'las ' + object_words.join(' ');
		if (gender == 'PM') return 'los ' + object_words.join(' ');
		if (gender == 'PN') return 'los ' + object_words.join(' ');
	}	


}



// JS level log functions
function console_log(string)
{
	if (typeof console != "undefined") console.log(string);
}


// Resources functions
function getResourceById(resource_type, id)
{
	for (var i=0;i<resources.length;i++)
	 if ((resources[i][0] == resource_type) && (resources[i][1]==id)) return resources[i][2];
	return false; 
}

// Flag read/write functions
function getFlag(flagno)
{
	 return flags[flagno];
}

function setFlag(flagno, value)
{
	 flags[flagno] = value;
}

// Locations functions
function loc_here()  // Returns current location, avoid direct use of flags
{
	 return getFlag(FLAG_LOCATION);
}


// Connections functions

function setConnection(locno1, dirno, locno2)
{
	connections[locno1][dirno] = locno2;
}

function getConnection(locno, dirno)
{
	return connections[locno][dirno];
}

// Objects text functions

function getObjectText(objno)
{
	return filterText(objects[objno]);
}


// Message text functions
function getMessageText(mesno)
{
	return filterText(messages[mesno]);
}

function getSysMessageText(sysno)
{
	return filterText(sysmessages[sysno]);
}

function getWriteMessageText(writeno)
{
	return filterText(writemessages[writeno]);
}

function getExitsText(locno,mesno)
{
  if ( locno === undefined ) return ''; // game hasn't fully initialised yet
  if ((getFlag(FLAG_LIGHT) == 0) || ((getFlag(FLAG_LIGHT) != 0) && lightObjectsPresent()))
  {
  		var exitcount = 0;
  		for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1) exitcount++;
      if (exitcount)
      {
    		var message = getMessageText(mesno);
    		var exitcountprogress = 0;
    		for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1)
    		{ 
    			exitcountprogress++;
    			message += getMessageText(mesno + 2 + i);
    			if (exitcountprogress == exitcount) message += getSysMessageText(SYSMESS_LISTEND);
    			if (exitcountprogress == exitcount-1) message += getSysMessageText(SYSMESS_LISTLASTSEPARATOR);
    			if (exitcountprogress <= exitcount-2) message += getSysMessageText(SYSMESS_LISTSEPARATOR);
  		  }
  		  return message;
      } else return getMessageText(mesno + 1);
  } else return getMessageText(mesno + 1);
}


// Location text functions
function getLocationText(locno)
{
	return  filterText(locations[locno]);
}



// Output processing functions
function implementTag(tag)
{
	tagparams = tag.split('|');
	for (var tagindex=0;tagindex<tagparams.length-1;tagindex++) tagparams[tagindex] = tagparams[tagindex].trim();
	if (tagparams.length == 0) {writeWarning(STR_INVALID_TAG_SEQUENCE_EMPTY); return ''}

	var resolved_hook_value = h_sequencetag(tagparams);
	if (resolved_hook_value!='') return resolved_hook_value;

	switch(tagparams[0].toUpperCase())
	{
		case 'URL': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					return '<a target="newWindow" href="' + tagparams[1]+ '">' + tagparams[2] + '</a>'; // Note: _blank would get the underscore character replaced by current selected object so I prefer to use a different target name as most browsers will open a new window
					break;
		case 'CLASS': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span class="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'STYLE': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'INK': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'PAPER': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="background-color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'OBJECT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(objects[getFlag(tagparams[1])]) return getObjectFixArticles(getFlag(tagparams[1])); else return '';
					   break;
		case 'WEIGHT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(objectsWeight[getFlag(tagparams[1])]) return objectsWeight[getFlag(tagparams[1])]; else return '';
					   break;
		case 'OLOCATION': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					      if(objectsLocation[getFlag(tagparams[1])]) return objectsLocation[getFlag(tagparams[1])]; else return '';
					      break;
		case 'MESSAGE':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(messages[getFlag(tagparams[1])]) return getMessageText(getFlag(tagparams[1])); else return '';
					   break;
		case 'SYSMESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(sysmessages[getFlag(tagparams[1])]) return getSysMessageText(getFlag(tagparams[1])); else return '';
					   break;
		case 'LOCATION':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(locations[getFlag(tagparams[1])]) return getLocationText(getFlag(tagparams[1])); else return '';
		case 'EXITS':if (tagparams.length != 3 ) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   return getExitsText(/^@\d+/.test(tagparams[1]) ? getFlag(tagparams[1].substr(1)) : tagparams[1],parseInt(tagparams[2],10));
					   break;
		case 'PROCESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   callProcess(tagparams[1]);
					   return "";
					   break;
		case 'ACTION': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   return '<a href="type: ' + tagparams[1] + '" onmouseup="orderEnteredLoop(\'' + tagparams[1]+ '\');return false;">' + tagparams[2] + '</a>';
					   break;
		case 'RESTART': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					    return '<a href="javascript: void(0)" onmouseup="restart()">' + tagparams[1] + '</a>';
					    break;
		case 'EXTERN': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					    return '<a href="javascript: void(0)" onmouseup="' + tagparams[1] + ' ">' + tagparams[2] + '</a>';
					    break;
		case 'TEXTPIC': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						var style = '';
						var post = '';
						var pre = '';
						align = tagparams[2];
						switch(align)
						{
							case 1: style = 'float:left'; break;
							case 2: style = 'float:right'; break;
							case 3: post = '<br />';
							case 4: pre='<center>';post='</center>';break;
						}
						return pre + "<img class='textpic' style='"+style+"' src='"+ RESOURCES_DIR + tagparams[1]+"' />" + post;
					    break;
		case 'HTML': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						return tagparams[1];
					    break;
		case 'FLAG': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						return getFlag(tagparams[1]);
					    break;
		case 'OREF': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
   			        if(objects[getFlag(FLAG_REFERRED_OBJECT)]) return getObjectFixArticles(getFlag(FLAG_REFERRED_OBJECT)); else return '';
					break;
		case 'TT':  
		case 'TOOLTIP':
					if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					var title = $('<span>'+tagparams[1]+'</span>').text().replace(/'/g,"&apos;").replace(/\n/g, "&#10;");
					var text = tagparams[2];
					return "<span title='"+title+"'>"+text+"</span>";
					break;
		case 'OPRO': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};  // returns the pronoun for a given object, used for english start database
					 switch (getSimpleGender(getFlag(FLAG_REFERRED_OBJECT)))
					 {
					 	case 'M' : return "him";
					 	case "F" : return "her";
					 	case "N" : return "it";
					 	case "P" : return "them";  // plural returns them
					 }
					break;

		default : return '[[[' + STR_INVALID_TAG_SEQUENCE_BADTAG + ' : ' + tagparams[0] + ']]]';
	}
}

function processTags(text)
{
	//Apply the {} tags filtering
	var pre, post, innerTag;
	tagfilter:
	while (text.indexOf('{') != -1)
	{
		if (( text.indexOf('}') == -1 ) || ((text.indexOf('}') < text.indexOf('{'))))
		{
			writeWarning(STR_INVALID_TAG_SEQUENCE + text);
			break tagfilter;
		}
		pre = text.substring(0,text.indexOf('{'));
		var openbracketcont = 1;
		pointer = text.indexOf('{') + 1;
		innerTag = ''
		while (openbracketcont>0)
		{
			if (text.charAt(pointer) == '{') openbracketcont++;
			if (text.charAt(pointer) == '}') openbracketcont--;
			if ( text.length <= pointer )
			{
				writeWarning(STR_INVALID_TAG_SEQUENCE + text);
				break tagfilter;
			}
			innerTag = innerTag + text.charAt(pointer);
			pointer++;
		}
		innerTag = innerTag.substring(0,innerTag.length - 1);
		post = text.substring(pointer);
		if (innerTag.indexOf('{') != -1 ) innerTag = processTags(innerTag); 
		innerTag = implementTag(innerTag);
		text = pre + innerTag + post;
	}
	return text;
}

function filterText(text)
{
	// ngPAWS sequences
	text = processTags(text);


	// Superglus sequences (only \n remains)
    text = text.replace(/\n/g, STR_NEWLINE);

	// PAWS sequences (only underscore)
	objno = getFlag(FLAG_REFERRED_OBJECT);
	if ((objno != EMPTY_OBJECT) && (objects[objno]))	text = text.replace(/_/g,objects[objno].firstToLower()); else text = text.replace(/_/g,'');
	text = text.replace(/¬/g,' ');

	return text;
}


// Text Output functions
function writeText(text, skipAutoComplete)
{
	if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
	text = h_writeText(text); // hook
	$('.text').append(text);
	$('.text').scrollTop($('.text')[0].scrollHeight);
	addToTranscript(text);
	if (!skipAutoComplete) addToAutoComplete(text);
	focusInput();
}

function writeWarning(text)
{
	if (showWarnings) writeText(text)
}

function addToTranscript(text)
{
	transcript = transcript + text;		
}

function writelnText(text, skipAutoComplete)
{
	if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
	writeText(text + STR_NEWLINE, skipAutoComplete);
}

function writeMessage(mesno)
{
	if (messages[mesno]!=null) writeText(getMessageText(mesno)); else writeWarning(STR_NEWLINE + STR_WRONG_MESSAGE + ' [' + mesno + ']');
}

function writeSysMessage(sysno)
{
		if (sysmessages[sysno]!=null) writeText(getSysMessageText(sysno)); else writeWarning(STR_NEWLINE + STR_WRONG_SYSMESS + ' [' + sysno + ']');
		$(".text").scrollTop($(".text")[0].scrollHeight);
}

function writeWriteMessage(writeno)
{
		writeText(getWriteMessageText(writeno)); 
}

function writeObject(objno)
{
	writeText(getObjectText(objno));
}

function clearTextWindow()
{
	$('.text').empty();
}


function clearInputWindow()
{
	$('.prompt').val('');
}


function writeLocation(locno)
{
	if (locations[locno]!=null) writeText(getLocationText(locno) + STR_NEWLINE); else writeWarning(STR_NEWLINE + STR_WRONG_LOCATION + ' [' + locno + ']');
}

// Screen control functions

function clearGraphicsWindow()
{
	$('.graphics').empty();	
}


function clearScreen()
{
	clearInputWindow();
	clearTextWindow();
	clearGraphicsWindow();
}

function copyOrderToTextWindow(player_order)
{

	last_player_orders.push(player_order);
	last_player_orders_pointer = 0;
	clearInputWindow();
	writelnText(STR_PROMPT_START + player_order + STR_PROMPT_END, false);
}

function get_prev_player_order()
{
	if (!last_player_orders.length) return '';
	var last = last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];
	if (last_player_orders_pointer < last_player_orders.length - 1) last_player_orders_pointer++;
	return last;
}

function get_next_player_order()
{
	if (!last_player_orders.length || last_player_orders_pointer == 0) return '';
	last_player_orders_pointer--;
	return last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];

}



// Graphics functions


function hideGraphicsWindow()
{
		$('.text').removeClass('half_text');
		$('.text').addClass('all_text');
		$('.graphics').removeClass('half_graphics');
		$('.graphics').addClass('hidden');
		if ($('.location_picture')) $('.location_picture').remove();
}



function drawPicture(picno)  
{
	var pictureDraw = false;
	if (graphicsON) 
	{
		if ((isDarkHere()) && (!lightObjectsPresent())) picno = 0;
		var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
		if (filename)
		{
			$('.graphics').removeClass('hidden');
			$('.graphics').addClass('half_graphics');
			$('.text').removeClass('all_text');
			$('.text').addClass('half_text');
			$('.graphics').html('<img alt="" class="location_picture" src="' +  filename + '" />');
			$('.location_picture').css('height','100%');
			pictureDraw = true;
		}
	}

	if (!pictureDraw) hideGraphicsWindow();
}




function clearPictureAt() // deletes all pictures drawn by "pictureAT" condact
{
	$.each($('.graphics img'), function () {
		if ($(this)[0].className!= 'location_picture') $(this).remove();
	});

}

// Turns functions

function incTurns()
{
	turns = getFlag(FLAG_TURNS_LOW) + 256 * getFlag(FLAG_TURNS_HIGH)  + 1;
	setFlag(FLAG_TURNS_LOW, turns % 256);
	setFlag(FLAG_TURNS_HIGH, Math.floor(turns / 256));
}

// input box functions

function disableInput()
{
	$(".input").prop('disabled', true); 
}

function enableInput()
{
	$(".input").prop('disabled', false); 
}

function focusInput()
{
	$(".prompt").focus();
	timeout_progress = 0;
}

// Object default attributes functions

function objectIsNPC(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_NPC);
}

function objectIsLight(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_LIGHT);
}

function objectIsWearable(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_WEARABLE);
}

function objectIsContainer(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_CONTAINER);
}

function objectIsSupporter(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_SUPPORTER);
}


function objectIsAttr(objno, attrno)
{
	if (attrno > 63) return false;
	var attrs = getObjectLowAttributes(objno);
	if (attrno > 31)
	{
		attrs = getObjectHighAttributes(objno);
		attrno = attrno - 32;
	}
	return bittest(attrs, attrno);
}

function isAccesibleContainer(objno)
{
	if (objectIsSupporter(objno)) return true;   // supporter
	if ( objectIsContainer(objno) && !objectIsAttr(objno, ATTR_OPENABLE) ) return true;  // No openable container
	if ( objectIsContainer(objno) && objectIsAttr(objno, ATTR_OPENABLE) && objectIsAttr(objno, ATTR_OPEN)  )  return true;  // No openable & open container
	return false;
}

//Objects and NPC functions

function findMatchingObject(locno)
{
	for (var i=0;i<num_objects;i++)
		if ((locno==-1) || (getObjectLocation(i) == locno))
		 if (((objectsNoun[i]) == getFlag(FLAG_NOUN1)) && (((objectsAdjective[i]) == EMPTY_WORD) || ((objectsAdjective[i]) == getFlag(FLAG_ADJECT1))))  return i;
	return EMPTY_OBJECT;
}

function getReferredObject()
{
	var objectfound = EMPTY_OBJECT; 
	refobject_search: 
	{
		object_id = findMatchingObject(LOCATION_CARRIED);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(LOCATION_WORN);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(loc_here());
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(-1);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	
	}
	return objectfound;
}


function getObjectLowAttributes(objno)
{
	return objectsAttrLO[objno];
}

function getObjectHighAttributes(objno)
{
	return objectsAttrHI[objno]
}


function setObjectLowAttributes(objno, attrs)
{
	objectsAttrLO[objno] = attrs;
}

function setObjectHighAttributes(objno, attrs)
{
	objectsAttrHI[objno] = attrs;
}


function getObjectLocation(objno)
{
	if (objno > last_object_number) 
		writeWarning(STR_INVALID_OBJECT + ' [' + objno + ']');
	return objectsLocation[objno];
}

function setObjectLocation(objno, locno)
{
	if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) - 1);
	objectsLocation[objno] = locno;
	if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) + 1);
}



// Sets all flags associated to  referred object by current LS  
function setReferredObject(objno) 
{
	if (objno == EMPTY_OBJECT)
	{
		setFlag(FLAG_REFERRED_OBJECT, EMPTY_OBJECT);
		setFlag(FLAG_REFERRED_OBJECT_LOCATION, LOCATION_NONCREATED);
		setFlag(FLAG_REFERRED_OBJECT_WEIGHT, 0);
		setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, 0);
		setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, 0);
		return;
	}
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setFlag(FLAG_REFERRED_OBJECT_LOCATION, getObjectLocation(objno));
	setFlag(FLAG_REFERRED_OBJECT_WEIGHT, getObjectWeight(objno));
	setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, getObjectLowAttributes(objno));
	setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, getObjectHighAttributes(objno));

}


function getObjectWeight(objno) 
{
	var weight = objectsWeight[objno];
	if ( ((objectIsContainer(objno)) || (objectIsSupporter(objno))) && (weight!=0)) // Container with zero weigth are magic boxes, anything you put inside weigths zero
  		weight = weight + getLocationObjectsWeight(objno);
	return weight;
}


function getLocationObjectsWeight(locno) 
{
	var weight = 0;
	for (var i=0;i<num_objects;i++)
	{
		if (getObjectLocation(i) == locno) 
		{
			objweight = objectsWeight[i];
			weight += objweight;
			if (objweight > 0)
			{
				if (  (objectIsContainer(i)) || (objectIsSupporter(i)) )
				{	
					weight += getLocationObjectsWeight(i);
				}
			}
		}
	}
	return weight;
}

function getObjectCountAt(locno) 
{
	var count = 0;
	for (i=0;i<num_objects;i++)
	{
		if (getObjectLocation(i) == locno) 
		{
			attr = getObjectLowAttributes(i);
			if (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)) count ++;  // Parser settings say we should include NPCs as objects
			 else if (!objectIsNPC(i)) count++;     // or object is not an NPC
		}
	}
	return count;
}


function getObjectCountAtWithAttr(locno, attrnoArray) 
{
	var count = 0;
	for (var i=0;i<num_objects;i++)
		if (getObjectLocation(i) == locno)  
			for (var j=0;j<attrnoArray.length;j++)
				if (objectIsAttr(i, attrnoArray[j])) count++;
	return count;
}


function getNPCCountAt(locno) 
{
	var count = 0;
	for (i=0;i<num_objects;i++)
		if ((getObjectLocation(i) == locno) &&  (objectIsNPC(i))) count++;
	return count;
}


// Location light function

function lightObjectsAt(locno) 
{
	return getObjectCountAtWithAttr(locno, [ATTR_LIGHT]) > 0;
}


function lightObjectsPresent() 
{
  if (lightObjectsAt(LOCATION_CARRIED)) return true;
  if (lightObjectsAt(LOCATION_WORN)) return true;
  if (lightObjectsAt(loc_here())) return true;
  return false;
}


function isDarkHere()
{
	return (getFlag(FLAG_LIGHT) != 0);
}

// Sound functions


function preloadsfx()
{
	for (var i=0;i<resources.length;i++)
	 	if (resources[i][0] == 'RESOURCE_TYPE_SND') 
	 	{
	 		var fileparts = resources[i][2].split('.');
			var basename = fileparts[0];
			var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] , preload: true} );
	 	}
}

function sfxplay(sfxno, channelno, times, method)
{

	if (!soundsON) return;
	if ((channelno <0) || (channelno >MAX_CHANNELS)) return;
	if (times == 0) times = -1; // more than 4000 million times
	var filename = getResourceById(RESOURCE_TYPE_SND, sfxno);
	if (filename)
	{
		var fileparts = filename.split('.');
		var basename = fileparts[0];
		var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] });
		if (soundChannels[channelno]) soundChannels[channelno].stop();
		soundLoopCount[channelno] = times;
		mySound.bind("ended", function(e) {
			for (sndloop=0;sndloop<MAX_CHANNELS;sndloop++)
				if (soundChannels[sndloop] == this)
				{
					if (soundLoopCount[sndloop]==-1) {this.play(); return }
					soundLoopCount[sndloop]--;
					if (soundLoopCount[sndloop] > 0) {this.play(); return }
					sfxstop(sndloop);
					return;
				}
		});
		soundChannels[channelno] = mySound;
		if (method=='play')	mySound.play(); else mySound.fadeIn(2000);
	}
}

function playLocationMusic(locno)
{
	if (soundsON) 
		{
			sfxstop(0);
			sfxplay(locno, 0, 0, 'play');
		}
}

function musicplay(musicno, times)  
{
	sfxplay(musicno, 0, times);
}

function channelActive(channelno)
{
	if (soundChannels[channelno]) return true; else return false;
}


function sfxstopall() 
{
	for (channelno=0;channelno<MAX_CHANNELS;channelno++) sfxstop(channelno);

}


function sfxstop(channelno)
{
	if (soundChannels[channelno]) 
		{
			soundChannels[channelno].unbind('ended');
			soundChannels[channelno].stop();
			soundChannels[channelno] = null;
		}
}

function sfxvolume(channelno, value)
{
	if (soundChannels[channelno]) soundChannels[channelno].setVolume(Math.floor( value * 100 / 65535)); // Inherited volume condact uses a number among 0 and 65535, buzz library uses 0-100.
}

function isSFXPlaying(channelno)
{
	if (!soundChannels[channelno]) return false;
	return true;
}


function sfxfadeout(channelno, value)
{
	if (!soundChannels[channelno]) return;
	soundChannels[channelno].fadeOut(value, function() { sfxstop(channelno) });
}

// *** Process functions ***

function callProcess(procno)
{
	if (inEND) return;
	current_process = procno;
	var prostr = procno.toString(); 
	while (prostr.length < 3) prostr = "0" + prostr;
	if (procno==0) in_response = true;
	if (doall_flag && in_response) done_flag = false;
	if (!in_response) done_flag = false;
	h_preProcess(procno);
    eval("pro" + prostr + "()");
	h_postProcess(procno);
	if (procno==0) in_response = false;
}

// Bitwise functions

function bittest(value, bitno)
{
	mask = 1 << bitno;
	return ((value & mask) != 0);
}

function bitset(value, bitno)
{

	mask = 1 << bitno;
	return value | mask;
}

function bitclear(value, bitno)
{
	mask = 1 << bitno;
	return value & (~mask);
}


function bitneg(value, bitno) 
{
	mask = 1 << bitno;
	return value ^ mask;

}

// Savegame functions
function getSaveGameObject()
{
	var savegame_object = new Object();
	// Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
	savegame_object.flags = flags.slice();
	savegame_object.objectsLocation = objectsLocation.slice();
	savegame_object.objectsWeight = objectsWeight.slice();
	savegame_object.objectsAttrLO = objectsAttrLO.slice();
	savegame_object.objectsAttrHI = objectsAttrHI.slice();
	savegame_object.connections = connections.slice();
	savegame_object.last_player_orders = last_player_orders.slice();
	savegame_object.last_player_orders_pointer = last_player_orders_pointer;
	savegame_object.transcript = transcript;
	savegame_object = h_saveGame(savegame_object);
	return savegame_object;
}

function restoreSaveGameObject(savegame_object)
{
	flags = savegame_object.flags;
	// Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
	objectsLocation = savegame_object.objectsLocation.slice();
	objectsWeight = savegame_object.objectsWeight.slice();
	objectsAttrLO = savegame_object.objectsAttrLO.slice();
	objectsAttrHI = savegame_object.objectsAttrHI.slice();
	connections = savegame_object.connections.slice();
	last_player_orders = savegame_object.last_player_orders.slice();
	last_player_orders_pointer = savegame_object.last_player_orders_pointer;
	transcript = savegame_object.transcript;
	h_restoreGame(savegame_object);
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        The parser                                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function loadPronounSufixes()
{

    var swapped;

	for (var j=0;j<vocabulary.length;j++) if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_PRONOUN)
			 pronoun_suffixes.push(vocabulary[j][VOCABULARY_WORD]);
	// Now sort them so the longest are first, so you rather replace SELOS in (COGESELOS=>COGE SELOS == >TAKE THEM) than LOS (COGESELOS==> COGESE LOS ==> TAKExx THEM) that woul not be understood (COGESE is not a verb, COGE is)
    do {
        swapped = false;
        for (var i=0; i < pronoun_suffixes.length-1; i++) 
        {
            if (pronoun_suffixes[i].length < pronoun_suffixes[i+1].length) 
            {
                var temp = pronoun_suffixes[i];
                pronoun_suffixes[i] = pronoun_suffixes[i+1];
                pronoun_suffixes[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}


function findVocabulary(word, forceDisableLevenshtein)  
{
	// Pending: in general this function is not very efficient. A solution where the vocabulary array is sorted by word so the first search can be binary search
	//          and possible typos are precalculated, so the distance is a lookup table instead of a function, would be much more efficient. On the other hand,
	//          the current solution is fast enough with a 1000+ words game that I don't consider improving this function to have high priority now.

	// Search word in vocabulary
	for (var j=0;j<vocabulary.length;j++)
		if (vocabulary[j][VOCABULARY_WORD] == word)
			 return vocabulary[j];

	if (forceDisableLevenshtein) return null;

	if (word.length <=4) return null; // Don't try to fix typo for words with less than 5 length

	if (bittest(getFlag(FLAG_PARSER_SETTINGS), 8)) return null; // If matching is disabled, we won't try to use levhenstein distance

	// Search words in vocabulary with a Levenshtein distance of 1
	var distance2_match = null;
	for (var k=0;k<vocabulary.length;k++)
	{
		if ([WORDTYPE_VERB,WORDTYPE_NOUN,WORDTYPE_ADJECT,WORDTYPE_ADVERB].indexOf(vocabulary[k][VOCABULARY_TYPE])  != -1 )
		{
			var distance = getLevenshteinDistance(vocabulary[k][VOCABULARY_WORD], word);
			if ((!distance2_match) && (distance==2)) distance2_match = vocabulary[k]; // Save first word with distance=2, in case we don't find any word with distance 1
			if (distance <= 1) return vocabulary[k];
		}
	} 

	// If we found any word with distance 2, return it, only if word was at least 7 characters long
	if ((distance2_match) &&  (word.length >6)) return distance2_match;

	// Word not found
	return null;
}

function normalize(player_order)   
// Removes accented characters and makes sure every sentence separator (colon, semicolon, quotes, etc.) has one space before and after. Also, all separators are converted to comma
{
	var originalchars = 'áéíóúäëïöüâêîôûàèìòùÁÉÍÓÚÄËÏÖÜÂÊÎÔÛÀÈÌÒÙ';
	var i;
	var output = '';
	var pos;

	for (i=0;i<player_order.length;i++) 
	{
		pos = originalchars.indexOf(player_order.charAt(i));
		if (pos!=-1) output = output + "aeiou".charAt(pos % 5); else 
		{
			ch = player_order.charAt(i);
				if ((ch=='.') || (ch==',') || (ch==';') || (ch=='"') || (ch=='\'') || (ch=='«') || (ch=='»')) output = output + ' , '; else output = output + player_order.charAt(i);
		}

	}
	return output;
}

function toParserBuffer(player_order)  // Converts a player order in a list of sentences separated by dot.
{
     player_order = normalize(player_order);
     player_order = player_order.toUpperCase();
    
	 var words = player_order.split(' ');
	 for (var q=0;q<words.length;q++)
	 {
	 	words[q] = words[q].trim();
	 	if  (words[q]!=',')
	 	{
	 		words[q] = words[q].trim();
	 		foundWord = findVocabulary(words[q], false);
	 		if (foundWord)
	 		{
	 			if (foundWord[VOCABULARY_TYPE]==WORDTYPE_CONJUNCTION)
	 			{
	 			words[q] = ','; // Replace conjunctions with commas
		 		} 
	 		}
	 	}
	 }

	 var output = '';
	 for (q=0;q<words.length;q++)
	 {
	 	if (words[q] == ',') output = output + ','; else output = output + words[q] + ' ';
	 }
	 output = output.replace(/ ,/g,',');
	 output = output.trim();
	 player_order_buffer = output;
}

function getSentencefromBuffer()
{
	var sentences = player_order_buffer.split(',');
	var result = sentences[0];
	sentences.splice(0,1);
	player_order_buffer = sentences.join();
	return result;
}

function processPronounSufixes(words)  
{
	// This procedure will split pronominal sufixes into separated words, so COGELA will become COGE LA at the end, and work exactly as TAKE IT does.
	// it's only for spanish so if lang is english then it makes no changes
	if (getLang() == 'EN') return words;
	var verbFound = false;
	if (!bittest(getFlag(FLAG_PARSER_SETTINGS),0)) return words;  // If pronoun sufixes inactive, just do nothing
	// First, we clear the word list from any match with pronouns, cause if we already have something that matches pronouns, probably is just concidence, like in COGE LA LLAVE
	var filtered_words = [];
	for (var q=0;q < words.length;q++)
	{
		foundWord = findVocabulary(words[q], false);
		if (foundWord) 
			{
				if (foundWord[VOCABULARY_TYPE] != WORDTYPE_PRONOUN) filtered_words[filtered_words.length] = words[q];
			}
			else filtered_words[filtered_words.length] = words[q];
	}
	words = filtered_words;

	// Now let's start trying to get sufixes
	new_words = [];
	for (var k=0;k < words.length;k++)
	{
		words[k] = words[k].trim();
		foundWord = findVocabulary(words[k], true); // true to disable Levenshtein distance applied
		if (foundWord) if (foundWord[VOCABULARY_TYPE] == WORDTYPE_VERB) verbFound = true;  // If we found a verb, we don't look for pronoun sufixes, as they have to come together with verb
		suffixFound = false;
		pronunsufix_search:
		for (var l=0;(l<pronoun_suffixes.length) && (!suffixFound) && (!verbFound);l++)
		{

			if (pronoun_suffixes[l] == words[k].rights(pronoun_suffixes[l].length))
			{
				var verb_part = words[k].substring(0,words[k].length - pronoun_suffixes[l].length);
				var checkWord = findVocabulary(verb_part, false);
				if ((!checkWord)  || (checkWord[VOCABULARY_TYPE] != WORDTYPE_VERB))  // If the part before the supposed-to-be pronoun sufix is not a verb, then is not a pronoun sufix
				{
					new_words.push(words[k]);	
					continue pronunsufix_search;
				}
				new_words.push(verb_part);  // split the word in two parts: verb + pronoun. Since that very moment it works like in english (COGERLO ==> COGER LO as of TAKE IT)
				new_words.push(pronoun_suffixes[l]);
				suffixFound = true;
				verbFound = true;
			}
		}
		if (!suffixFound) new_words.push(words[k]);
	}
	return new_words;
}

function getLogicSentence()
{
	parser_word_found = false; ;
	aux_verb = -1;
	aux_noun1 = -1;
	aux_adject1 = -1;
	aux_adverb = -1;
	aux_pronoun = -1
	aux_pronoun_adject = -1
	aux_preposition = -1;
	aux_noun2 = -1;
	aux_adject2 = -1;
	initializeLSWords();
	SL_found = false;

	var order = getSentencefromBuffer();
	setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS),1)); // Initialize flag that says an unknown word was found in the sentence


	words = order.split(" ");
	words = processPronounSufixes(words);
	wordsearch_loop:
	for (var i=0;i<words.length;i++)
	{
		original_word = currentword = words[i];
		if (currentword.length>10) currentword = currentword.substring(0,MAX_WORD_LENGHT);
		foundWord = findVocabulary(currentword, false);
		if (foundWord)
		{
			wordtype = foundWord[VOCABULARY_TYPE];
			word_id = foundWord[VOCABULARY_ID];

			switch (wordtype)
			{
				case WORDTYPE_VERB: if (aux_verb == -1)  aux_verb = word_id; 
				        			break;

				case WORDTYPE_NOUN: if (aux_noun1 == -1) aux_noun1 = word_id; else if (aux_noun2 == -1) aux_noun2 = word_id;
									break;

				case WORDTYPE_ADJECT: if (aux_adject1 == -1) aux_adject1 = word_id; else if (aux_adject2 == -1) aux_adject2 = word_id;
									  break;

				case WORDTYPE_ADVERB: if (aux_adverb == -1) aux_adverb = word_id;
				        			  break;

				case WORDTYPE_PRONOUN: if (aux_pronoun == -1) 
											{
												aux_pronoun = word_id;
												if ((previous_noun != EMPTY_WORD) && (aux_noun1 == -1))
												{
													aux_noun1 = previous_noun;
													if (previous_adject != EMPTY_WORD) aux_adject1 = previous_adject;
												}
											}

				        			   break;

				case WORDTYPE_CONJUNCTION: break wordsearch_loop; // conjunction or nexus. Should not appear in this function, just added for security
				
				case WORDTYPE_PREPOSITION: if (aux_preposition == -1) aux_preposition = word_id;
										   if (aux_noun1!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),2));  // Set bit that determines that a preposition word was found after first noun
										   break;
			}

			// Nouns that can be converted to verbs
			if ((aux_noun1!=-1) && (aux_verb==-1) && (aux_noun1 < NUM_CONVERTIBLE_NOUNS))
			{
				aux_verb = aux_noun1;
				aux_noun1 = -1;
			}

			if ((aux_verb==-1) && (aux_noun1!=-1) && (previous_verb!=EMPTY_WORD)) aux_verb = previous_verb;  // Support "TAKE SWORD AND SHIELD" --> "TAKE WORD AND TAKE SHIELD"

			if ((aux_verb!=-1) || (aux_noun1!=-1) || (aux_adject1!=-1 || (aux_preposition!=-1) || (aux_adverb!=-1))) SL_found = true;



		} else if (aux_verb!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),1));  // Set bit that determines that an unknown word was found after the verb
	}

	if (SL_found)
	{
		if (aux_verb != -1) setFlag(FLAG_VERB, aux_verb);
		if (aux_noun1 != -1) setFlag(FLAG_NOUN1, aux_noun1);
		if (aux_adject1 != -1) setFlag(FLAG_ADJECT1, aux_adject1);
		if (aux_adverb != -1) setFlag(FLAG_ADVERB, aux_adverb);
		if (aux_pronoun != -1) 
			{
				setFlag(FLAG_PRONOUN, aux_noun1);
				setFlag(FLAG_PRONOUN_ADJECT, aux_adject1);
			}
			else
			{
				setFlag(FLAG_PRONOUN, EMPTY_WORD);
				setFlag(FLAG_PRONOUN_ADJECT, EMPTY_WORD);
			}
		if (aux_preposition != -1) setFlag(FLAG_PREP, aux_preposition);
		if (aux_noun2 != -1) setFlag(FLAG_NOUN2, aux_noun2);
		if (aux_adject2 != -1) setFlag(FLAG_ADJECT2, aux_adject2);
		setReferredObject(getReferredObject());
		previous_verb = aux_verb;
		if ((aux_noun1!=-1) && (aux_noun1>=NUM_PROPER_NOUNS))
		{
			previous_noun = aux_noun1;
			if (aux_adject1!=-1) previous_adject = aux_adject1;
		}
		
	}
	if ((aux_verb + aux_noun1+ aux_adject1 + aux_adverb + aux_pronoun + aux_preposition + aux_noun2 + aux_adject2) != -8) parser_word_found = true;

	return SL_found;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        Main functions and main loop                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Interrupt functions

function enableInterrupt()
{
	interruptDisabled = false;
}

function disableInterrupt()
{
	interruptDisabled = true;
}

function timer()
{
	// Timeout control
	timeout_progress=  timeout_progress + 1/32;  //timer happens every 40 milliseconds, but timeout counter should only increase every 1.28 seconds (according to PAWS documentation)
	timeout_length = getFlag(FLAG_TIMEOUT_LENGTH);
	if ((timeout_length) && (timeout_progress> timeout_length))  // time for timeout
	{
		timeout_progress = 0;
		if (($('.prompt').val() == '')  || (($('.prompt').val()!='') && (!bittest(getFlag(FLAG_TIMEOUT_SETTINGS),0))) )  // but first check there is no text type, or is allowed to timeout when text typed already
		{
			setFlag(FLAG_TIMEOUT_SETTINGS, bitset(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
			writeSysMessage(SYSMESS_TIMEOUT);	
			callProcess(PROCESS_TURN);
		}
	}	

	// PAUSE condact control
	if (inPause)
	{
		pauseRemainingTime = pauseRemainingTime - 40; // every tick = 40 milliseconds
		if (pauseRemainingTime<=0)
		{
			inPause = false;
			hideAnykeyLayer();
			waitKeyCallback()
		}
	}

	// Interrupt process control
	if (!interruptDisabled)
	if (interruptProcessExists)
	{
		callProcess(interrupt_proc);
		setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened 
	}

}

// Initialize and finalize functions

function farewell()
{
	writeSysMessage(SYSMESS_FAREWELL);
	ACCnewline();
}


function initializeConnections()
{
  connections = [].concat(connections_start);
}

function initializeObjects()
{
  for (i=0;i<objects.length;i++)
  {
  	objectsAttrLO = [].concat(objectsAttrLO_start);
  	objectsAttrHI = [].concat(objectsAttrHI_start);
  	objectsLocation = [].concat(objectsLocation_start);
  	objectsWeight = [].concat(objectsWeight_start);
  }
}

function  initializeLSWords()
{
  setFlag(FLAG_PREP,EMPTY_WORD);
  setFlag(FLAG_NOUN2,EMPTY_WORD);
  setFlag(FLAG_ADJECT2,EMPTY_WORD);
  setFlag(FLAG_PRONOUN,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_VERB,EMPTY_WORD);
  setFlag(FLAG_NOUN1,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_ADVERB,EMPTY_WORD);
}


function initializeFlags()
{
  flags = [];
  for (var  i=0;i<FLAG_COUNT;i++) flags.push(0);
  setFlag(FLAG_MAXOBJECTS_CARRIED,4);
  setFlag(FLAG_PARSER_SETTINGS,9); // Pronoun sufixes active, DOALL and others ignore NPCs, etc. 00001001
  setFlag(FLAG_MAXWEIGHT_CARRIED,10);
  initializeLSWords();
  setFlag(FLAG_OBJECT_LIST_FORMAT,64); // List objects in a single sentence (comma separated)
  setFlag(FLAG_OBJECTS_CARRIED_COUNT,carried_objects);  // FALTA: el compilador genera esta variable, hay que cambiarlo en el compilador, ERA numero_inicial_de_objetos_llevados
}

function initializeInternalVars()
{
	num_objects = last_object_number + 1;
	transcript = '';
	timeout_progress = 0;
	previous_noun = EMPTY_WORD;
	previous_verb = EMPTY_WORD;
	previous_adject = EMPTY_WORD;
	player_order_buffer = '';
	last_player_orders = [];
	last_player_orders_pointer = 0;
	graphicsON = true; 
	soundsON = true; 
	interruptDisabled = false;
	unblock_process = null;
	done_flag = false;
	describe_location_flag =false;
	in_response = false;
	success = false;
	doall_flag = false;
	entry_for_doall	= '';
}

function initializeSound()
{
	sfxstopall();
}




function initialize()
{
	preloadsfx();
	initializeInternalVars();
	initializeSound();
	initializeFlags();
	initializeObjects();
	initializeConnections();
}



// Main loops

function descriptionLoop()
{
	do
	{
		describe_location_flag = false;
		if (!getFlag(FLAG_MODE)) clearTextWindow();
		if ((isDarkHere()) && (!lightObjectsPresent())) writeSysMessage(SYSMESS_ISDARK); else writeLocation(loc_here()); 
		h_description_init();
		playLocationMusic(loc_here());
		if (loc_here()) drawPicture(loc_here()); else hideGraphicsWindow(); // Don't show picture at location 0
		ACCminus(FLAG_AUTODEC2,1);
		if (isDarkHere()) ACCminus(FLAG_AUTODEC3,1);
		if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC4,1);
		callProcess(PROCESS_DESCRIPTION);
		h_description_post();
		if (describe_location_flag) continue; // descriptionLoop() again without nesting
		describe_location_flag = false;
		callProcess(PROCESS_TURN);
		if (describe_location_flag) continue; 
		describe_location_flag = false;
		focusInput();
		break; // Dirty trick to make this happen just one, but many times if descriptioLoop() should be repeated
	} while (true);

}

function orderEnteredLoop(player_order)
{
	previous_verb = EMPTY_WORD;
	setFlag(FLAG_TIMEOUT_SETTINGS, bitclear(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
	if (player_order == '') {writeSysMessage(SYSMESS_SORRY); ACCnewline(); return; };	
	player_order = h_playerOrder(player_order); //hook
	copyOrderToTextWindow(player_order);
	toParserBuffer(player_order);
	do 
	{
		describe_location_flag = false;
		ACCminus(FLAG_AUTODEC5,1);
		ACCminus(FLAG_AUTODEC6,1);
		ACCminus(FLAG_AUTODEC7,1);
		ACCminus(FLAG_AUTODEC8,1);
		if (isDarkHere()) ACCminus(FLAG_AUTODEC9,1);
		if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC10,1);
		
		if (describe_location_flag) 
		{
			descriptionLoop();
			return;
		};

		if (getLogicSentence())
		{
			incTurns();
			done_flag = false;
			callProcess(PROCESS_RESPONSE); // Response table
			if (describe_location_flag) 
			{
				descriptionLoop();
				return;
			};
			if (!done_flag) 
			{
				if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (CNDmove(FLAG_LOCATION)))
				{
					descriptionLoop();
					return;
				} else if (getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) {writeSysMessage(SYSMESS_WRONGDIRECTION);ACCnewline();}	else {writeSysMessage(SYSMESS_CANTDOTHAT);ACCnewline();};

			}
		} else
		{
			h_invalidOrder(player_order);
			if (parser_word_found) {writeSysMessage(SYSMESS_IDONTUNDERSTAND);   ACCnewline() }
			    		      else {writeSysMessage(SYSMESS_NONSENSE_SENTENCE); ACCnewline() };	
		}  
		callProcess(PROCESS_TURN);
	} while (player_order_buffer !='');
	previous_verb = ''; // Can't use previous verb if a new order is typed (we keep previous noun though, it can be used)
	focusInput();
}


function restart()
{
	location.reload();	
}


function hideBlock()
{
	clearInputWindow();
    $('.block_layer').hide('slow');
    enableInterrupt();   	
    $('.input').show();  
    focusInput();
}

function hideAnykeyLayer()
{
	$('.anykey_layer').hide();
    $('.input').show();  
    focusInput();   
}

function showAnykeyLayer()
{
	$('.anykey_layer').show();
    $('.input').hide();  
}

//called when the block layer is closed
function closeBlock()
{
	if (!inBlock) return;
	inBlock = false;
	hideBlock();
    var proToCall = unblock_process;
	unblock_process = null;
	callProcess(proToCall);
	if (describe_location_flag) descriptionLoop();
}

function setInputPlaceHolder()
{
	var prompt_msg = getFlag(FLAG_PROMPT);
	if (!prompt_msg)
	{
		var random = Math.floor((Math.random()*100));
		if (random<30) prompt_msg = SYSMESS_PROMPT0; else
		if ((random>=30) && (random<60)) prompt_msg = SYSMESS_PROMPT1; else
		if ((random>=60) && (random<90)) prompt_msg = SYSMESS_PROMPT2; else
		if (random>=90) prompt_msg = SYSMESS_PROMPT3;
	}
	$('.prompt').attr('placeholder', $('<div>'+getSysMessageText(prompt_msg).replace(/(?:<br>)*$/,'').replace( /<br>/g, ', ' )+'</div>').text());
}


function divTextScrollUp()
{
   	var currentPos = $('.text').scrollTop();
	if (currentPos>=DIV_TEXT_SCROLL_STEP) $('.text').scrollTop(currentPos - DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop(0);
}

function divTextScrollDown()
{
   	var currentPos = $('.text').scrollTop();
   	if (currentPos <= ($('.text')[0].scrollHeight - DIV_TEXT_SCROLL_STEP)) $('.text').scrollTop(currentPos + DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop($('.text')[0].scrollHeight);
}

// Autocomplete functions

function predictiveText(currentText)
{
	if (currentText == '') return currentText;
	var wordToComplete;
	var words = currentText.split(' ');
	if (autocompleteStep!=0) wordToComplete = autocompleteBaseWord; else wordToComplete = words[words.length-1];
	words[words.length-1] = completedWord(wordToComplete);
	return words.join(' ');
}


function initAutoComplete()
{
	for (var j=0;j<vocabulary.length;j++)
		if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_VERB)
			if (vocabulary[j][VOCABULARY_WORD].length >= 3)
				autocomplete.push(vocabulary[j][VOCABULARY_WORD].toLowerCase());
}

function addToAutoComplete(sentence)
{
	var words = sentence.split(' ');
	for (var i=0;i<words.length;i++)
	{
		var finalWord = '';
		for (var j=0;j<words[i].length;j++)
		{
			var c = words[i][j].toLowerCase();
			if ("abcdefghijklmnopqrstuvwxyzáéíóúàèìòùçäëïÖüâêîôû".indexOf(c) != -1) finalWord = finalWord + c;
			else break;
		}
	
		if (finalWord.length>=3) 
		{
			var index = autocomplete.indexOf(finalWord);
			if (index!=-1) autocomplete.splice(index,1);
			autocomplete.push(finalWord);
		}
	}
}

function completedWord(word)
{
	if (word=='') return '';
   autocompleteBaseWord  =word;
   var foundCount = 0;
   for (var i = autocomplete.length-1;i>=0; i--)
   {
   	  if (autocomplete[i].length > word.length) 
   	  	 if (autocomplete[i].indexOf(word)==0) 
   	  	 	{
   	  	 		foundCount++;
   	  	 		if (foundCount>autocompleteStep)
   	  	 		{
   	  	 			autocompleteStep++;
   	  	 			return autocomplete[i];
   	  	 		}
   	  	 	}
   }
   return word;
}


// Exacution starts here, called by the html file on document.ready()
function start()
{
	h_init(); //hook
	$('.graphics').addClass('half_graphics');
	$('.text').addClass('half_text');
	if (isBadIE()) alert(STR_BADIE)
	loadPronounSufixes();	
    setInputPlaceHolder();
    initAutoComplete();

	// Assign keypress action for input box (detect enter key press)
	$('.prompt').keypress(function(e) {  
    	if (e.which == 13) 
    	{ 
    		setInputPlaceHolder();
    		player_order = $('.prompt').val();
    		if (player_order.charAt(0) == '#')
    		{
    			addToTranscript(player_order + STR_NEWLINE);
    			clearInputWindow();
    		} 
    		else
    		if (player_order!='') 
    				orderEnteredLoop(player_order);
    	}
    });

	// Assign arrow up key press to recover last order
    $('.prompt').keyup( function(e) {
    	if (e.which  == 38) $('.prompt').val(get_prev_player_order());
    	if (e.which  == 40) $('.prompt').val(get_next_player_order());
    });


    // Assign tab keydown to complete word
    $('.prompt').keydown( function(e) {
    	if (e.which == 9) 
    		{
    			$('.prompt').val(predictiveText($('.prompt').val()));
    			e.preventDefault();
    		} else 
    		{
		    	autocompleteStep = 0;
    			autocompleteBaseWord = ''; // Any keypress other than tab resets the autocomplete feature
    		}
    });

    //Detect resize to change flag 12
     $(window).resize(function () {
     	setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened 
     	clearPictureAt();
     	return;
     });


     // assign any click on block layer --> close it
     $(document).click( function(e) {

	// if waiting for END response
	if (inEND)
	{
		restart();
		return;
	}

     	if (inBlock)
     	{
     		closeBlock();
     		e.preventDefault();
     		return;
     	}

     	if (inAnykey)  // return for ANYKEY, accepts mouse click
     	{
     		inAnykey = false;
     		hideAnykeyLayer();
     		waitKeyCallback();
     		e.preventDefault();
     		return;
    	}

     });

     //Make tap act as click
    //document.addEventListener('touchstart', function(e) {$(document).click(); }, false);   
     
     
	$(document).keydown(function(e) {

		if (!h_keydown(e)) return; // hook

		// if waiting for END response
		if (inEND)
		{
			var endYESresponse = getSysMessageText(SYSMESS_YES);
			var endNOresponse = getSysMessageText(SYSMESS_NO);
			if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
			if (!endNOresponse.length) endNOresponse = 'N'; 
			var endYESresponseCode = endYESresponse.charCodeAt(0);
			var endNOresponseCode = endNOresponse.charCodeAt(0);

			switch ( e.keyCode )
			{
				case endYESresponseCode:
				case 13: // return
				case 32: // space
					location.reload();
					break;
				case endNOresponseCode:
					inEND = false;
					sfxstopall();
					$('body').hide('slow');
					break;
			}
			return;
		}


		// if waiting for QUIT response
		if (inQUIT)
		{
			var endYESresponse = getSysMessageText(SYSMESS_YES);
			var endNOresponse = getSysMessageText(SYSMESS_NO);
			if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
			if (!endNOresponse.length) endNOresponse = 'N'; 
			var endYESresponseCode = endYESresponse.charCodeAt(0);
			var endNOresponseCode = endNOresponse.charCodeAt(0);

			switch ( e.keyCode )
			{
				case endYESresponseCode:
				case 13: // return
				case 32: // space
					inQUIT=false;
					e.preventDefault();
					waitKeyCallback();
					return;
				case endNOresponseCode:
					inQUIT=false;
					waitkey_callback_function.pop();
					hideAnykeyLayer();
					e.preventDefault();
					break;
			}
		}

		// ignore uninteresting keys
		switch ( e.keyCode )
		{
			case 9:  // tab   \ keys used during
			case 13: // enter / keyboard navigation
			case 16: // shift
			case 17: // ctrl
			case 18: // alt
			case 20: // caps lock
			case 91: // left Windows key
			case 92: // left Windows key
			case 93: // left Windows key
			case 225: // right alt
				// do not focus the input - the user was probably doing something else
				// (e.g. alt-tab'ing to another window)
				return;
		}


		if (inGetkey)  // return for getkey
     	{
     		setFlag(getkey_return_flag, e.keyCode);
     		getkey_return_flag = null;
     		inGetkey = false;
     		hideAnykeyLayer();
     		e.preventDefault();
     		waitKeyCallback();
     		return;
      	}

     	// Scroll text window using PgUp/PgDown
        if (e.keyCode==33)  // PgUp
        {
        	divTextScrollUp();
        	e.preventDefault();
        	return;
        }
        if (e.keyCode==34)  // PgDown
        {
        	divTextScrollDown();
        	return;
        }


     	if (inAnykey)  // return for anykey
     	{
     		inAnykey = false;
     		hideAnykeyLayer();
     		e.preventDefault();
     		waitKeyCallback();
     		return;
     	}

		// if keypress and block displayed, close it
     	if (inBlock)
     		{
     			closeBlock();
     			e.preventDefault();
     			return;
     		}


     	// if ESC pressed and transcript layer visible, close it
     	if ((inTranscript) &&  (e.keyCode == 27)) 
     		{
     			$('.transcript_layer').hide();
     			inTranscript = false;
     			e.preventDefault();
     			return;
     		}

	// focus the input if the user is likely to expect it
	// (but not if they're e.g. ctrl+c'ing some text)
	switch ( e.keyCode )
	{
		case 8: // backspace
		case 9: // tab
		case 13: // enter
			break;
		default:
			if ( !e.ctrlKey && !e.altKey ) focusInput();
	}

	});


    $(document).bind('wheel mousewheel',function(e)
    {
  		if((e.originalEvent.wheelDelta||-e.originalEvent.deltaY) > 0) divTextScrollUp(); else divTextScrollDown();
    });


	initialize();
	descriptionLoop();
	focusInput();
	
	h_post();  //hook

    // Start interrupt process
    setInterval( timer, TIMER_MILLISECONDS );

}

$('document').ready(
	function ()
	{
		start();
	}
	);

// VOCABULARY

vocabulary = [];
vocabulary.push([35, "AIR", 1]);
vocabulary.push([2, "AND", 5]);
vocabulary.push([36, "AQUA", 1]);
vocabulary.push([9, "ASCE", 0]);
vocabulary.push([115, "ATTA", 0]);
vocabulary.push([58, "AUTO", 1]);
vocabulary.push([71, "BADG", 1]);
vocabulary.push([17, "BALL", 1]);
vocabulary.push([28, "BATT", 1]);
vocabulary.push([28, "BATTERY", 1]);
vocabulary.push([43, "BLAS", 1]);
vocabulary.push([43, "BLASTER", 1]);
vocabulary.push([31, "BLOW", 0]);
vocabulary.push([207, "BLOWPIPE", 1]);
vocabulary.push([69, "BLUE", 1]);
vocabulary.push([42, "BOMB", 1]);
vocabulary.push([18, "BOOS", 1]);
vocabulary.push([18, "BOOSTERPAK", 1]);
vocabulary.push([32, "BOOT", 1]);
vocabulary.push([32, "BOOTS", 1]);
vocabulary.push([63, "BOX", 1]);
vocabulary.push([34, "BRAC", 1]);
vocabulary.push([34, "BRACELET", 1]);
vocabulary.push([67, "BUTT", 1]);
vocabulary.push([92, "CANT", 1]);
vocabulary.push([29, "CAPE", 1]);
vocabulary.push([14, "CARD", 1]);
vocabulary.push([90, "CHAI", 1]);
vocabulary.push([15, "CIGA", 1]);
vocabulary.push([15, "CIGAR", 1]);
vocabulary.push([9, "CLIM", 0]);
vocabulary.push([44, "CLIP", 1]);
vocabulary.push([29, "CLOA", 1]);
vocabulary.push([29, "CLOAK", 1]);
vocabulary.push([60, "CODE", 1]);
vocabulary.push([96, "COGS", 1]);
vocabulary.push([44, "COIN", 1]);
vocabulary.push([19, "COMB", 1]);
vocabulary.push([51, "COMP", 1]);
vocabulary.push([58, "CRAN", 1]);
vocabulary.push([22, "CRED", 1]);
vocabulary.push([63, "CRYS", 1]);
vocabulary.push([30, "CUFF", 1]);
vocabulary.push([30, "CUFFLINKS", 1]);
vocabulary.push([200, "CUNT", 0]);
vocabulary.push([93, "CUPB", 1]);
vocabulary.push([14, "D", 0]);
vocabulary.push([50, "DATA", 1]);
vocabulary.push([83, "DISC", 1]);
vocabulary.push([24, "DISP", 1]);
vocabulary.push([24, "DISPLACER", 1]);
vocabulary.push([27, "DISR", 1]);
vocabulary.push([27, "DISRUPTER", 1]);
vocabulary.push([71, "DOCK", 1]);
vocabulary.push([26, "DOOR", 1]);
vocabulary.push([14, "DOWN", 0]);
vocabulary.push([95, "DRAW", 1]);
vocabulary.push([47, "DRIN", 0]);
vocabulary.push([47, "DRINK", 0]);
vocabulary.push([55, "DROI", 1]);
vocabulary.push([101, "DROP", 0]);
vocabulary.push([94, "DUST", 1]);
vocabulary.push([3, "E", 0]);
vocabulary.push([3, "EAST", 0]);
vocabulary.push([38, "ELEC", 1]);
vocabulary.push([38, "ELECTROSHI", 1]);
vocabulary.push([83, "EMPL", 1]);
vocabulary.push([96, "ENGI", 1]);
vocabulary.push([109, "ENTE", 0]);
vocabulary.push([99, "EXAM", 0]);
vocabulary.push([62, "FIEL", 1]);
vocabulary.push([77, "FIRE", 0]);
vocabulary.push([41, "FLAS", 1]);
vocabulary.push([41, "FLASK", 1]);
vocabulary.push([62, "FORC", 1]);
vocabulary.push([200, "FUCK", 0]);
vocabulary.push([65, "GAS", 1]);
vocabulary.push([65, "GASL", 1]);
vocabulary.push([100, "GET", 0]);
vocabulary.push([76, "GIVE", 0]);
vocabulary.push([23, "GLUE", 1]);
vocabulary.push([70, "GREE", 1]);
vocabulary.push([64, "GRIL", 1]);
vocabulary.push([61, "GUAR", 1]);
vocabulary.push([43, "GUN", 1]);
vocabulary.push([43, "HAN", 1]);
vocabulary.push([43, "HAND", 1]);
vocabulary.push([43, "HANDBLASTE", 1]);
vocabulary.push([116, "HELP", 0]);
vocabulary.push([56, "HIGH", 1]);
vocabulary.push([73, "HURL", 0]);
vocabulary.push([54, "HYGI", 1]);
vocabulary.push([11, "I", 1]);
vocabulary.push([68, "IGNI", 1]);
vocabulary.push([12, "IN", 0]);
vocabulary.push([14, "INFR", 1]);
vocabulary.push([14, "INFRADAT", 1]);
vocabulary.push([87, "INSE", 0]);
vocabulary.push([11, "INVE", 0]);
vocabulary.push([18, "JET", 1]);
vocabulary.push([19, "KEY", 1]);
vocabulary.push([115, "KILL", 0]);
vocabulary.push([20, "LADD", 1]);
vocabulary.push([20, "LADDERS", 1]);
vocabulary.push([57, "LATT", 1]);
vocabulary.push([73, "LAUN", 0]);
vocabulary.push([17, "LEAD", 1]);
vocabulary.push([81, "LEVE", 1]);
vocabulary.push([60, "LIBR", 1]);
vocabulary.push([16, "LIGH", 0]);
vocabulary.push([16, "LIGHT", 1]);
vocabulary.push([42, "LIMP", 1]);
vocabulary.push([65, "LITE", 1]);
vocabulary.push([108, "LOAD", 0]);
vocabulary.push([97, "LOCK", 1]);
vocabulary.push([105, "LOOK", 0]);
vocabulary.push([96, "MACH", 1]);
vocabulary.push([32, "MAGN", 1]);
vocabulary.push([32, "MAGNET", 1]);
vocabulary.push([53, "MAIN", 1]);
vocabulary.push([25, "MANU", 1]);
vocabulary.push([25, "MANUAL", 1]);
vocabulary.push([35, "MASK", 1]);
vocabulary.push([24, "MATT", 1]);
vocabulary.push([24, "MATTER", 1]);
vocabulary.push([55, "MULT", 1]);
vocabulary.push([1, "N", 0]);
vocabulary.push([35, "NATU", 1]);
vocabulary.push([35, "NATUFLOW", 1]);
vocabulary.push([7, "NE", 0]);
vocabulary.push([57, "NET", 1]);
vocabulary.push([1, "NORT", 0]);
vocabulary.push([1, "NORTH", 0]);
vocabulary.push([37, "NOTE", 1]);
vocabulary.push([8, "NW", 0]);
vocabulary.push([10, "O", 0]);
vocabulary.push([91, "OIL", 1]);
vocabulary.push([22, "ONE", 1]);
vocabulary.push([72, "OPEN", 0]);
vocabulary.push([10, "OUT", 0]);
vocabulary.push([45, "PACK", 1]);
vocabulary.push([89, "PANE", 1]);
vocabulary.push([40, "PASS", 1]);
vocabulary.push([63, "PHON", 1]);
vocabulary.push([45, "PHOT", 1]);
vocabulary.push([45, "PHOTON", 1]);
vocabulary.push([31, "PIPE", 1]);
vocabulary.push([200, "PISS", 0]);
vocabulary.push([91, "POOL", 1]);
vocabulary.push([46, "POST", 1]);
vocabulary.push([66, "PRES", 0]);
vocabulary.push([53, "PROB", 1]);
vocabulary.push([80, "PULL", 0]);
vocabulary.push([78, "PUSH", 0]);
vocabulary.push([50, "QUAN", 1]);
vocabulary.push([106, "QUIT", 0]);
vocabulary.push([105, "R", 0]);
vocabulary.push([92, "RACK", 1]);
vocabulary.push([108, "RE", 0]);
vocabulary.push([205, "READ", 0]);
vocabulary.push([68, "RED", 1]);
vocabulary.push([105, "REDE", 0]);
vocabulary.push([37, "REM", 1]);
vocabulary.push([37, "REMI", 1]);
vocabulary.push([37, "REMINDANOT", 1]);
vocabulary.push([102, "REMO", 0]);
vocabulary.push([108, "REST", 0]);
vocabulary.push([54, "ROBO", 1]);
vocabulary.push([73, "ROLL", 0]);
vocabulary.push([2, "S", 0]);
vocabulary.push([107, "SAVE", 0]);
vocabulary.push([109, "SAY", 0]);
vocabulary.push([117, "SCOR", 0]);
vocabulary.push([5, "SE", 0]);
vocabulary.push([98, "SEAR", 0]);
vocabulary.push([90, "SEAT", 1]);
vocabulary.push([40, "SECU", 1]);
vocabulary.push([40, "SECURIPASS", 1]);
vocabulary.push([59, "SHAR", 1]);
vocabulary.push([38, "SHIE", 1]);
vocabulary.push([38, "SHIELD", 1]);
vocabulary.push([29, "SHOC", 1]);
vocabulary.push([29, "SHOCKCAPE", 1]);
vocabulary.push([109, "SHOU", 0]);
vocabulary.push([96, "SHUT", 1]);
vocabulary.push([46, "SIGN", 1]);
vocabulary.push([115, "SLAY", 0]);
vocabulary.push([39, "SLI", 1]);
vocabulary.push([73, "SLIN", 0]);
vocabulary.push([39, "SLIP", 1]);
vocabulary.push([75, "SMOK", 0]);
vocabulary.push([2, "SOUT", 0]);
vocabulary.push([2, "SOUTH", 0]);
vocabulary.push([52, "SPEA", 1]);
vocabulary.push([33, "SPON", 1]);
vocabulary.push([33, "SPONGE", 1]);
vocabulary.push([74, "SQUE", 0]);
vocabulary.push([27, "STAT", 1]);
vocabulary.push([27, "STATIC", 1]);
vocabulary.push([16, "STIC", 0]);
vocabulary.push([16, "STICK", 1]);
vocabulary.push([106, "STOP", 0]);
vocabulary.push([63, "STOR", 1]);
vocabulary.push([21, "SULP", 1]);
vocabulary.push([21, "SULPHURTAB", 1]);
vocabulary.push([6, "SW", 0]);
vocabulary.push([100, "T", 0]);
vocabulary.push([21, "TAB", 1]);
vocabulary.push([21, "TABL", 1]);
vocabulary.push([100, "TAKE", 0]);
vocabulary.push([13, "TELE", 1]);
vocabulary.push([88, "TENT", 1]);
vocabulary.push([50, "TERM", 1]);
vocabulary.push([2, "THEN", 5]);
vocabulary.push([39, "THIN", 1]);
vocabulary.push([39, "THINKSLIP", 1]);
vocabulary.push([101, "THRO", 0]);
vocabulary.push([20, "TITA", 1]);
vocabulary.push([20, "TITANINIUM", 1]);
vocabulary.push([59, "TRAC", 1]);
vocabulary.push([61, "TROO", 1]);
vocabulary.push([23, "TUBE", 1]);
vocabulary.push([202, "TURN", 0]);
vocabulary.push([9, "U", 0]);
vocabulary.push([72, "UNLO", 0]);
vocabulary.push([9, "UP", 0]);
vocabulary.push([4, "W", 0]);
vocabulary.push([206, "WASH", 0]);
vocabulary.push([41, "WATE", 1]);
vocabulary.push([41, "WATER", 1]);
vocabulary.push([103, "WEAR", 0]);
vocabulary.push([4, "WEST", 0]);
vocabulary.push([79, "WHIT", 1]);
vocabulary.push([30, "WHIZ", 1]);
vocabulary.push([30, "WHIZZ", 1]);
vocabulary.push([110, "XXXX", 1]);
vocabulary.push([82, "YELL", 1]);



// SYS MESSAGES

total_sysmessages=68;

sysmessages = [];

sysmessages[0] = "Everything is dark.<br>";
sysmessages[1] = "You can also see:--<br>";
sysmessages[2] = "I await your command.<br>";
sysmessages[3] = "I'm ready for your instructions.<br>";
sysmessages[4] = "What are your instructions?<br>";
sysmessages[5] = "Give me your command.<br>";
sysmessages[6] = "Sorry I don't understand that. Try some different words.<br>";
sysmessages[7] = "You can't go in that direction.<br>";
sysmessages[8] = "You can't do that.<br>";
sysmessages[9] = "You are carrying:--";
sysmessages[10] = "                          (worn)";
sysmessages[11] = "Nothing at all.<br>";
sysmessages[12] = "Do you really want to quit now?<br>";
sysmessages[13] = "{CLASS|center|END OF GAME}Do you want  to try again?<br>";
sysmessages[14] = "Bye. Have a nice day.<br>";
sysmessages[15] = "OK.<br>";
sysmessages[16] = "{CLASS|center|Press any key to continue}<br>";
sysmessages[17] = "You have taken ";
sysmessages[18] = " turn";
sysmessages[19] = "s";
sysmessages[20] = ".<br>";
sysmessages[21] = "You have completed ";
sysmessages[22] = "% of the game.<br>";
sysmessages[23] = "You're not wearing it.<br>";
sysmessages[24] = "You can't carry any more — your hands are full.<br>";
sysmessages[25] = "You don't have it.<br>";
sysmessages[26] = "It's not here.<br>";
sysmessages[27] = "You can't carry any more.<br>";
sysmessages[28] = "You don't have it.<br>";
sysmessages[29] = "You're already wearing it.<br>";
sysmessages[30] = "Y<br>";
sysmessages[31] = "N<br>";
sysmessages[32] = "More…<br>";
sysmessages[33] = "><br>";
sysmessages[34] = "\n<br>";
sysmessages[35] = "\nTime passes…<br>";
sysmessages[36] = "";
sysmessages[37] = "";
sysmessages[38] = "";
sysmessages[39] = "";
sysmessages[40] = "\nYou can't do that.<br>";
sysmessages[41] = "\nYou can't do that.<br>";
sysmessages[42] = "\nYou can't carry any more.<br>";
sysmessages[43] = "\n  <br>";
sysmessages[44] = "\nYou put {OREF} into<br>";
sysmessages[45] = "\n{OREF} is not into<br>";
sysmessages[46] = "\n<br>";
sysmessages[47] = "\n<br>";
sysmessages[48] = "\n<br>";
sysmessages[49] = "\n> <br>";
sysmessages[50] = "\nYou can't do that.<br>";
sysmessages[51] = "\n.<br>";
sysmessages[52] = "\nThat is not into<br>";
sysmessages[53] = "\nnothing at all<br>";
sysmessages[54] = "\nFile not found.<br>";
sysmessages[55] = "\nFile corrupt.<br>";
sysmessages[56] = "\nI/O error. File not saved.<br>";
sysmessages[57] = "\nDirectory full.<br>";
sysmessages[58] = "\nPlease enter savegame name you used when saving the game status.";
sysmessages[59] = "\nInvalid savegame name. Please check the name you entered is correct, and make sure you are trying to load the game from the same browser you saved it.<br>";
sysmessages[60] = "\nPlease enter savegame name. Remember to note down the name you choose, as it will be requested in order to restore the game status.";
sysmessages[61] = "\n<br>";
sysmessages[62] = "\nSorry? Please try other words.<br>";
sysmessages[63] = "\nHere<br>";
sysmessages[64] = "\nyou can see<br>";
sysmessages[65] = "\nyou can see<br>";
sysmessages[66] = "\ninside you see<br>";
sysmessages[67] = "\non top you see<br>\n\n\n\n\n\n";

// USER MESSAGES

total_messages=14;

messages = [];

messages[1000] = "\nExits: ";
messages[1001] = "\nYou can't see any exits\n";
messages[1003] = "{ACTION|nort|nort}";
messages[1004] = "{ACTION|sout|sout}";
messages[1005] = "{ACTION|east|east}";
messages[1006] = "{ACTION|west|west}";
messages[1007] = "{ACTION|ne|ne}";
messages[1008] = "{ACTION|nw|nw}";
messages[1009] = "{ACTION|se|se}";
messages[1010] = "{ACTION|sw|sw}";
messages[1011] = "{ACTION|up|up}";
messages[1012] = "{ACTION|jump|jump}";
messages[1013] = "{ACTION|in|in}";
messages[1014] = "{ACTION|out|out}\n\n\n\n\n\n";

// WRITE MESSAGES

total_writemessages=200;

writemessages = [];

writemessages[0] = "RESPONSE_START";
writemessages[1] = "RESPONSE_USER";
writemessages[2] = "The door is damaged-you are unable to move it!";
writemessages[3] = "The door is damaged-you are unable to move it!";
writemessages[4] = "You duck as enemy fire pins you down!";
writemessages[5] = "Hand-rolled";
writemessages[6] = "The major drive system fires as the 4TH stage seperates-you are in direct line with the boosters";
writemessages[7] = "Energy barriers block you!";
writemessages[8] = "Energy barriers block you!";
writemessages[9] = "You scramble up the ladder..";
writemessages[10] = "You can't quite reach the vent!";
writemessages[11] = "You scramble up the ladder..";
writemessages[12] = "With what?";
writemessages[13] = "It's already lit!";
writemessages[14] = "The dart pierces the tank as destructor bolts wreck your shield-with a loud hiss the tanks burst, suffocating Erra Quann!Out of control the\nmachine skids off trailing smoke";
writemessages[15] = "A small dart shoots out!";
writemessages[16] = "And which one might that be, mega-brain?";
writemessages[17] = "And which one might that be, mega-brain?";
writemessages[18] = "The damaged controls spark into life-with a roar the plutonium based engines strain and in a huge explosion tear your ship apart!";
writemessages[19] = "The damaged engines ignite and the fighter erupts-a new star is suddenly visible from the planet surface!";
writemessages[20] = "A thin tube snakes out from the ship and docks with the Quann Tulla. A panel lights, indicating docking tube insecure!";
writemessages[21] = "Nothing happens";
writemessages[22] = "QUANN TULLA IN ORBIT AROUND ORION &#35; 3";
writemessages[23] = "The ships engines whine briefly but soon fade-the ship is beyond repair!";
writemessages[24] = "The ships hyper-drive engages!";
writemessages[25] = "ENEMY FIGHTERS APPROACHING SHIP!";
writemessages[26] = "Oh well-the exercise will do you good!";
writemessages[27] = "The door is locked.";
writemessages[28] = "The door unlocks and you enter.";
writemessages[29] = "The door is damaged-you are unable to move it!";
writemessages[30] = "The key vibrates the cupboard\nto wreckage!";
writemessages[31] = "With what?";
writemessages[32] = "The key vibrates the cupboard\nto wreckage!";
writemessages[33] = "The crane lunges at the ball, but totally misjudging its weight it falls headlong into the waste chute, muttering\nloudly to itself!";
writemessages[34] = "At what?";
writemessages[35] = "A thick blob of ultra-yuk III &#34;Superfast&#34;glue squirts out!";
writemessages[36] = "The glue covers the probe! &#34;NEAGH!&#34;it squeaks,&#34;I'M OFF!&#34;\nIt squelches loudly away..";
writemessages[37] = "The light fades from the room briefly, and in the darkness the sharpshot is cornered by its own ricochets!There is a loud bang..";
writemessages[38] = "As you handle the sponge all the light around you seems to drain, but slowly fades back";
writemessages[39] = "GOVT. WARNING-CIGARS CAN\nDAMAGE YOUR HEALTH!";
writemessages[40] = "It's not lit, dum-dum!";
writemessages[41] = "The computer examines the slip and with much consternation sets about the problem. After a few seconds it self-destructs, having proved to itself that it did not exist!";
writemessages[42] = "Nobody here wants it!";
writemessages[43] = "The jets auto-lite, raising you briefly, but soon fade.";
writemessages[44] = "A shot rings out!";
writemessages[45] = "The Droid rolls down the passage and hits the damaged doors-smoke pours up the corridor but as it clears you see the field has been destroyed!";
writemessages[46] = "The droid is held in an inert field-you need a power supply!";
writemessages[47] = "You're not strong enough!";
writemessages[48] = "Oh well-the exercise will do you good!";
writemessages[49] = "You will insist in fiddling\nwith things, won't you?A trapdoor opens beneath your feet..";
writemessages[50] = ">TERMINAL ONE ACCESSED\nQUANN DATAFILE LAST ENTRY.. massive negative power surge destroying crew-scanners show HighDome 1 source of energy- >DATALOCK";
writemessages[51] = "The computer sinks into a recess in the floor!";
writemessages[52] = ">TERMINAL 2 ACCESSED\n QUANN DATAFILE 2 HighDome 1 info stored-most advanced, self dependent fighting machine yet is also responsible for attack on Quann Tulla. Highdome moving west to present location. Operation manuals in library computer-code no. deleted from memory-engine units intact- >DATALOCK";
writemessages[53] = "The computer sinks into a recess in the floor!";
writemessages[54] = ">TERMINAL 3 ACCESSED\nInterlock 3 doors switched off main computer housed N. of bridge{CLASS|center|         >DATALOCK}";
writemessages[55] = "The computer sinks into a recess in the floor!";
writemessages[56] = "It does does not fit-and is hastily chewed up!";
writemessages[57] = "FUNCTION 1:INTERLOCK OPEN.";
writemessages[58] = "It does does not fit-and is hastily chewed up!";
writemessages[59] = "It does does not fit-and is hastily chewed up!";
writemessages[60] = "P-H-O-N-E--H-O-M-E-!";
writemessages[61] = "The photon pack slots into place easily and with a whoosh of spray the pod launches itself from the lake and into the higher atmosphere. Taking the controls you set the pod on course for home-WELL DONE!YOU HAVE ESCAPED!!";
writemessages[62] = "You find nothing";
writemessages[63] = "You find nothing";
writemessages[64] = "You find nothing";
writemessages[65] = "You find nothing";
writemessages[66] = "You find nothing";
writemessages[67] = "You find nothing";
writemessages[68] = "You find nothing";
writemessages[69] = "You find nothing";
writemessages[70] = "You find nothing";
writemessages[71] = "You find nothing";
writemessages[72] = "You find nothing";
writemessages[73] = "Vocal slave operant.( Comjump Ltd.)";
writemessages[74] = "Vocal slave operant.( Comjump Ltd.)";
writemessages[75] = "A small plastic card of magnetic tape, it enables the owner to access certain computer systems";
writemessages[76] = "Hand-rolled";
writemessages[77] = "Hand-rolled";
writemessages[78] = "A long tube of volatile gas-it looks like some kind of weapon";
writemessages[79] = "Large Red decals read,&#34;Quann Tulla Souvenir Shop&#34;";
writemessages[80] = "It's lead.";
writemessages[81] = "A twin-thruster hover-pack";
writemessages[82] = "An electronic key-suitable for most locks";
writemessages[83] = "They're very light!";
writemessages[84] = "A small wisp of acid rises as you handle it..";
writemessages[85] = "recently devalued to a 0.05 Cred piece.";
writemessages[86] = "Amidst the &#34;Techno-flash&#34;adverts you can just about read, SQUEEZET O USE>.";
writemessages[87] = "A small negapos emiter circuit it emits negative energy in huge waves-it appears to be switched to overload!";
writemessages[88] = "Microfilm code-lock protected it is marked&#34;ION DRIVE DESIGN&#34;";
writemessages[89] = "An anti-ion particle imploder. With a suitable detonator, and placed in the heart of the chain drive it will tear the Quann Tulla apart!";
writemessages[90] = "A standard Never-Ready +2";
writemessages[91] = "Insulation-lined to prevent most electric shocks.";
writemessages[92] = "&#34;Muscle-enhancers to increase even the puniest wearers push&#34;";
writemessages[93] = "A considerable advance on the Amazonian standard model-a full dart clip is loaded.";
writemessages[94] = "Made to measure";
writemessages[95] = "As you handle the sponge all the light around you seems to drain, but slowly fades back";
writemessages[96] = "Voice-encoded, it allows the user to operate certain teleports";
writemessages[97] = "A small metal-rimmed &#34;breather&#34; tank it holds a full capacity of breathable air.";
writemessages[98] = "For underwater use";
writemessages[99] = "It reads, &#34;Library code Operation Manuals, ENTER CODE XXXX&#34;";
writemessages[100] = "A personal deflector shield if worn";
writemessages[101] = "It holds a massive mathematical problem that seems to try and prove that 1+1=0?";
writemessages[102] = "For access to highly sensitive areas";
writemessages[103] = "Looks very dodgy!";
writemessages[104] = "With a suitable power field, it would make a very powerful bomb";
writemessages[105] = "A hand-held weapon";
writemessages[106] = "Sol-Telecom issue.";
writemessages[107] = "The heart of the Empire-type cutter craft.";
writemessages[108] = "The sign-post has been switched!";
writemessages[109] = "The first terminal of the Quann Tulla computer system";
writemessages[110] = "The 2nd terminal of the Quann Tulla computer system";
writemessages[111] = "The 3rd terminal of the Quann Tulla computer system";
writemessages[112] = "The very heart of the ships operation modules";
writemessages[113] = "The latest model of the Tete a' Tete systems";
writemessages[114] = "A highly logical maintenance operant. It appears to be quite agitated by your appearance.";
writemessages[115] = "It has been thoroughly cleaned s mells faintly of Domesta +     b leach. It sniffs loudly at you, r unning an electric eye up and  d own..";
writemessages[116] = "A purpose built all operation machine, it is in very poor shape";
writemessages[117] = "A purpose built all operation machine, it is in very poor shape";
writemessages[118] = "A vacuum-sealed floatbubble of indeterminate armour plating it contains the very latest in all technological equipment. A tinted dome tops the bodywork and huge air tanks are housed at the rear -it buzzes alarmingly!";
writemessages[119] = "Nobody here wants it!";
writemessages[120] = "A satellite loading arm";
writemessages[121] = "A small box containing photo- 'lectric eye cells it scans the immediate area relentlessly";
writemessages[122] = "An instant reference databank, it is code accessed for ease of use";
writemessages[123] = "It's nothing special";
writemessages[124] = "The sign-post has been switched!";
writemessages[125] = "Random violence is not recommended!";
writemessages[126] = "A catal deposit of the remaining anti-neg field that attacked the ship";
writemessages[127] = "Sol Telecom coin operated. Cheap rates at weekends.";
writemessages[128] = "It has been crushed during the voyage-an ordinary key won't open it!";
writemessages[129] = "Thick metal cut bar";
writemessages[130] = "Large Red decals read,&#34;Quann Tulla Souvenir Shop&#34;";
writemessages[131] = "A special lightweight Antigrav device-it decreases your weight if worn!";
writemessages[132] = "It is running-just get it to the weakest engine chain and it will auto-arm!";
writemessages[133] = "It's nothing special";
writemessages[134] = "The ball shrinks!";
writemessages[135] = "The lightstick explodes, engulfing you in flames!";
writemessages[136] = "The Empire guards fall upon you with cries of victory but the lightstick explodes as you fall! The troops take the full force of the blast&#59;shielding you!";
writemessages[137] = "The ball grows!";
writemessages[138] = "A massive auto-arm crane races from the shadows and with a\nloud rasp hurls you through a garbage chute into space.. (Weigh yourself down next time!)";
writemessages[139] = "The tablet destroys the grill!";
writemessages[140] = "Okay-but it's no good in it's present state!";
writemessages[141] = "The teleport is damaged.. you are torn apart, atom by atom!";
writemessages[142] = "You can't teleport without wearing a working teleport bracelet!";
writemessages[143] = "You can't teleport without wearing a working teleport bracelet!";
writemessages[144] = "DEFENCE SYSTEMS REMAIN INTACT- TELEPORT SELF-DESTRUCT FUNCTION TO BE FULFILLED- Statlasers shoot out!";
writemessages[145] = "The computer sinks into a recess in the floor!";
writemessages[146] = "RESPONSE_DEFAULT_START";
writemessages[147] = "Mumble.. Mumble.. Talking to our- selves now, are we?";
writemessages[148] = "Random violence is not recommended!";
writemessages[149] = "Prime directive 4/3:Random violence is not recommended!";
writemessages[150] = "Hygiene clone II appears from nowhere with a bar of soap to clean out your mouth!";
writemessages[151] = "Without a portacube de-coder\nthe code is quite beyond you!";
writemessages[152] = "How wonderfully hygenic!";
writemessages[153] = "RESPONSE_DEFAULT_END";
writemessages[154] = "PRO1";
writemessages[155] = "PRO2";
writemessages[156] = "The temperature is being lowered as a defensive measure by Quann terminal 2-It must be turned off immediately!";
writemessages[157] = "You are freezing cold!";
writemessages[158] = "You freeze to death!";
writemessages[159] = "You have suffocated!";
writemessages[160] = "You fall through the hole!";
writemessages[161] = "The force field is attracted to the metal mask--you are blasted!";
writemessages[162] = "You cannot breathe beneath the water--unable to reach the surface you drown!";
writemessages[163] = "You sink slowly beneath the surface to the sea-bed.";
writemessages[164] = "A score of destructor bolts shoot out!";
writemessages[165] = "Destructor bolts are deflected by the shield, but the sheer weight of them forces you to the ground!";
writemessages[166] = "You are very thirsty-the teleport has dehydrated you--you desperately need water!";
writemessages[167] = "Without water, you die of thirst!";
writemessages[168] = "A massive auto-arm crane races from the shadows and with a\nloud rasp hurls you through a garbage chute into space.. (Weigh yourself down next time!)";
writemessages[169] = "Blinding lights dazzle you as\na sharpshot hunter Tracer fires a volley of tracer shots-deadly accurate, every one hits!";
writemessages[170] = "The sharpshoot pauses briefly, considering its aim..";
writemessages[171] = "The sharpshot, having taken aim blasts a volley at you as you enter!";
writemessages[172] = "The creature sees the thick clouds of smoke and turns a\ndark shade of green-&#34;UGH!&#34;\nit yells,&#34;HUMANS!&#34;-and swiftly disappears.";
writemessages[173] = "A remaining securi-robot rushes to you and seeing you have no securi-card carries out it sole purpose. It shoots you.";
writemessages[174] = "A securi-robot rushes up to you and examines the card.&#34;THAT'LL DO NICELY!&#34;it clicks, and zooms away!";
writemessages[175] = "The boosterpak auto-fires as a light net rises from the floor! The jets raise you above the net of energy, but the resulting explosion destroys both the trap and the boosterpak, throwing you to the ground!";
writemessages[176] = "A latticed energy net shoots across the room at your feet--there is no time to act!";
writemessages[177] = "The temperature is being lowered as a defensive measure by Quann terminal 2--It must be turned off immediately!";
writemessages[178] = "The sight of the gun throws the troops into panic-they stage a tactical withdrawal..";
writemessages[179] = "The robot produces an enormous scrubbing brush-&#34;that's your lot!&#34;it shouts-&#34;stand by for a scrubbing!&#34;.. a mass of metal bristles engulf you…";
writemessages[180] = "The robot produces an enormous scrubbing brush-&#34;that's your lot!&#34;it shouts-&#34;stand by for a scrubbing!&#34;.. a mass of metal bristles engulf you…";
writemessages[181] = "The probe descends upon you in\na rage-&#34;THEY'RE MINE!&#34;it yells, carrying you off to the reproc. vats!";
writemessages[182] = "The static disrupter and limpet bomb lock together to create an Emploder disc!";
writemessages[183] = "The Crane arm stops in mid-lift, watching you carefully, eyeing the lead ball..";
writemessages[184] = "The matter displacer disrupts the displacer field and in a surge of anti-matter the room explodes!";
writemessages[185] = "With a huge explosion the ship is torn apart!You are successful but fail to escape!";
writemessages[186] = "The Emploder flies from your arms and in a flurry of sparks disappears into the engine drive!You have started a chain reaction-abandon ship!The Quann Tulla is about to explode!";
writemessages[187] = "QUANN TULLA SELF DESTRUCTION IMMINENT!";
writemessages[188] = "The entire keel of the ship shifts below you-An enormous wave of energy rises from every direction, shredding the ship as it rushes to engulf you!";
writemessages[189] = "Massive bolts of electricity shoot from the cables, electrocuting you instantly!";
writemessages[190] = "Massive bolts of electricity shoot from the cables but the ShocCape insulates you!";
writemessages[191] = "Without the correct footwear\nthe rolling of the damaged ship pitches you into space..";
writemessages[192] = "The magnet boots hold you firm.";
writemessages[193] = "You have suffocated!";
writemessages[194] = "You can't breathe!";
writemessages[195] = "You are too heavy for the tube! With a loud tearing of fabric it collapses beneath you, hurling you into space..";
writemessages[196] = "The badge lowers your weight sufficiently enough to let you cross!";
writemessages[197] = "Enemy guards leap upon you from the undergrowth!";
writemessages[198] = "Empire guards fall upon you with cries of victory!";
writemessages[199] = "Enemy guards are closing in!";

// LOCATION MESSAGES

total_location_messages=127;

locations = [];

locations[0] = "You are lieing in an leaking air filled suspend-bubble in the uni-am chamber of your ship. Registers on the wall indicate your waking--but the other instruments are all damaged. The only exit is {ACTION|out|out}.";
locations[1] = "You are standing {ACTION|in|in} the Suspend Am chamber-sparks fly from the machines all around you, and the air is full of thick, choking black smoke.";
locations[2] = "You are in the cargo bay-several crates and boxes are shattered all about you, and wreckage is strewn about the floor. There is a storeroom {ACTION|south|south}, and the cockpit is {ACTION|east|east}. Gazing through the docking tube door to the {ACTION|north|north}, you can see it is swaying dangerously into space.";
locations[3] = "You are in a blackened store-room.";
locations[4] = "You are in the cockpit of the CRIMSON CLOUD II space hopper. Considerable damage appears to have occured to your ship during the flight-On the console before you only 3 buttons are lit:Red, Blue and Green. The massive hull of the QUANN TULLA fills your sight!";
locations[5] = "You are before the docking tube in a smoke-filled airlock. The tube before you is pocked with hundreds of small holes, making it look very pecarious!";
locations[6] = "You are struggling to maintain your balance in the tube. Harsh neon lights show up ahead.";
locations[7] = "Auto-speakers announce&#34;ENTERWAY: QUANN TULLA&#34;. You are in the brightly-lit cargo hanger of the Quann Tulla. Once a credit to the crew it is now in total squalor.";
locations[8] = "You are in the auto-decontam shower. Sterilised water pours down from above.";
locations[9] = "You are in the wiring circuits of the external computer centre- thick, disarrayed wires lead {ACTION|east|east}";
locations[10] = "You are outside a battered door on which is written,&#34;DANGER-HIGH VOLTAGE!&#34;";
locations[11] = "";
locations[12] = "You are on a slideway catwalk further along the platform. Platforms wind {ACTION|NE|NE},{ACTION|W|W}.";
locations[13] = "You are where the walkway ends before a severely damaged bulkhead.";
locations[14] = "";
locations[15] = "";
locations[16] = "You are at the computer defence command centre (external)-huge turrets, now silent, point aimlessly into space. You wonder how cunning the enemy is that attacks the Federations greatest flag ship from within, avoiding the advanced external systems..";
locations[17] = "You are in a rusty satellite. Wires hang from every wall";
locations[18] = "You are at a savage tear {ACTION|in|in} the hull&#59;vast piles of metal ready for disposal into space are piled everywhere.";
locations[19] = "You are in the central reference library-microfilm racks lead off in all directions.";
locations[20] = "You are in an empty Micro-rack store that runs {ACTION|E|E}/{ACTION|W|W}";
locations[21] = "You are in a large room-every wall is covered in smashed projector screens. Interlock 1\nis east.";
locations[22] = "You are in a burnt-out computer terminal room. An open air vent is just above your head.";
locations[23] = "You are before a totally jammed Securi-door.";
locations[24] = "You are in the Quann Terminal 1 housing. Once a busy command centre, it is now a burnt out shell.          ";
locations[25] = "You are impeded by a set of large ruptured doors.";
locations[26] = "You are crawling in a dusty air vent.";
locations[27] = "You are in a sealed corridor.";
locations[28] = "You are in the personal hygiene check-in. Sparkling clean, it looks as if it has just been dusted!";
locations[29] = "You are before the 1st interlock one through doors that connect the sections of the ship.";
locations[30] = "You are {ACTION|in|in} the Telecom station. Once the heart of the crews leisure time it is now empty.";
locations[31] = "You are in an icy corridor at a junction.";
locations[32] = "You are {ACTION|east|east} of a junction on a greasy walkway";
locations[33] = "You are standing on a small ledge in a chemi-bath degreaser pit.";
locations[34] = "You are in the 1st battery of the ion displacer turrets. Vast banks of machinery seem to run for miles in every direction, but piles of wreckage block the aisles as a result of the attack on the ship.";
locations[35] = "You are in the 2nd battery in\na confusion of thick cables. A sign reads&#34;TELEPORT {ACTION|N|N}.&#34;";
locations[36] = "You are in a dusty teleport station-simulated granite pillars cover every wall in &#34;Roman&#34;style:similarily, most\nare in ruins.";
locations[37] = "You are in battery 3 amidst a pile of immovable debris.";
locations[38] = "";
locations[39] = "You are at the end of the Ion imploder tube-the metal plating rises high above your head, and looks undamaged.";
locations[40] = "You are on a metal rimmed lip over the ion mount casing floating far below in space.";
locations[41] = "";
locations[42] = "You are on a circular floatway above the central housing case of the drive tube. Below you vast sparks leap to the rear of the ship out of sight, noiselessly. Huge arcs of light force you to look away.";
locations[43] = "You are in a long corridor that runs {ACTION|N|N}/{ACTION|S|S} to a bright neon light";
locations[44] = "You are in a small locker room. lockers cover every wall.";
locations[45] = "You are at a junction. A large signpost reads,&#34;Terminal 2 {ACTION|West|West}&#34;";
locations[46] = "You are inside a dusty old telephone box by a phone- amazingly enough, it seems much smaller on the inside than on the outside!";
locations[47] = "You are in the cental housing of Quann terminal 2";
locations[48] = "You are in a deserted shuttle port. Once a busy terminal, the empty cargo hangers and launch pads echo your footsteps eerily against the cracked plas-skin tinted dome high above. A Large shuttle craft lies slewed across the pad.";
locations[49] = "You are in the wreckage of the shuttle before the charred remnants of the pilots seat";
locations[50] = "You are in an engine inspection panel-unfortunatly, the engine has been destroyed.";
locations[51] = "You are studying the digital system electrical analyser, consisting of an arrangement of massive cogs-primitive as it may seem, the cogs seem to have been of primary engineering concern!";
locations[52] = "You are in the port side 1st missile bunker.";
locations[53] = "You are in the second bunker- judged by the condtion of the ship so far, it has suffered a major attack-but the missiles have not been armed or fired!";
locations[54] = "A swaying sign reads,&#34;MISSILE BUNKER 3&#34;Stars are visible via\na huge tear in the hull";
locations[55] = "You are rotating W-E on the East side inner wheel of the drive unit-ionglass tubes filter great magnetic explosions to the rear of the 5 Exom tube, storing the energy created in the engines.";
locations[56] = "You are in interlock 2. A sign reads,&#34;Booster-paks compulsory {ACTION|east|east}.&#34;";
locations[57] = "You are on the second rim of\nthe inner rim wheel above the ion tube. Imploder valves swing dangerously above, collecting stored energy!";
locations[58] = "You are on a stop-off jumpath above the rotating wheel. Strange lights revolve in merged colours far below";
locations[59] = "You are in an airlock between rotator wheels. Wires hang in disarray far above";
locations[60] = "You are at the junction of {ACTION|east|east} and {ACTION|west|west} rims at the base of the wheel:long floatways once led N and SW, but they are now burnt and twisted.";
locations[61] = "You are in an ante room before the doors of Interlock 3. A sign reads,&#34;Passing through here with out switching off Term.3 will result in automatic 3rd stage seperation.&#34;";
locations[62] = "You are below flashing lights in interlock 3-deep cracks in the floor indicate locked 3rd/4th stage docking units";
locations[63] = "";
locations[64] = "You are in a wrecked crew canteen. metal canteen racks lie discarded on the floor.";
locations[65] = "You are in a small anteroom that has recently been ransacked!";
locations[66] = "You are in a charred office-burnt plaswood lies everywhere and a strong smell of smoke hangs in the air.";
locations[67] = "You are {ACTION|north|north} of the wheel and south of a large exi-tube to the outer hull";
locations[68] = "You are in an exi-tube leading to a walkway between the tube and a radio station open to space";
locations[69] = "You are in a radio transmission room. All machinery has been ripped out and thick piles of dust coat the floor";
locations[70] = "You are at the base of a huge radio mast-lights flash brightly at the top far above, contrasting against the darkness all around!";
locations[71] = "You are on a metal walkway on the outer hull, open to space. The hull rotates below your feet, open to space";
locations[72] = "You are in a doorway {ACTION|north|north} of the walkway entering a pressure corridor bulging from the hull";
locations[73] = "From here a walkway points {ACTION|north|north} from where a pattern of strong lights shine.";
locations[74] = "You are in a brightly-lit alcove";
locations[75] = "This is the housing for Quann terminal 3";
locations[76] = "You are at the base of the 1st main computer databank silo. Far above the massive discdrives whirr in activity. The vast data- run rises in 3 blocks, each being about 0.5 Exoms high!";
locations[77] = "You are following a path between towering silo's lettered with the legend,&#34;MICRODRIV. IV&#34;";
locations[78] = "You are N. of the silo's standing by a strange pool of leaking oil that covers the ground.";
locations[79] = "";
locations[80] = "You are at a door blocking your view {ACTION|north|north}.";
locations[81] = "You are at a sign post that reads,&#34;BRIDGE-{ACTION|SOUTH|SOUTH}.&#34;";
locations[82] = "You are in a inversegrav lift that appears to move-but the sensation is so quick it is really difficult to tell.";
locations[83] = "You are at the main control panel on the bridge. Through the massive domed glass you look out onto the ship stretched out before you into the distance. Vast fields of machinery run to the edge of your vision, and seemingly just visible on the horizon of metal you can see your own ship dwarved by the hull of the Quann Tulla. A large lever is before you.";
locations[84] = "You are beneath a massive tinted dome overlooking the ship. The bridge is {ACTION|E|E}/{ACTION|W|W}.";
locations[85] = "You are at the edge of the ships guidance pre-sets. Large open draws poke out as if they had been searched in vain, whilst 3 buttons wink on the panel:-White yellow and Blue.";
locations[86] = "You are in an empty room-great clouds of dust rise from your feet as you enter..";
locations[87] = "You are in an inert energy field";
locations[88] = "You are at a junction with a massive, damaged shield-door.";
locations[89] = "You are in a miniature room\nthat houses the main computer- the roof is so small you have\nto stoop. Lite-vids show exit {ACTION|SE|SE}.";
locations[90] = "You are seated at the controls of a spacial fighter through the screen of which enemy fighters are visible approaching from a large planet!A large red button marked &#34;IGNITION&#34; is beside you!";
locations[91] = "You are at the connection of the Ion tube to the drive motors- the roar of the engines shakes the floor beneath your feet.";
locations[92] = "You are directly over the tube connector on a steel alloy bridge. West the full length of the ion tube becomes apparent as it runs the length of the ship- Eastwards the huge cones of the now inoperable boosterjets point into space far above your head.";
locations[93] = "You are before the door to the Quann Tulla engine room!A large sign over the door reads,\nALL VISITORS BEYOND THIS POINT PLEASE SHOW IDENTIFICATION.";
locations[94] = "You are at the edge of the 1st drive engine on a long metal pathway. The huge bays disappear far to the {ACTION|south|south} in a link of engines. The air is charged with static and the noise is deafening! ";
locations[95] = "You stand before the second link of the huge Ion drive engines. Vibrations move horizontal angle from lesser to upper degrees";
locations[96] = "YOu are at a small deck of motor guidance sensors-heavily shielded, they are nevertheless the weakest part of the engine drive units.";
locations[97] = "You are at the heart of the engine guidance system. A large red door marked &#34;STRICTLY NO ADMITTANCE&#34;is east whilst the walkway curves {ACTION|West|West}.";
locations[98] = "You are at a Greco-styled room emblazoned&#34;TELEPORT STATION 2&#34; by a glowing teleport computer";
locations[99] = "You are on top of a high sand dune above a massive desert. The sun is high in the sky, the heat unbearable. You are on the edge of the desert whilst a tropical rain forest lies in a deep valley before you.";
locations[100] = "You are on the edge of a desert above a thick tropical jungle. Through the heat haze a clearing is just visible by hacked path.";
locations[101] = "You are on the edge of the clearing {ACTION|south|south} of a dust cloud";
locations[102] = "You are on a cleared path through the jungle. A strange hut is {ACTION|west|west}.";
locations[103] = "Much to your surprise the small hut is just a disguise for an Empire-style power generator.. odd piles of concealing cables cover the floor.";
locations[104] = "You are further along the path that veers from {ACTION|N|N} to {ACTION|E|E}.";
locations[105] = "You are at the end of the path over a large clearing. Smoke rises from the {ACTION|east|east}.";
locations[106] = "You are on the edge of a tropical lake surrounded by trees. A steep bank falls into the water, from which comes a strange sheen";
locations[107] = "You are in clouded water in a deep lake, floating down to the sea bed amongst rank weeds";
locations[108] = "You are on the sea bed by a hidden underwater launch pad\nof Empire design.";
locations[109] = "You are {ACTION|in|in} an airlock of a small aqua sphere. Water drips from above";
locations[110] = "You are inside the aqua sphere. Harsh tube lights hang from a dank, claustrophobic corridor.";
locations[111] = "You are in the cockpit of a well armoured Atmos-Pod. All controls seem operational-but the power supply needs insertion.";
locations[112] = "You are floating in murky weed- clogged water above a small rock ledge.";
locations[113] = "You are in a clearing covered with numerous Empire-style fibre tents.";
locations[114] = "You are in a deserted Empire outpost:ashes lie recently scattered.";
locations[115] = "";
locations[116] = "You are lost in a thickly- wooded rain forest.";
locations[117] = "You are at the top of a high tree above the forest floor. A small rope walkway leads {ACTION|East|East}.";
locations[118] = "You are clinging to a rocking rope walkway leading to a high cliff.";
locations[119] = "You are on the edge of a tall granite cliff. Boulders litter the rock";
locations[120] = "You are west of a large rocket launch pad. All is oddly silent";
locations[121] = "You are on a dummy launch pad -similar from a distance to a real pad, but made of synthfibes.";
locations[122] = "You are in an empty Aqua-store, standing before a large cupboard";
locations[123] = "You are in a dummy ship made up to resemble a real ship-this must surely be a trap!A large ripped out panel swings open as you approach..";
locations[124] = "";
locations[125] = "";
locations[126] = "";

// CONNECTIONS

connections = [];
connections_start = [];

connections[0] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1 ];
connections[1] = [ -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1 ];
connections[2] = [ -1, 5, 3, 4, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[3] = [ -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[4] = [ -1, -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[5] = [ -1, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[6] = [ -1, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[7] = [ -1, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[8] = [ -1, -1, 7, 20, -1, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[9] = [ -1, 10, -1, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[10] = [ -1, -1, 9, -1, -1, -1, -1, -1, 8, -1, -1, -1, -1, -1, -1, -1 ];
connections[11] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[12] = [ -1, -1, -1, -1, 9, -1, -1, 13, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[13] = [ -1, -1, 16, -1, -1, -1, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[14] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[15] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[16] = [ -1, 13, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[17] = [ -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, 18, -1, -1, -1, -1, -1 ];
connections[18] = [ -1, -1, 17, -1, 16, -1, -1, -1, -1, -1, -1, -1, 17, -1, -1, -1 ];
connections[19] = [ -1, -1, -1, -1, 20, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[20] = [ -1, 21, -1, 19, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[21] = [ -1, -1, 20, -1, 22, -1, -1, 28, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[22] = [ -1, 25, -1, 21, 23, -1, -1, 27, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[23] = [ -1, -1, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[24] = [ -1, -1, -1, -1, -1, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[25] = [ -1, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[26] = [ -1, -1, -1, -1, -1, -1, -1, -1, 24, -1, -1, -1, -1, -1, 22, -1 ];
connections[27] = [ -1, -1, -1, -1, -1, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[28] = [ -1, -1, -1, -1, -1, -1, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[29] = [ -1, -1, -1, 30, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[30] = [ -1, -1, -1, 31, 29, -1, -1, -1, -1, -1, -1, -1, 46, -1, -1, -1 ];
connections[31] = [ -1, -1, -1, 32, 30, -1, -1, 43, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[32] = [ -1, -1, -1, 42, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[33] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 34, -1 ];
connections[34] = [ -1, -1, -1, 35, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[35] = [ -1, 36, -1, 37, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[36] = [ -1, -1, 35, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[37] = [ -1, 39, -1, -1, 35, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[38] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[39] = [ -1, 40, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[40] = [ -1, 55, 39, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[41] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[42] = [ -1, 43, -1, -1, 32, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[43] = [ -1, 45, 42, -1, -1, -1, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[44] = [ -1, -1, -1, 45, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[45] = [ -1, 47, 43, -1, 44, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[46] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 30, -1, -1, -1, -1, -1 ];
connections[47] = [ -1, -1, 45, 52, 48, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[48] = [ -1, -1, 49, 47, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[49] = [ -1, 48, 50, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[50] = [ -1, 49, -1, -1, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[51] = [ -1, -1, -1, 50, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[52] = [ -1, -1, -1, 53, 47, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[53] = [ -1, -1, 54, -1, 52, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[54] = [ -1, 53, 55, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[55] = [ -1, 54, 40, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[56] = [ -1, -1, -1, 57, 55, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[57] = [ -1, -1, -1, 58, 56, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[58] = [ -1, -1, 59, 67, 57, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[59] = [ -1, 58, -1, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[60] = [ -1, -1, 61, 64, 59, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[61] = [ -1, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[62] = [ -1, 61, -1, 76, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[63] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[64] = [ -1, 65, -1, -1, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[65] = [ -1, -1, 64, 66, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[66] = [ -1, -1, -1, -1, 65, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[67] = [ -1, 68, -1, -1, 58, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[68] = [ -1, 71, 67, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[69] = [ -1, -1, -1, -1, 70, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[70] = [ -1, -1, -1, 69, 71, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[71] = [ -1, -1, 68, 70, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[72] = [ -1, 73, -1, 71, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[73] = [ -1, 74, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[74] = [ -1, -1, 73, 75, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[75] = [ -1, -1, -1, -1, 74, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[76] = [ -1, -1, -1, 77, 62, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[77] = [ -1, -1, -1, 78, 76, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[78] = [ -1, -1, -1, 80, 77, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[79] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[80] = [ -1, 86, -1, 81, 78, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[81] = [ -1, -1, 82, -1, 80, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[82] = [ -1, 81, 84, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[83] = [ -1, -1, -1, -1, 84, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[84] = [ -1, 82, -1, 83, 85, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[85] = [ -1, -1, -1, 84, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[86] = [ -1, -1, 80, 87, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[87] = [ -1, -1, -1, -1, 86, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[88] = [ -1, -1, 87, 89, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[89] = [ -1, -1, -1, -1, 88, 91, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[90] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[91] = [ -1, -1, 92, -1, -1, -1, -1, -1, 89, -1, -1, -1, -1, -1, -1, -1 ];
connections[92] = [ -1, 91, 93, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[93] = [ -1, 92, 94, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[94] = [ -1, 93, 95, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[95] = [ -1, 94, 97, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[96] = [ -1, -1, -1, -1, 97, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[97] = [ -1, 95, -1, -1, 98, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[98] = [ -1, -1, -1, 97, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[99] = [ -1, -1, 100, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[100] = [ -1, 99, 101, 100, 100, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[101] = [ -1, 100, 102, 101, 101, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[102] = [ -1, 101, 104, -1, 103, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[103] = [ -1, -1, -1, 102, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[104] = [ -1, 102, -1, 105, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[105] = [ -1, 106, -1, 113, 104, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[106] = [ -1, -1, 105, -1, -1, -1, -1, -1, -1, -1, -1, -1, 107, -1, -1, -1 ];
connections[107] = [ -1, -1, -1, 108, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[108] = [ -1, -1, -1, 109, 107, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[109] = [ -1, -1, -1, 112, 108, -1, -1, -1, -1, -1, -1, -1, 110, -1, -1, -1 ];
connections[110] = [ -1, 111, 109, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[111] = [ -1, -1, 110, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[112] = [ -1, -1, -1, -1, 109, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[113] = [ -1, -1, -1, 114, 105, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[114] = [ -1, -1, -1, 116, 113, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[115] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[116] = [ -1, 116, 116, 116, 114, -1, -1, -1, -1, 117, -1, -1, -1, -1, -1, -1 ];
connections[117] = [ -1, -1, -1, 118, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 116, -1 ];
connections[118] = [ -1, -1, -1, 119, 117, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[119] = [ -1, -1, -1, -1, 118, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[120] = [ -1, -1, 119, 121, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[121] = [ -1, 123, -1, 122, 120, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[122] = [ -1, -1, -1, -1, 121, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[123] = [ -1, -1, 121, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[124] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[125] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[126] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];

connections_start[0] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1 ];
connections_start[1] = [ -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1 ];
connections_start[2] = [ -1, 5, 3, 4, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[3] = [ -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[4] = [ -1, -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[5] = [ -1, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[6] = [ -1, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[7] = [ -1, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[8] = [ -1, -1, 7, 20, -1, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[9] = [ -1, 10, -1, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[10] = [ -1, -1, 9, -1, -1, -1, -1, -1, 8, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[11] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[12] = [ -1, -1, -1, -1, 9, -1, -1, 13, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[13] = [ -1, -1, 16, -1, -1, -1, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[14] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[15] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[16] = [ -1, 13, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[17] = [ -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, 18, -1, -1, -1, -1, -1 ];
connections_start[18] = [ -1, -1, 17, -1, 16, -1, -1, -1, -1, -1, -1, -1, 17, -1, -1, -1 ];
connections_start[19] = [ -1, -1, -1, -1, 20, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[20] = [ -1, 21, -1, 19, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[21] = [ -1, -1, 20, -1, 22, -1, -1, 28, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[22] = [ -1, 25, -1, 21, 23, -1, -1, 27, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[23] = [ -1, -1, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[24] = [ -1, -1, -1, -1, -1, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[25] = [ -1, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[26] = [ -1, -1, -1, -1, -1, -1, -1, -1, 24, -1, -1, -1, -1, -1, 22, -1 ];
connections_start[27] = [ -1, -1, -1, -1, -1, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[28] = [ -1, -1, -1, -1, -1, -1, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[29] = [ -1, -1, -1, 30, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[30] = [ -1, -1, -1, 31, 29, -1, -1, -1, -1, -1, -1, -1, 46, -1, -1, -1 ];
connections_start[31] = [ -1, -1, -1, 32, 30, -1, -1, 43, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[32] = [ -1, -1, -1, 42, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[33] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 34, -1 ];
connections_start[34] = [ -1, -1, -1, 35, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[35] = [ -1, 36, -1, 37, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[36] = [ -1, -1, 35, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[37] = [ -1, 39, -1, -1, 35, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[38] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[39] = [ -1, 40, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[40] = [ -1, 55, 39, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[41] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[42] = [ -1, 43, -1, -1, 32, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[43] = [ -1, 45, 42, -1, -1, -1, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[44] = [ -1, -1, -1, 45, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[45] = [ -1, 47, 43, -1, 44, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[46] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 30, -1, -1, -1, -1, -1 ];
connections_start[47] = [ -1, -1, 45, 52, 48, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[48] = [ -1, -1, 49, 47, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[49] = [ -1, 48, 50, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[50] = [ -1, 49, -1, -1, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[51] = [ -1, -1, -1, 50, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[52] = [ -1, -1, -1, 53, 47, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[53] = [ -1, -1, 54, -1, 52, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[54] = [ -1, 53, 55, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[55] = [ -1, 54, 40, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[56] = [ -1, -1, -1, 57, 55, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[57] = [ -1, -1, -1, 58, 56, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[58] = [ -1, -1, 59, 67, 57, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[59] = [ -1, 58, -1, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[60] = [ -1, -1, 61, 64, 59, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[61] = [ -1, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[62] = [ -1, 61, -1, 76, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[63] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[64] = [ -1, 65, -1, -1, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[65] = [ -1, -1, 64, 66, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[66] = [ -1, -1, -1, -1, 65, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[67] = [ -1, 68, -1, -1, 58, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[68] = [ -1, 71, 67, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[69] = [ -1, -1, -1, -1, 70, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[70] = [ -1, -1, -1, 69, 71, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[71] = [ -1, -1, 68, 70, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[72] = [ -1, 73, -1, 71, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[73] = [ -1, 74, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[74] = [ -1, -1, 73, 75, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[75] = [ -1, -1, -1, -1, 74, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[76] = [ -1, -1, -1, 77, 62, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[77] = [ -1, -1, -1, 78, 76, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[78] = [ -1, -1, -1, 80, 77, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[79] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[80] = [ -1, 86, -1, 81, 78, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[81] = [ -1, -1, 82, -1, 80, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[82] = [ -1, 81, 84, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[83] = [ -1, -1, -1, -1, 84, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[84] = [ -1, 82, -1, 83, 85, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[85] = [ -1, -1, -1, 84, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[86] = [ -1, -1, 80, 87, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[87] = [ -1, -1, -1, -1, 86, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[88] = [ -1, -1, 87, 89, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[89] = [ -1, -1, -1, -1, 88, 91, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[90] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[91] = [ -1, -1, 92, -1, -1, -1, -1, -1, 89, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[92] = [ -1, 91, 93, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[93] = [ -1, 92, 94, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[94] = [ -1, 93, 95, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[95] = [ -1, 94, 97, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[96] = [ -1, -1, -1, -1, 97, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[97] = [ -1, 95, -1, -1, 98, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[98] = [ -1, -1, -1, 97, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[99] = [ -1, -1, 100, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[100] = [ -1, 99, 101, 100, 100, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[101] = [ -1, 100, 102, 101, 101, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[102] = [ -1, 101, 104, -1, 103, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[103] = [ -1, -1, -1, 102, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[104] = [ -1, 102, -1, 105, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[105] = [ -1, 106, -1, 113, 104, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[106] = [ -1, -1, 105, -1, -1, -1, -1, -1, -1, -1, -1, -1, 107, -1, -1, -1 ];
connections_start[107] = [ -1, -1, -1, 108, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[108] = [ -1, -1, -1, 109, 107, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[109] = [ -1, -1, -1, 112, 108, -1, -1, -1, -1, -1, -1, -1, 110, -1, -1, -1 ];
connections_start[110] = [ -1, 111, 109, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[111] = [ -1, -1, 110, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[112] = [ -1, -1, -1, -1, 109, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[113] = [ -1, -1, -1, 114, 105, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[114] = [ -1, -1, -1, 116, 113, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[115] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[116] = [ -1, 116, 116, 116, 114, -1, -1, -1, -1, 117, -1, -1, -1, -1, -1, -1 ];
connections_start[117] = [ -1, -1, -1, 118, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 116, -1 ];
connections_start[118] = [ -1, -1, -1, 119, 117, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[119] = [ -1, -1, -1, -1, 118, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[120] = [ -1, -1, 119, 121, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[121] = [ -1, 123, -1, 122, 120, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[122] = [ -1, -1, -1, -1, 121, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[123] = [ -1, -1, 121, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[124] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[125] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[126] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];


resources=[];


 //OBJECTS

objects = [];
objectsAttrLO = [];
objectsAttrHI = [];
objectsLocation = [];
objectsNoun = [];
objectsAdjective = [];
objectsWeight = [];
objectsAttrLO_start = [];
objectsAttrHI_start = [];
objectsLocation_start = [];
objectsWeight_start = [];

objects[0] = "an infradat data card";
objectsNoun[0] = 255;
objectsAdjective[0] = 255;
objectsLocation[0] = 252;
objectsLocation_start[0] = 252;
objectsWeight[0] = 1;
objectsWeight_start[0] = 1;
objectsAttrLO[0] = 1;
objectsAttrLO_start[0] = 1;
objectsAttrHI[0] = 0;
objectsAttrHI_start[0] = 0;

objects[1] = "a fat cigar";
objectsNoun[1] = 255;
objectsAdjective[1] = 255;
objectsLocation[1] = 252;
objectsLocation_start[1] = 252;
objectsWeight[1] = 1;
objectsWeight_start[1] = 1;
objectsAttrLO[1] = 0;
objectsAttrLO_start[1] = 0;
objectsAttrHI[1] = 0;
objectsAttrHI_start[1] = 0;

objects[2] = "a neon lightstick";
objectsNoun[2] = 255;
objectsAdjective[2] = 255;
objectsLocation[2] = 252;
objectsLocation_start[2] = 252;
objectsWeight[2] = 1;
objectsWeight_start[2] = 1;
objectsAttrLO[2] = 0;
objectsAttrLO_start[2] = 0;
objectsAttrHI[2] = 0;
objectsAttrHI_start[2] = 0;

objects[3] = "a curiously shaped ball of lead";
objectsNoun[3] = 255;
objectsAdjective[3] = 255;
objectsLocation[3] = 20;
objectsLocation_start[3] = 20;
objectsWeight[3] = 1;
objectsWeight_start[3] = 1;
objectsAttrLO[3] = 0;
objectsAttrLO_start[3] = 0;
objectsAttrHI[3] = 0;
objectsAttrHI_start[3] = 0;

objects[4] = "a twin-jet boosterpak";
objectsNoun[4] = 255;
objectsAdjective[4] = 255;
objectsLocation[4] = 17;
objectsLocation_start[4] = 17;
objectsWeight[4] = 1;
objectsWeight_start[4] = 1;
objectsAttrLO[4] = 0;
objectsAttrLO_start[4] = 0;
objectsAttrHI[4] = 0;
objectsAttrHI_start[4] = 0;

objects[5] = "a combulock key";
objectsNoun[5] = 255;
objectsAdjective[5] = 255;
objectsLocation[5] = 24;
objectsLocation_start[5] = 24;
objectsWeight[5] = 1;
objectsWeight_start[5] = 1;
objectsAttrLO[5] = 0;
objectsAttrLO_start[5] = 0;
objectsAttrHI[5] = 0;
objectsAttrHI_start[5] = 0;

objects[6] = "a set of titaninium ladders";
objectsNoun[6] = 255;
objectsAdjective[6] = 255;
objectsLocation[6] = 19;
objectsLocation_start[6] = 19;
objectsWeight[6] = 1;
objectsWeight_start[6] = 1;
objectsAttrLO[6] = 0;
objectsAttrLO_start[6] = 0;
objectsAttrHI[6] = 0;
objectsAttrHI_start[6] = 0;

objects[7] = "a sulphurtab";
objectsNoun[7] = 255;
objectsAdjective[7] = 255;
objectsLocation[7] = 28;
objectsLocation_start[7] = 28;
objectsWeight[7] = 1;
objectsWeight_start[7] = 1;
objectsAttrLO[7] = 0;
objectsAttrLO_start[7] = 0;
objectsAttrHI[7] = 0;
objectsAttrHI_start[7] = 0;

objects[8] = "a one-cred note";
objectsNoun[8] = 255;
objectsAdjective[8] = 255;
objectsLocation[8] = 16;
objectsLocation_start[8] = 16;
objectsWeight[8] = 1;
objectsWeight_start[8] = 1;
objectsAttrLO[8] = 0;
objectsAttrLO_start[8] = 0;
objectsAttrHI[8] = 0;
objectsAttrHI_start[8] = 0;

objects[9] = "a tube of permaglue";
objectsNoun[9] = 255;
objectsAdjective[9] = 255;
objectsLocation[9] = 23;
objectsLocation_start[9] = 23;
objectsWeight[9] = 1;
objectsWeight_start[9] = 1;
objectsAttrLO[9] = 0;
objectsAttrLO_start[9] = 0;
objectsAttrHI[9] = 0;
objectsAttrHI_start[9] = 0;

objects[10] = "a humming matter displacer";
objectsNoun[10] = 255;
objectsAdjective[10] = 255;
objectsLocation[10] = 252;
objectsLocation_start[10] = 252;
objectsWeight[10] = 1;
objectsWeight_start[10] = 1;
objectsAttrLO[10] = 0;
objectsAttrLO_start[10] = 0;
objectsAttrHI[10] = 0;
objectsAttrHI_start[10] = 0;

objects[11] = "the Quann Tulla operation manual";
objectsNoun[11] = 255;
objectsAdjective[11] = 255;
objectsLocation[11] = 252;
objectsLocation_start[11] = 252;
objectsWeight[11] = 1;
objectsWeight_start[11] = 1;
objectsAttrLO[11] = 0;
objectsAttrLO_start[11] = 0;
objectsAttrHI[11] = 0;
objectsAttrHI_start[11] = 0;

objects[12] = "a Gas lyter+";
objectsNoun[12] = 255;
objectsAdjective[12] = 255;
objectsLocation[12] = 9;
objectsLocation_start[12] = 9;
objectsWeight[12] = 1;
objectsWeight_start[12] = 1;
objectsAttrLO[12] = 0;
objectsAttrLO_start[12] = 0;
objectsAttrHI[12] = 0;
objectsAttrHI_start[12] = 0;

objects[13] = "a static disrupter";
objectsNoun[13] = 255;
objectsAdjective[13] = 255;
objectsLocation[13] = 33;
objectsLocation_start[13] = 33;
objectsWeight[13] = 1;
objectsWeight_start[13] = 1;
objectsAttrLO[13] = 0;
objectsAttrLO_start[13] = 0;
objectsAttrHI[13] = 0;
objectsAttrHI_start[13] = 0;

objects[14] = "a &#34;longalife&#34; battery";
objectsNoun[14] = 255;
objectsAdjective[14] = 255;
objectsLocation[14] = 252;
objectsLocation_start[14] = 252;
objectsWeight[14] = 1;
objectsWeight_start[14] = 1;
objectsAttrLO[14] = 0;
objectsAttrLO_start[14] = 0;
objectsAttrHI[14] = 0;
objectsAttrHI_start[14] = 0;

objects[15] = "a shockcape";
objectsNoun[15] = 255;
objectsAdjective[15] = 255;
objectsLocation[15] = 26;
objectsLocation_start[15] = 26;
objectsWeight[15] = 1;
objectsWeight_start[15] = 1;
objectsAttrLO[15] = 0;
objectsAttrLO_start[15] = 0;
objectsAttrHI[15] = 0;
objectsAttrHI_start[15] = 0;

objects[16] = "a set of whizz-curcuit cufflinks";
objectsNoun[16] = 255;
objectsAdjective[16] = 255;
objectsLocation[16] = 75;
objectsLocation_start[16] = 75;
objectsWeight[16] = 1;
objectsWeight_start[16] = 1;
objectsAttrLO[16] = 0;
objectsAttrLO_start[16] = 0;
objectsAttrHI[16] = 0;
objectsAttrHI_start[16] = 0;

objects[17] = "a blowpipe";
objectsNoun[17] = 255;
objectsAdjective[17] = 255;
objectsLocation[17] = 252;
objectsLocation_start[17] = 252;
objectsWeight[17] = 1;
objectsWeight_start[17] = 1;
objectsAttrLO[17] = 0;
objectsAttrLO_start[17] = 0;
objectsAttrHI[17] = 0;
objectsAttrHI_start[17] = 0;

objects[18] = "a pair of magnet boots";
objectsNoun[18] = 255;
objectsAdjective[18] = 255;
objectsLocation[18] = 54;
objectsLocation_start[18] = 54;
objectsWeight[18] = 1;
objectsWeight_start[18] = 1;
objectsAttrLO[18] = 0;
objectsAttrLO_start[18] = 0;
objectsAttrHI[18] = 0;
objectsAttrHI_start[18] = 0;

objects[19] = "a glowing sponge";
objectsNoun[19] = 255;
objectsAdjective[19] = 255;
objectsLocation[19] = 252;
objectsLocation_start[19] = 252;
objectsWeight[19] = 1;
objectsWeight_start[19] = 1;
objectsAttrLO[19] = 0;
objectsAttrLO_start[19] = 0;
objectsAttrHI[19] = 0;
objectsAttrHI_start[19] = 0;

objects[20] = "a teleport bracelet";
objectsNoun[20] = 255;
objectsAdjective[20] = 255;
objectsLocation[20] = 66;
objectsLocation_start[20] = 66;
objectsWeight[20] = 1;
objectsWeight_start[20] = 1;
objectsAttrLO[20] = 0;
objectsAttrLO_start[20] = 0;
objectsAttrHI[20] = 0;
objectsAttrHI_start[20] = 0;

objects[21] = "a natuflow airmask";
objectsNoun[21] = 255;
objectsAdjective[21] = 255;
objectsLocation[21] = 2;
objectsLocation_start[21] = 2;
objectsWeight[21] = 1;
objectsWeight_start[21] = 1;
objectsAttrLO[21] = 0;
objectsAttrLO_start[21] = 0;
objectsAttrHI[21] = 0;
objectsAttrHI_start[21] = 0;

objects[22] = "an aqualung";
objectsNoun[22] = 255;
objectsAdjective[22] = 255;
objectsLocation[22] = 252;
objectsLocation_start[22] = 252;
objectsWeight[22] = 1;
objectsWeight_start[22] = 1;
objectsAttrLO[22] = 0;
objectsAttrLO_start[22] = 0;
objectsAttrHI[22] = 0;
objectsAttrHI_start[22] = 0;

objects[23] = "a shrivelled remindanote";
objectsNoun[23] = 255;
objectsAdjective[23] = 255;
objectsLocation[23] = 89;
objectsLocation_start[23] = 89;
objectsWeight[23] = 1;
objectsWeight_start[23] = 1;
objectsAttrLO[23] = 0;
objectsAttrLO_start[23] = 0;
objectsAttrHI[23] = 0;
objectsAttrHI_start[23] = 0;

objects[24] = "an electroshield";
objectsNoun[24] = 255;
objectsAdjective[24] = 255;
objectsLocation[24] = 252;
objectsLocation_start[24] = 252;
objectsWeight[24] = 1;
objectsWeight_start[24] = 1;
objectsAttrLO[24] = 0;
objectsAttrLO_start[24] = 0;
objectsAttrHI[24] = 0;
objectsAttrHI_start[24] = 0;

objects[25] = "a thinkslip";
objectsNoun[25] = 255;
objectsAdjective[25] = 255;
objectsLocation[25] = 252;
objectsLocation_start[25] = 252;
objectsWeight[25] = 1;
objectsWeight_start[25] = 1;
objectsAttrLO[25] = 0;
objectsAttrLO_start[25] = 0;
objectsAttrHI[25] = 0;
objectsAttrHI_start[25] = 0;

objects[26] = "an ornate securipass";
objectsNoun[26] = 255;
objectsAdjective[26] = 255;
objectsLocation[26] = 252;
objectsLocation_start[26] = 252;
objectsWeight[26] = 1;
objectsWeight_start[26] = 1;
objectsAttrLO[26] = 0;
objectsAttrLO_start[26] = 0;
objectsAttrHI[26] = 0;
objectsAttrHI_start[26] = 0;

objects[27] = "a flask of cool water";
objectsNoun[27] = 255;
objectsAdjective[27] = 255;
objectsLocation[27] = 96;
objectsLocation_start[27] = 96;
objectsWeight[27] = 1;
objectsWeight_start[27] = 1;
objectsAttrLO[27] = 0;
objectsAttrLO_start[27] = 0;
objectsAttrHI[27] = 0;
objectsAttrHI_start[27] = 0;

objects[28] = "a LIMPETBOMB";
objectsNoun[28] = 255;
objectsAdjective[28] = 255;
objectsLocation[28] = 91;
objectsLocation_start[28] = 91;
objectsWeight[28] = 1;
objectsWeight_start[28] = 1;
objectsAttrLO[28] = 0;
objectsAttrLO_start[28] = 0;
objectsAttrHI[28] = 0;
objectsAttrHI_start[28] = 0;

objects[29] = "a handblaster";
objectsNoun[29] = 255;
objectsAdjective[29] = 255;
objectsLocation[29] = 252;
objectsLocation_start[29] = 252;
objectsWeight[29] = 1;
objectsWeight_start[29] = 1;
objectsAttrLO[29] = 0;
objectsAttrLO_start[29] = 0;
objectsAttrHI[29] = 0;
objectsAttrHI_start[29] = 0;

objects[30] = "A small coin";
objectsNoun[30] = 255;
objectsAdjective[30] = 255;
objectsLocation[30] = 46;
objectsLocation_start[30] = 46;
objectsWeight[30] = 1;
objectsWeight_start[30] = 1;
objectsAttrLO[30] = 0;
objectsAttrLO_start[30] = 0;
objectsAttrHI[30] = 0;
objectsAttrHI_start[30] = 0;

objects[31] = "an interat photon pack";
objectsNoun[31] = 255;
objectsAdjective[31] = 255;
objectsLocation[31] = 112;
objectsLocation_start[31] = 112;
objectsWeight[31] = 1;
objectsWeight_start[31] = 1;
objectsAttrLO[31] = 0;
objectsAttrLO_start[31] = 0;
objectsAttrHI[31] = 0;
objectsAttrHI_start[31] = 0;

objects[32] = "QUANN TULLA TERMINAL 1";
objectsNoun[32] = 255;
objectsAdjective[32] = 255;
objectsLocation[32] = 24;
objectsLocation_start[32] = 24;
objectsWeight[32] = 1;
objectsWeight_start[32] = 1;
objectsAttrLO[32] = 0;
objectsAttrLO_start[32] = 0;
objectsAttrHI[32] = 0;
objectsAttrHI_start[32] = 0;

objects[33] = "QUANN TERMINAL 2";
objectsNoun[33] = 255;
objectsAdjective[33] = 255;
objectsLocation[33] = 47;
objectsLocation_start[33] = 47;
objectsWeight[33] = 1;
objectsWeight_start[33] = 1;
objectsAttrLO[33] = 0;
objectsAttrLO_start[33] = 0;
objectsAttrHI[33] = 0;
objectsAttrHI_start[33] = 0;

objects[34] = "QUANN TERMINAL 3";
objectsNoun[34] = 255;
objectsAdjective[34] = 255;
objectsLocation[34] = 75;
objectsLocation_start[34] = 75;
objectsWeight[34] = 1;
objectsWeight_start[34] = 1;
objectsAttrLO[34] = 0;
objectsAttrLO_start[34] = 0;
objectsAttrHI[34] = 0;
objectsAttrHI_start[34] = 0;

objects[35] = "COMPUTER TERMINAL DATALINK &#35;A";
objectsNoun[35] = 255;
objectsAdjective[35] = 255;
objectsLocation[35] = 89;
objectsLocation_start[35] = 89;
objectsWeight[35] = 1;
objectsWeight_start[35] = 1;
objectsAttrLO[35] = 0;
objectsAttrLO_start[35] = 0;
objectsAttrHI[35] = 0;
objectsAttrHI_start[35] = 0;

objects[36] = "SPEAKTALK COMMUNI-1, marked; &#34;Insert cash to open interlock 1&#34;";
objectsNoun[36] = 255;
objectsAdjective[36] = 255;
objectsLocation[36] = 21;
objectsLocation_start[36] = 21;
objectsWeight[36] = 1;
objectsWeight_start[36] = 1;
objectsAttrLO[36] = 0;
objectsAttrLO_start[36] = 0;
objectsAttrHI[36] = 0;
objectsAttrHI_start[36] = 0;

objects[37] = "a humming maintenance probe floating in the air, eyeing you!";
objectsNoun[37] = 255;
objectsAdjective[37] = 255;
objectsLocation[37] = 16;
objectsLocation_start[37] = 16;
objectsWeight[37] = 1;
objectsWeight_start[37] = 1;
objectsAttrLO[37] = 0;
objectsAttrLO_start[37] = 0;
objectsAttrHI[37] = 0;
objectsAttrHI_start[37] = 0;

objects[38] = "a perfectly scrubbed personal hygiene robot";
objectsNoun[38] = 255;
objectsAdjective[38] = 255;
objectsLocation[38] = 28;
objectsLocation_start[38] = 28;
objectsWeight[38] = 1;
objectsWeight_start[38] = 1;
objectsAttrLO[38] = 0;
objectsAttrLO_start[38] = 0;
objectsAttrHI[38] = 0;
objectsAttrHI_start[38] = 0;

objects[39] = "a multitask traxdroid";
objectsNoun[39] = 255;
objectsAdjective[39] = 255;
objectsLocation[39] = 87;
objectsLocation_start[39] = 87;
objectsWeight[39] = 1;
objectsWeight_start[39] = 1;
objectsAttrLO[39] = 0;
objectsAttrLO_start[39] = 0;
objectsAttrHI[39] = 0;
objectsAttrHI_start[39] = 0;

objects[40] = "The hovering form of the most advanced fighting machine known to man, and at within its'glowing operator dome, the mocking figure of Erra Quann!Realisation comes at once-this is HIGHDOME 1!";
objectsNoun[40] = 255;
objectsAdjective[40] = 255;
objectsLocation[40] = 65;
objectsLocation_start[40] = 65;
objectsWeight[40] = 1;
objectsWeight_start[40] = 1;
objectsAttrLO[40] = 0;
objectsAttrLO_start[40] = 0;
objectsAttrHI[40] = 0;
objectsAttrHI_start[40] = 0;

objects[41] = "A powerlite lattice!";
objectsNoun[41] = 255;
objectsAdjective[41] = 255;
objectsLocation[41] = 58;
objectsLocation_start[41] = 58;
objectsWeight[41] = 1;
objectsWeight_start[41] = 1;
objectsAttrLO[41] = 0;
objectsAttrLO_start[41] = 0;
objectsAttrHI[41] = 0;
objectsAttrHI_start[41] = 0;

objects[42] = "A rotating autograb load crane!";
objectsNoun[42] = 255;
objectsAdjective[42] = 255;
objectsLocation[42] = 18;
objectsLocation_start[42] = 18;
objectsWeight[42] = 1;
objectsWeight_start[42] = 1;
objectsAttrLO[42] = 0;
objectsAttrLO_start[42] = 0;
objectsAttrHI[42] = 0;
objectsAttrHI_start[42] = 0;

objects[43] = "A Sharpshot hunter tracer";
objectsNoun[43] = 255;
objectsAdjective[43] = 255;
objectsLocation[43] = 74;
objectsLocation_start[43] = 74;
objectsWeight[43] = 1;
objectsWeight_start[43] = 1;
objectsAttrLO[43] = 0;
objectsAttrLO_start[43] = 0;
objectsAttrHI[43] = 0;
objectsAttrHI_start[43] = 0;

objects[44] = "The Quann Tulla library code computer.";
objectsNoun[44] = 255;
objectsAdjective[44] = 255;
objectsLocation[44] = 19;
objectsLocation_start[44] = 19;
objectsWeight[44] = 1;
objectsWeight_start[44] = 1;
objectsAttrLO[44] = 0;
objectsAttrLO_start[44] = 0;
objectsAttrHI[44] = 0;
objectsAttrHI_start[44] = 0;

objects[45] = "";
objectsNoun[45] = 255;
objectsAdjective[45] = 255;
objectsLocation[45] = 252;
objectsLocation_start[45] = 252;
objectsWeight[45] = 1;
objectsWeight_start[45] = 1;
objectsAttrLO[45] = 0;
objectsAttrLO_start[45] = 0;
objectsAttrHI[45] = 0;
objectsAttrHI_start[45] = 0;

objects[46] = "";
objectsNoun[46] = 255;
objectsAdjective[46] = 255;
objectsLocation[46] = 252;
objectsLocation_start[46] = 252;
objectsWeight[46] = 1;
objectsWeight_start[46] = 1;
objectsAttrLO[46] = 0;
objectsAttrLO_start[46] = 0;
objectsAttrHI[46] = 0;
objectsAttrHI_start[46] = 0;

objects[47] = "";
objectsNoun[47] = 255;
objectsAdjective[47] = 255;
objectsLocation[47] = 252;
objectsLocation_start[47] = 252;
objectsWeight[47] = 1;
objectsWeight_start[47] = 1;
objectsAttrLO[47] = 0;
objectsAttrLO_start[47] = 0;
objectsAttrHI[47] = 0;
objectsAttrHI_start[47] = 0;

objects[48] = "A mass of empire troops milling about the launch pad, weapons at the ready";
objectsNoun[48] = 255;
objectsAdjective[48] = 255;
objectsLocation[48] = 120;
objectsLocation_start[48] = 120;
objectsWeight[48] = 1;
objectsWeight_start[48] = 1;
objectsAttrLO[48] = 0;
objectsAttrLO_start[48] = 0;
objectsAttrHI[48] = 0;
objectsAttrHI_start[48] = 0;

objects[49] = "A crackling force-field!";
objectsNoun[49] = 255;
objectsAdjective[49] = 255;
objectsLocation[49] = 107;
objectsLocation_start[49] = 107;
objectsWeight[49] = 1;
objectsWeight_start[49] = 1;
objectsAttrLO[49] = 0;
objectsAttrLO_start[49] = 0;
objectsAttrHI[49] = 0;
objectsAttrHI_start[49] = 0;

objects[50] = "A large blue Telephone Box";
objectsNoun[50] = 255;
objectsAdjective[50] = 255;
objectsLocation[50] = 30;
objectsLocation_start[50] = 30;
objectsWeight[50] = 1;
objectsWeight_start[50] = 1;
objectsAttrLO[50] = 0;
objectsAttrLO_start[50] = 0;
objectsAttrHI[50] = 0;
objectsAttrHI_start[50] = 0;

objects[51] = "A thickly set metal grill";
objectsNoun[51] = 255;
objectsAdjective[51] = 255;
objectsLocation[51] = 32;
objectsLocation_start[51] = 32;
objectsWeight[51] = 1;
objectsWeight_start[51] = 1;
objectsAttrLO[51] = 0;
objectsAttrLO_start[51] = 0;
objectsAttrHI[51] = 0;
objectsAttrHI_start[51] = 0;

objects[52] = "A displacer field of enormous proportions";
objectsNoun[52] = 255;
objectsAdjective[52] = 255;
objectsLocation[52] = 54;
objectsLocation_start[52] = 54;
objectsWeight[52] = 1;
objectsWeight_start[52] = 1;
objectsAttrLO[52] = 0;
objectsAttrLO_start[52] = 0;
objectsAttrHI[52] = 0;
objectsAttrHI_start[52] = 0;

objects[53] = "A lit cigar, billowing out great clouds of nauseous green smoke..";
objectsNoun[53] = 255;
objectsAdjective[53] = 255;
objectsLocation[53] = 252;
objectsLocation_start[53] = 252;
objectsWeight[53] = 1;
objectsWeight_start[53] = 1;
objectsAttrLO[53] = 0;
objectsAttrLO_start[53] = 0;
objectsAttrHI[53] = 0;
objectsAttrHI_start[53] = 0;

objects[54] = "A multitask trax droid holding a battery.";
objectsNoun[54] = 255;
objectsAdjective[54] = 255;
objectsLocation[54] = 252;
objectsLocation_start[54] = 252;
objectsWeight[54] = 1;
objectsWeight_start[54] = 1;
objectsAttrLO[54] = 0;
objectsAttrLO_start[54] = 0;
objectsAttrHI[54] = 0;
objectsAttrHI_start[54] = 0;

objects[55] = "";
objectsNoun[55] = 255;
objectsAdjective[55] = 255;
objectsLocation[55] = 252;
objectsLocation_start[55] = 252;
objectsWeight[55] = 1;
objectsWeight_start[55] = 1;
objectsAttrLO[55] = 0;
objectsAttrLO_start[55] = 0;
objectsAttrHI[55] = 0;
objectsAttrHI_start[55] = 0;

objects[56] = "A lighter than air docking badge";
objectsNoun[56] = 255;
objectsAdjective[56] = 255;
objectsLocation[56] = 252;
objectsLocation_start[56] = 252;
objectsWeight[56] = 1;
objectsWeight_start[56] = 1;
objectsAttrLO[56] = 0;
objectsAttrLO_start[56] = 0;
objectsAttrHI[56] = 0;
objectsAttrHI_start[56] = 0;

objects[57] = "A damaged crystalfibe store box";
objectsNoun[57] = 255;
objectsAdjective[57] = 255;
objectsLocation[57] = 3;
objectsLocation_start[57] = 3;
objectsWeight[57] = 1;
objectsWeight_start[57] = 1;
objectsAttrLO[57] = 0;
objectsAttrLO_start[57] = 0;
objectsAttrHI[57] = 0;
objectsAttrHI_start[57] = 0;

objects[58] = "Interlock 1 indicator on";
objectsNoun[58] = 255;
objectsAdjective[58] = 255;
objectsLocation[58] = 21;
objectsLocation_start[58] = 21;
objectsWeight[58] = 1;
objectsWeight_start[58] = 1;
objectsAttrLO[58] = 0;
objectsAttrLO_start[58] = 0;
objectsAttrHI[58] = 0;
objectsAttrHI_start[58] = 0;

objects[59] = "Interlock 1 indicator off";
objectsNoun[59] = 255;
objectsAdjective[59] = 255;
objectsLocation[59] = 252;
objectsLocation_start[59] = 252;
objectsWeight[59] = 1;
objectsWeight_start[59] = 1;
objectsAttrLO[59] = 0;
objectsAttrLO_start[59] = 0;
objectsAttrHI[59] = 0;
objectsAttrHI_start[59] = 0;

objects[60] = "";
objectsNoun[60] = 255;
objectsAdjective[60] = 255;
objectsLocation[60] = 252;
objectsLocation_start[60] = 252;
objectsWeight[60] = 1;
objectsWeight_start[60] = 1;
objectsAttrLO[60] = 0;
objectsAttrLO_start[60] = 0;
objectsAttrHI[60] = 0;
objectsAttrHI_start[60] = 0;

objects[61] = "";
objectsNoun[61] = 255;
objectsAdjective[61] = 255;
objectsLocation[61] = 252;
objectsLocation_start[61] = 252;
objectsWeight[61] = 1;
objectsWeight_start[61] = 1;
objectsAttrLO[61] = 0;
objectsAttrLO_start[61] = 0;
objectsAttrHI[61] = 0;
objectsAttrHI_start[61] = 0;

objects[62] = "Interlock 2 indicator on ";
objectsNoun[62] = 255;
objectsAdjective[62] = 255;
objectsLocation[62] = 55;
objectsLocation_start[62] = 55;
objectsWeight[62] = 1;
objectsWeight_start[62] = 1;
objectsAttrLO[62] = 0;
objectsAttrLO_start[62] = 0;
objectsAttrHI[62] = 0;
objectsAttrHI_start[62] = 0;

objects[63] = "Interlock 2 indicator off";
objectsNoun[63] = 255;
objectsAdjective[63] = 255;
objectsLocation[63] = 252;
objectsLocation_start[63] = 252;
objectsWeight[63] = 1;
objectsWeight_start[63] = 1;
objectsAttrLO[63] = 0;
objectsAttrLO_start[63] = 0;
objectsAttrHI[63] = 0;
objectsAttrHI_start[63] = 0;

objects[64] = "";
objectsNoun[64] = 255;
objectsAdjective[64] = 255;
objectsLocation[64] = 252;
objectsLocation_start[64] = 252;
objectsWeight[64] = 1;
objectsWeight_start[64] = 1;
objectsAttrLO[64] = 0;
objectsAttrLO_start[64] = 0;
objectsAttrHI[64] = 0;
objectsAttrHI_start[64] = 0;

objects[65] = "";
objectsNoun[65] = 255;
objectsAdjective[65] = 255;
objectsLocation[65] = 252;
objectsLocation_start[65] = 252;
objectsWeight[65] = 1;
objectsWeight_start[65] = 1;
objectsAttrLO[65] = 0;
objectsAttrLO_start[65] = 0;
objectsAttrHI[65] = 0;
objectsAttrHI_start[65] = 0;

objects[66] = "";
objectsNoun[66] = 255;
objectsAdjective[66] = 255;
objectsLocation[66] = 252;
objectsLocation_start[66] = 252;
objectsWeight[66] = 1;
objectsWeight_start[66] = 1;
objectsAttrLO[66] = 0;
objectsAttrLO_start[66] = 0;
objectsAttrHI[66] = 0;
objectsAttrHI_start[66] = 0;

objects[67] = "";
objectsNoun[67] = 255;
objectsAdjective[67] = 255;
objectsLocation[67] = 252;
objectsLocation_start[67] = 252;
objectsWeight[67] = 1;
objectsWeight_start[67] = 1;
objectsAttrLO[67] = 0;
objectsAttrLO_start[67] = 0;
objectsAttrHI[67] = 0;
objectsAttrHI_start[67] = 0;

objects[68] = "Highdome 1 floating after you, its destructor bolts straffing the room witha deadly accuracy!";
objectsNoun[68] = 255;
objectsAdjective[68] = 255;
objectsLocation[68] = 66;
objectsLocation_start[68] = 66;
objectsWeight[68] = 1;
objectsWeight_start[68] = 1;
objectsAttrLO[68] = 0;
objectsAttrLO_start[68] = 0;
objectsAttrHI[68] = 0;
objectsAttrHI_start[68] = 0;

objects[69] = "An Emploder disc\n\n\n\n\n";
objectsNoun[69] = 255;
objectsAdjective[69] = 255;
objectsLocation[69] = 252;
objectsLocation_start[69] = 252;
objectsWeight[69] = 1;
objectsWeight_start[69] = 1;
objectsAttrLO[69] = 0;
objectsAttrLO_start[69] = 0;
objectsAttrHI[69] = 0;
objectsAttrHI_start[69] = 0;

last_object_number =  69; 
carried_objects = 0;
total_object_messages=70;

