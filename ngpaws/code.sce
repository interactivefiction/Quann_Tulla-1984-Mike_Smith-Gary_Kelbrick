






;#define const aLight       = 0
;#define const aWearable    = 1
;#define const aContainer   = 2
;#define const aNPC         = 3
;#define const aConcealed   = 4
;#define const aEdible      = 5
;#define const aDrinkable   = 6
;#define const aEnterable   = 7
;#define const aFemale      = 8
;#define const aLockable    = 9
;#define const aLocked      = 10
;#define const aMale        = 11
;#define const aNeuter      = 12
;#define const aOpenable    = 13
;#define const aOpen        = 14
;#define const aPluralName  = 15
;#define const aTransparent = 16
;#define const aScenery     = 17
;#define const aSupporter   = 18
;#define const aSwitchable  = 19
;#define const aOn          = 20
;#define const aStatic      = 21


;#define const object_0_infr = 0
;#define const object_1_ciga = 1
;#define const object_2_ligh = 2
;#define const object_3 = 3
;#define const object_4_jet = 4
;#define const object_5_key = 5
;#define const object_6_ladd = 6
;#define const object_7_tab = 7
;#define const object_8_one = 8
;#define const object_9_glue = 9
;#define const object_10_matt = 10
;#define const object_11_manu = 11
;#define const object_12_gas = 12
;#define const object_13_stat = 13
;#define const object_14_batt = 14
;#define const object_15_shoc = 15
;#define const object_16_cuff = 16
;#define const object_17_blow = 17
;#define const object_18_boot = 18
;#define const object_19_spon = 19
;#define const object_20_brac = 20
;#define const object_21_air = 21
;#define const object_22_aqua = 22
;#define const object_23_rem = 23
;#define const object_24_shie = 24
;#define const object_25_sli = 25
;#define const object_26_secu = 26
;#define const object_27_wate = 27
;#define const object_28_bomb = 28
;#define const object_29_gun = 29
;#define const object_30_clip = 30
;#define const object_31_phot = 31
;#define const object_32 = 32
;#define const object_33 = 33
;#define const object_34 = 34
;#define const object_35 = 35
;#define const object_36 = 36
;#define const object_37 = 37
;#define const object_38 = 38
;#define const object_39 = 39
;#define const object_40 = 40
;#define const object_41 = 41
;#define const object_42 = 42
;#define const object_43 = 43
;#define const object_44 = 44
;#define const object_45 = 45
;#define const object_46 = 46
;#define const object_47 = 47
;#define const object_48 = 48
;#define const object_49 = 49
;#define const object_50 = 50
;#define const object_51 = 51
;#define const object_52 = 52
;#define const object_53_ciga = 53
;#define const object_54 = 54
;#define const object_55 = 55
;#define const object_56_badg = 56
;#define const object_57 = 57
;#define const object_58 = 58
;#define const object_59 = 59
;#define const object_60 = 60
;#define const object_61 = 61
;#define const object_62 = 62
;#define const object_63 = 63
;#define const object_64 = 64
;#define const object_65 = 65
;#define const object_66 = 66
;#define const object_67 = 67
;#define const object_68 = 68
;#define const object_69_disc = 69


;#define const room_0 = 0
;#define const room_1 = 1
;#define const room_2 = 2
;#define const room_3 = 3
;#define const room_4 = 4
;#define const room_5 = 5
;#define const room_6 = 6
;#define const room_7 = 7
;#define const room_8 = 8
;#define const room_9 = 9
;#define const room_10 = 10
;#define const room_11 = 11
;#define const room_12 = 12
;#define const room_13 = 13
;#define const room_14 = 14
;#define const room_15 = 15
;#define const room_16 = 16
;#define const room_17 = 17
;#define const room_18 = 18
;#define const room_19 = 19
;#define const room_20 = 20
;#define const room_21 = 21
;#define const room_22 = 22
;#define const room_23 = 23
;#define const room_24 = 24
;#define const room_25 = 25
;#define const room_26 = 26
;#define const room_27 = 27
;#define const room_28 = 28
;#define const room_29 = 29
;#define const room_30 = 30
;#define const room_31 = 31
;#define const room_32 = 32
;#define const room_33 = 33
;#define const room_34 = 34
;#define const room_35 = 35
;#define const room_36 = 36
;#define const room_37 = 37
;#define const room_38 = 38
;#define const room_39 = 39
;#define const room_40 = 40
;#define const room_41 = 41
;#define const room_42 = 42
;#define const room_43 = 43
;#define const room_44 = 44
;#define const room_45 = 45
;#define const room_46 = 46
;#define const room_47 = 47
;#define const room_48 = 48
;#define const room_49 = 49
;#define const room_50 = 50
;#define const room_51 = 51
;#define const room_52 = 52
;#define const room_53 = 53
;#define const room_54 = 54
;#define const room_55 = 55
;#define const room_56 = 56
;#define const room_57 = 57
;#define const room_58 = 58
;#define const room_59 = 59
;#define const room_60 = 60
;#define const room_61 = 61
;#define const room_62 = 62
;#define const room_63 = 63
;#define const room_64 = 64
;#define const room_65 = 65
;#define const room_66 = 66
;#define const room_67 = 67
;#define const room_68 = 68
;#define const room_69 = 69
;#define const room_70 = 70
;#define const room_71 = 71
;#define const room_72 = 72
;#define const room_73 = 73
;#define const room_74 = 74
;#define const room_75 = 75
;#define const room_76 = 76
;#define const room_77 = 77
;#define const room_78 = 78
;#define const room_79 = 79
;#define const room_80 = 80
;#define const room_81 = 81
;#define const room_82 = 82
;#define const room_83 = 83
;#define const room_84 = 84
;#define const room_85 = 85
;#define const room_86 = 86
;#define const room_87 = 87
;#define const room_88 = 88
;#define const room_89 = 89
;#define const room_90 = 90
;#define const room_91 = 91
;#define const room_92 = 92
;#define const room_93 = 93
;#define const room_94 = 94
;#define const room_95 = 95
;#define const room_96 = 96
;#define const room_97 = 97
;#define const room_98 = 98
;#define const room_99 = 99
;#define const room_100 = 100
;#define const room_101 = 101
;#define const room_102 = 102
;#define const room_103 = 103
;#define const room_104 = 104
;#define const room_105 = 105
;#define const room_106 = 106
;#define const room_107 = 107
;#define const room_108 = 108
;#define const room_109 = 109
;#define const room_110 = 110
;#define const room_111 = 111
;#define const room_112 = 112
;#define const room_113 = 113
;#define const room_114 = 114
;#define const room_115 = 115
;#define const room_116 = 116
;#define const room_117 = 117
;#define const room_118 = 118
;#define const room_119 = 119
;#define const room_120 = 120
;#define const room_121 = 121
;#define const room_122 = 122
;#define const room_123 = 123
;#define const room_124 = 124
;#define const room_125 = 125
;#define const room_126 = 126

;#define const room_noncreated = 252 ; destroying an object moves it to this room
;#define const room_worn = 253 ; wearing an object moves it to this room
;#define const room_carried = 254 ; carrying an object moves it to this room
;#define const room_here = 255 ; not used by any Quill condacts










;#define const yesno_is_dark                       =   0
;#define const total_carried                       =   1
;#define const countdown_location_description      =   2
;#define const countdown_location_description_dark =   3
;#define const countdown_object_description_unlit  =   4
;#define const countdown_player_input_1            =   5
;#define const countdown_player_input_2            =   6
;#define const countdown_player_input_3            =   7
;#define const countdown_player_input_4            =   8
;#define const countdown_player_input_dark         =   9
;#define const countdown_player_input_unlit        =  10
;#define const game_flag_11                        = 111 ; ngPAWS assigns a special meaning to flag 11
;#define const game_flag_12                        = 112 ; ngPAWS assigns a special meaning to flag 12
;#define const game_flag_13                        = 113 ; ngPAWS assigns a special meaning to flag 13
;#define const game_flag_14                        = 114 ; ngPAWS assigns a special meaning to flag 14
;#define const game_flag_15                        = 115 ; ngPAWS assigns a special meaning to flag 15
;#define const game_flag_16                        = 116 ; ngPAWS assigns a special meaning to flag 16
;#define const game_flag_17                        =  17
;#define const game_flag_18                        =  18
;#define const game_flag_19                        =  19
;#define const game_flag_20                        =  20
;#define const game_flag_21                        =  21
;#define const game_flag_22                        =  22
;#define const game_flag_23                        =  23
;#define const game_flag_24                        =  24
;#define const game_flag_25                        =  25
;#define const game_flag_26                        =  26
;#define const game_flag_27                        =  27
;#define const pause_parameter                     =  28
;#define const bitset_graphics_status              =  29
;#define const total_player_score                  =  30
;#define const total_turns_lower                   =  31
;#define const total_turns_higher                  =  32
;#define const word_verb                           =  33
;#define const word_noun                           =  34
;#define const room_number                         =  38 ; (35 in The Quill, but 38 in ngPAWS)




;#define const pause_sound_effect_tone_increasing = 1 ; increasing tone (value is the duration)
;#define const pause_sound_effect_telephone       = 2 ; ringing telephone (value is the number of rings)
;#define const pause_sound_effect_tone_decreasing = 3 ; decreasing tone (opposite of effect 1)
;#define const pause_sound_effect_white_noise     = 5 ; white noise (resembles an audience clapping, value is the number of repetitions)
;#define const pause_sound_effect_machine_gun     = 6 ; machine gun noise (value is the duration)

;#define const pause_flash = 4 ; flash the screen and the border (value is the number of flashes)
;#define const pause_box_wipe = 9 ; a series of coloured boxes expands out of the centre of the screen at speed

;#define const pause_font_default = 7 ; switch to the default font
;#define const pause_font_alternate = 8 ; switch to the alternative font.

;#define const pause_change_youcansee = 10 ; Change the "You can also see" message to the message number passed to PAUSE

;#define const pause_restart = 12 ; Restart the game without warning.
;#define const pause_reboot = 13 ; Reboot the Spectrum without warning
;#define const pause_ability = 11 ; Set the maximum number of objects carried at once to the value passed to PAUSE
;#define const pause_ability_plus = 14 ; Increase number of objects that can be carried at once by the amount passed to PAUSE
;#define const pause_ability_minus = 15 ; Decrease number of objects that can be carried at once by the amount passed to PAUSE
;#define const pause_toggle_graphics = 19 ; switch graphics off (for PAUSE 255) or on (for any other value)
;#define const pause_ram_save_load = 21 ; RAMload (for PAUSE 50) or RAMsave (for any other value)
;#define const pause_set_identity_byte = 22 ; Set the identity in saved games to the value passed to PAUSE

;#define const pause_click_effect_1 = 16
;#define const pause_click_effect_2 = 17
;#define const pause_click_effect_3 = 18









/CTL






/VOC






n         1    verb
nort      1    verb
north     1    verb
s         2    verb
sout      2    verb
south     2    verb
e         3    verb
east      3    verb
w         4    verb
west      4    verb
se        5    verb
sw        6    verb
ne        7    verb
nw        8    verb

asce      9    verb

clim      9    verb

u         9    verb

up        9    verb
o        10    verb
out      10    verb
i        11    noun


inve     11    verb
in       12    verb
tele     13    noun
d        14    verb
down     14    verb






card     14    noun
infr     14    noun
infradat 14    noun
ciga     15    noun
cigar    15    noun
light    16    noun
ligh     16    verb
stick    16    noun
stic     16    verb
ball     17    noun
lead     17    noun
boos     18    noun
boosterpak 18    noun
jet      18    noun
comb     19    noun
key      19    noun
ladd     20    noun
ladders  20    noun
tita     20    noun
titaninium 20    noun
sulp     21    noun
sulphurtab 21    noun
tab      21    noun
tabl     21    noun
cred     22    noun
one      22    noun
glue     23    noun
tube     23    noun
disp     24    noun
displacer 24    noun
matt     24    noun
matter   24    noun
manu     25    noun
manual     25    noun
door     26    noun
disr     27    noun
disrupter 27    noun
stat     27    noun
static   27    noun
batt     28    noun
battery  28    noun
cape     29    noun
cloa     29    noun
cloak    29    noun
shoc     29    noun
shockcape 29    noun
cuff     30    noun
cufflinks 30    noun
whiz     30    noun
whizz    30    noun

blow     31    verb
pipe     31    noun

boot     32    noun
boots    32    noun
magn     32    noun
magnet   32    noun
spon     33    noun
sponge   33    noun
brac     34    noun
bracelet 34    noun
air      35    noun
mask     35    noun
natu     35    noun
natuflow 35    noun
aqua     36    noun
note     37    noun
rem      37    noun
remi     37    noun
remindanote 37    noun
elec     38    noun
electroshield 38    noun
shie     38    noun
shield   38    noun
sli      39    noun
slip     39    noun
thin     39    noun
thinkslip 39    noun
pass     40    noun
secu     40    noun
securipass 40    noun
flas     41    noun
flask    41    noun
wate     41    noun
water    41    noun
bomb     42    noun
limp     42    noun
blas     43    noun
handblaster 43    noun
blaster  43    noun
gun      43    noun
han      43    noun
hand     43    noun
clip     44    noun
coin     44    noun
pack     45    noun
phot     45    noun
photon   45    noun
post     46    noun
sign     46    noun
drin     47    verb
drink    47    verb






data     50    noun
quan     50    noun
term     50    noun
comp     51    noun
spea     52    noun
main     53    noun
prob     53    noun
hygi     54    noun
robo     54    noun
droi     55    noun
mult     55    noun
high     56    noun
latt     57    noun
net      57    noun
auto     58    noun
cran     58    noun
shar     59    noun
trac     59    noun
code     60    noun
libr     60    noun
guar     61    noun
troo     61    noun
fiel     62    noun
forc     62    noun
box      63    noun
crys     63    noun
phon     63    noun
stor     63    noun
gril     64    noun
gas      65    noun
gasl     65    noun
lite     65    noun
pres     66    verb
butt     67    noun
igni     68    noun
red      68    noun
blue     69    noun
gree     70    noun
badg     71    noun
dock     71    noun
open     72    verb
unlo     72    verb
hurl     73    verb
laun     73    verb
roll     73    verb
slin     73    verb
sque     74    verb
smok     75    verb
give     76    verb
fire     77    verb
push     78    verb
whit     79    noun
pull     80    verb
leve     81    noun
yell     82    noun
disc     83    noun
empl     83    noun
inse     87    verb
tent     88    noun
pane     89    noun
chai     90    noun
seat     90    noun
oil      91    noun
pool     91    noun
cant     92    noun
rack     92    noun
cupb     93    noun
dust     94    noun
draw     95    noun
cogs     96    noun
engi     96    noun
mach     96    noun
shut     96    noun
lock     97    noun
sear     98    verb
exam     99    verb
get     100    verb
t       100    verb
take    100    verb
drop    101    verb
thro    101    verb
remo    102    verb
wear    103    verb

look    105    verb
r       105    verb
rede    105    verb
quit    106    verb
stop    106    verb
save    107    verb
load    108    verb
re      108    verb
rest    108    verb
ente    109    verb
say     109    verb
shou    109    verb
xxxx    110    noun
atta    115    verb
kill    115    verb
slay    115    verb
help    116    verb
scor    117    verb
cunt    200    verb
fuck    200    verb
piss    200    verb
turn    202    verb
read    205    verb
wash    206    verb
blowpipe 207   noun



AND          2    conjunction
THEN         2    conjunction






/STX
/0
Everything is dark.<br>
/1
You can also see:--<br>
/2
I await your command.<br>
/3
I'm ready for your instructions.<br>
/4
What are your instructions?<br>
/5
Give me your command.<br>
/6
Sorry I don't understand that. Try some different words.<br>
/7
You can't go in that direction.<br>
/8
You can't do that.<br>
/9
You are carrying:--
/10
                          (worn)
/11
Nothing at all.<br>
/12
Do you really want to quit now?<br>
/13
{CLASS|center|END OF GAME}Do you want  to try again?<br>
/14
Bye. Have a nice day.<br>
/15
OK.<br>
/16
{CLASS|center|Press any key to continue}<br>
/17
You have taken 
/18
 turn
/19
s
/20
.<br>
/21
You have completed 
/22
% of the game.<br>
/23
You're not wearing it.<br>
/24
You can't carry any more — your hands are full.<br>
/25
You don't have it.<br>
/26
It's not here.<br>
/27
You can't carry any more.<br>
/28
You don't have it.<br>
/29
You're already wearing it.<br>
/30
Y<br>
/31
N<br>
/32
More…<br>
/33
><br>
/34

<br>
/35

Time passes…<br>
/36


/37


/38


/39


/40

You can't do that.<br>
/41

You can't do that.<br>
/42

You can't carry any more.<br>
/43

  <br>
/44

You put {OREF} into<br>
/45

{OREF} is not into<br>
/46

<br>
/47

<br>
/48

<br>
/49

> <br>
/50

You can't do that.<br>
/51

.<br>
/52

That is not into<br>
/53

nothing at all<br>
/54

File not found.<br>
/55

File corrupt.<br>
/56

I/O error. File not saved.<br>
/57

Directory full.<br>
/58

Please enter savegame name you used when saving the game status.
/59

Invalid savegame name. Please check the name you entered is correct, and make sure you are trying to load the game from the same browser you saved it.<br>
/60

Please enter savegame name. Remember to note down the name you choose, as it will be requested in order to restore the game status.
/61

<br>
/62

Sorry? Please try other words.<br>
/63

Here<br>
/64

you can see<br>
/65

you can see<br>
/66

inside you see<br>
/67

on top you see<br>






/MTX


/1000

Exits: 
/1001

You can't see any exits

/1003
{ACTION|nort|nort}
/1004
{ACTION|sout|sout}
/1005
{ACTION|east|east}
/1006
{ACTION|west|west}
/1007
{ACTION|ne|ne}
/1008
{ACTION|nw|nw}
/1009
{ACTION|se|se}
/1010
{ACTION|sw|sw}
/1011
{ACTION|up|up}
/1012
{ACTION|jump|jump}
/1013
{ACTION|in|in}
/1014
{ACTION|out|out}






/OTX
/0 ; sust(object_0_infr//0)
an infradat data card
/1 ; sust(object_1_ciga//1)
a fat cigar
/2 ; sust(object_2_ligh//2)
a neon lightstick
/3 ; sust(object_3//3)
a curiously shaped ball of lead
/4 ; sust(object_4_jet//4)
a twin-jet boosterpak
/5 ; sust(object_5_key//5)
a combulock key
/6 ; sust(object_6_ladd//6)
a set of titaninium ladders
/7 ; sust(object_7_tab//7)
a sulphurtab
/8 ; sust(object_8_one//8)
a one-cred note
/9 ; sust(object_9_glue//9)
a tube of permaglue
/10 ; sust(object_10_matt//10)
a humming matter displacer
/11 ; sust(object_11_manu//11)
the Quann Tulla operation manual
/12 ; sust(object_12_gas//12)
a Gas lyter+
/13 ; sust(object_13_stat//13)
a static disrupter
/14 ; sust(object_14_batt//14)
a &#34;longalife&#34; battery
/15 ; sust(object_15_shoc//15)
a shockcape
/16 ; sust(object_16_cuff//16)
a set of whizz-curcuit cufflinks
/17 ; sust(object_17_blow//17)
a blowpipe
/18 ; sust(object_18_boot//18)
a pair of magnet boots
/19 ; sust(object_19_spon//19)
a glowing sponge
/20 ; sust(object_20_brac//20)
a teleport bracelet
/21 ; sust(object_21_air//21)
a natuflow airmask
/22 ; sust(object_22_aqua//22)
an aqualung
/23 ; sust(object_23_rem//23)
a shrivelled remindanote
/24 ; sust(object_24_shie//24)
an electroshield
/25 ; sust(object_25_sli//25)
a thinkslip
/26 ; sust(object_26_secu//26)
an ornate securipass
/27 ; sust(object_27_wate//27)
a flask of cool water
/28 ; sust(object_28_bomb//28)
a LIMPETBOMB
/29 ; sust(object_29_gun//29)
a handblaster
/30 ; sust(object_30_clip//30)
A small coin
/31 ; sust(object_31_phot//31)
an interat photon pack
/32 ; sust(object_32//32)
QUANN TULLA TERMINAL 1
/33 ; sust(object_33//33)
QUANN TERMINAL 2
/34 ; sust(object_34//34)
QUANN TERMINAL 3
/35 ; sust(object_35//35)
COMPUTER TERMINAL DATALINK &#35;A
/36 ; sust(object_36//36)
SPEAKTALK COMMUNI-1, marked; &#34;Insert cash to open interlock 1&#34;
/37 ; sust(object_37//37)
a humming maintenance probe floating in the air, eyeing you!
/38 ; sust(object_38//38)
a perfectly scrubbed personal hygiene robot
/39 ; sust(object_39//39)
a multitask traxdroid
/40 ; sust(object_40//40)
The hovering form of the most advanced fighting machine known to man, and at within its'glowing operator dome, the mocking figure of Erra Quann!Realisation comes at once-this is HIGHDOME 1!
/41 ; sust(object_41//41)
A powerlite lattice!
/42 ; sust(object_42//42)
A rotating autograb load crane!
/43 ; sust(object_43//43)
A Sharpshot hunter tracer
/44 ; sust(object_44//44)
The Quann Tulla library code computer.
/45 ; sust(object_45//45)

/46 ; sust(object_46//46)

/47 ; sust(object_47//47)

/48 ; sust(object_48//48)
A mass of empire troops milling about the launch pad, weapons at the ready
/49 ; sust(object_49//49)
A crackling force-field!
/50 ; sust(object_50//50)
A large blue Telephone Box
/51 ; sust(object_51//51)
A thickly set metal grill
/52 ; sust(object_52//52)
A displacer field of enormous proportions
/53 ; sust(object_53_ciga//53)
A lit cigar, billowing out great clouds of nauseous green smoke..
/54 ; sust(object_54//54)
A multitask trax droid holding a battery.
/55 ; sust(object_55//55)

/56 ; sust(object_56_badg//56)
A lighter than air docking badge
/57 ; sust(object_57//57)
A damaged crystalfibe store box
/58 ; sust(object_58//58)
Interlock 1 indicator on
/59 ; sust(object_59//59)
Interlock 1 indicator off
/60 ; sust(object_60//60)

/61 ; sust(object_61//61)

/62 ; sust(object_62//62)
Interlock 2 indicator on 
/63 ; sust(object_63//63)
Interlock 2 indicator off
/64 ; sust(object_64//64)

/65 ; sust(object_65//65)

/66 ; sust(object_66//66)

/67 ; sust(object_67//67)

/68 ; sust(object_68//68)
Highdome 1 floating after you, its destructor bolts straffing the room witha deadly accuracy!
/69 ; sust(object_69_disc//69)
An Emploder disc





/LTX
/0 ; sust(room_0//0)
You are lieing in an leaking air filled suspend-bubble in the uni-am chamber of your ship. Registers on the wall indicate your waking--but the other instruments are all damaged. The only exit is {ACTION|out|out}.
/1 ; sust(room_1//1)
You are standing {ACTION|in|in} the Suspend Am chamber-sparks fly from the machines all around you, and the air is full of thick, choking black smoke.
/2 ; sust(room_2//2)
You are in the cargo bay-several crates and boxes are shattered all about you, and wreckage is strewn about the floor. There is a storeroom {ACTION|south|south}, and the cockpit is {ACTION|east|east}. Gazing through the docking tube door to the {ACTION|north|north}, you can see it is swaying dangerously into space.
/3 ; sust(room_3//3)
You are in a blackened store-room.
/4 ; sust(room_4//4)
You are in the cockpit of the CRIMSON CLOUD II space hopper. Considerable damage appears to have occured to your ship during the flight-On the console before you only 3 buttons are lit:Red, Blue and Green. The massive hull of the QUANN TULLA fills your sight!
/5 ; sust(room_5//5)
You are before the docking tube in a smoke-filled airlock. The tube before you is pocked with hundreds of small holes, making it look very pecarious!
/6 ; sust(room_6//6)
You are struggling to maintain your balance in the tube. Harsh neon lights show up ahead.
/7 ; sust(room_7//7)
Auto-speakers announce&#34;ENTERWAY: QUANN TULLA&#34;. You are in the brightly-lit cargo hanger of the Quann Tulla. Once a credit to the crew it is now in total squalor.
/8 ; sust(room_8//8)
You are in the auto-decontam shower. Sterilised water pours down from above.
/9 ; sust(room_9//9)
You are in the wiring circuits of the external computer centre- thick, disarrayed wires lead {ACTION|east|east}
/10 ; sust(room_10//10)
You are outside a battered door on which is written,&#34;DANGER-HIGH VOLTAGE!&#34;
/11 ; sust(room_11//11)

/12 ; sust(room_12//12)
You are on a slideway catwalk further along the platform. Platforms wind {ACTION|NE|NE},{ACTION|W|W}.
/13 ; sust(room_13//13)
You are where the walkway ends before a severely damaged bulkhead.
/14 ; sust(room_14//14)

/15 ; sust(room_15//15)

/16 ; sust(room_16//16)
You are at the computer defence command centre (external)-huge turrets, now silent, point aimlessly into space. You wonder how cunning the enemy is that attacks the Federations greatest flag ship from within, avoiding the advanced external systems..
/17 ; sust(room_17//17)
You are in a rusty satellite. Wires hang from every wall
/18 ; sust(room_18//18)
You are at a savage tear {ACTION|in|in} the hull&#59;vast piles of metal ready for disposal into space are piled everywhere.
/19 ; sust(room_19//19)
You are in the central reference library-microfilm racks lead off in all directions.
/20 ; sust(room_20//20)
You are in an empty Micro-rack store that runs {ACTION|E|E}/{ACTION|W|W}
/21 ; sust(room_21//21)
You are in a large room-every wall is covered in smashed projector screens. Interlock 1\nis east.
/22 ; sust(room_22//22)
You are in a burnt-out computer terminal room. An open air vent is just above your head.
/23 ; sust(room_23//23)
You are before a totally jammed Securi-door.
/24 ; sust(room_24//24)
You are in the Quann Terminal 1 housing. Once a busy command centre, it is now a burnt out shell.          
/25 ; sust(room_25//25)
You are impeded by a set of large ruptured doors.
/26 ; sust(room_26//26)
You are crawling in a dusty air vent.
/27 ; sust(room_27//27)
You are in a sealed corridor.
/28 ; sust(room_28//28)
You are in the personal hygiene check-in. Sparkling clean, it looks as if it has just been dusted!
/29 ; sust(room_29//29)
You are before the 1st interlock one through doors that connect the sections of the ship.
/30 ; sust(room_30//30)
You are {ACTION|in|in} the Telecom station. Once the heart of the crews leisure time it is now empty.
/31 ; sust(room_31//31)
You are in an icy corridor at a junction.
/32 ; sust(room_32//32)
You are {ACTION|east|east} of a junction on a greasy walkway
/33 ; sust(room_33//33)
You are standing on a small ledge in a chemi-bath degreaser pit.
/34 ; sust(room_34//34)
You are in the 1st battery of the ion displacer turrets. Vast banks of machinery seem to run for miles in every direction, but piles of wreckage block the aisles as a result of the attack on the ship.
/35 ; sust(room_35//35)
You are in the 2nd battery in\na confusion of thick cables. A sign reads&#34;TELEPORT {ACTION|N|N}.&#34;
/36 ; sust(room_36//36)
You are in a dusty teleport station-simulated granite pillars cover every wall in &#34;Roman&#34;style:similarily, most\nare in ruins.
/37 ; sust(room_37//37)
You are in battery 3 amidst a pile of immovable debris.
/38 ; sust(room_38//38)

/39 ; sust(room_39//39)
You are at the end of the Ion imploder tube-the metal plating rises high above your head, and looks undamaged.
/40 ; sust(room_40//40)
You are on a metal rimmed lip over the ion mount casing floating far below in space.
/41 ; sust(room_41//41)

/42 ; sust(room_42//42)
You are on a circular floatway above the central housing case of the drive tube. Below you vast sparks leap to the rear of the ship out of sight, noiselessly. Huge arcs of light force you to look away.
/43 ; sust(room_43//43)
You are in a long corridor that runs {ACTION|N|N}/{ACTION|S|S} to a bright neon light
/44 ; sust(room_44//44)
You are in a small locker room. lockers cover every wall.
/45 ; sust(room_45//45)
You are at a junction. A large signpost reads,&#34;Terminal 2 {ACTION|West|West}&#34;
/46 ; sust(room_46//46)
You are inside a dusty old telephone box by a phone- amazingly enough, it seems much smaller on the inside than on the outside!
/47 ; sust(room_47//47)
You are in the cental housing of Quann terminal 2
/48 ; sust(room_48//48)
You are in a deserted shuttle port. Once a busy terminal, the empty cargo hangers and launch pads echo your footsteps eerily against the cracked plas-skin tinted dome high above. A Large shuttle craft lies slewed across the pad.
/49 ; sust(room_49//49)
You are in the wreckage of the shuttle before the charred remnants of the pilots seat
/50 ; sust(room_50//50)
You are in an engine inspection panel-unfortunatly, the engine has been destroyed.
/51 ; sust(room_51//51)
You are studying the digital system electrical analyser, consisting of an arrangement of massive cogs-primitive as it may seem, the cogs seem to have been of primary engineering concern!
/52 ; sust(room_52//52)
You are in the port side 1st missile bunker.
/53 ; sust(room_53//53)
You are in the second bunker- judged by the condtion of the ship so far, it has suffered a major attack-but the missiles have not been armed or fired!
/54 ; sust(room_54//54)
A swaying sign reads,&#34;MISSILE BUNKER 3&#34;Stars are visible via\na huge tear in the hull
/55 ; sust(room_55//55)
You are rotating W-E on the East side inner wheel of the drive unit-ionglass tubes filter great magnetic explosions to the rear of the 5 Exom tube, storing the energy created in the engines.
/56 ; sust(room_56//56)
You are in interlock 2. A sign reads,&#34;Booster-paks compulsory {ACTION|east|east}.&#34;
/57 ; sust(room_57//57)
You are on the second rim of\nthe inner rim wheel above the ion tube. Imploder valves swing dangerously above, collecting stored energy!
/58 ; sust(room_58//58)
You are on a stop-off jumpath above the rotating wheel. Strange lights revolve in merged colours far below
/59 ; sust(room_59//59)
You are in an airlock between rotator wheels. Wires hang in disarray far above
/60 ; sust(room_60//60)
You are at the junction of {ACTION|east|east} and {ACTION|west|west} rims at the base of the wheel:long floatways once led N and SW, but they are now burnt and twisted.
/61 ; sust(room_61//61)
You are in an ante room before the doors of Interlock 3. A sign reads,&#34;Passing through here with out switching off Term.3 will result in automatic 3rd stage seperation.&#34;
/62 ; sust(room_62//62)
You are below flashing lights in interlock 3-deep cracks in the floor indicate locked 3rd/4th stage docking units
/63 ; sust(room_63//63)

/64 ; sust(room_64//64)
You are in a wrecked crew canteen. metal canteen racks lie discarded on the floor.
/65 ; sust(room_65//65)
You are in a small anteroom that has recently been ransacked!
/66 ; sust(room_66//66)
You are in a charred office-burnt plaswood lies everywhere and a strong smell of smoke hangs in the air.
/67 ; sust(room_67//67)
You are {ACTION|north|north} of the wheel and south of a large exi-tube to the outer hull
/68 ; sust(room_68//68)
You are in an exi-tube leading to a walkway between the tube and a radio station open to space
/69 ; sust(room_69//69)
You are in a radio transmission room. All machinery has been ripped out and thick piles of dust coat the floor
/70 ; sust(room_70//70)
You are at the base of a huge radio mast-lights flash brightly at the top far above, contrasting against the darkness all around!
/71 ; sust(room_71//71)
You are on a metal walkway on the outer hull, open to space. The hull rotates below your feet, open to space
/72 ; sust(room_72//72)
You are in a doorway {ACTION|north|north} of the walkway entering a pressure corridor bulging from the hull
/73 ; sust(room_73//73)
From here a walkway points {ACTION|north|north} from where a pattern of strong lights shine.
/74 ; sust(room_74//74)
You are in a brightly-lit alcove
/75 ; sust(room_75//75)
This is the housing for Quann terminal 3
/76 ; sust(room_76//76)
You are at the base of the 1st main computer databank silo. Far above the massive discdrives whirr in activity. The vast data- run rises in 3 blocks, each being about 0.5 Exoms high!
/77 ; sust(room_77//77)
You are following a path between towering silo's lettered with the legend,&#34;MICRODRIV. IV&#34;
/78 ; sust(room_78//78)
You are N. of the silo's standing by a strange pool of leaking oil that covers the ground.
/79 ; sust(room_79//79)

/80 ; sust(room_80//80)
You are at a door blocking your view {ACTION|north|north}.
/81 ; sust(room_81//81)
You are at a sign post that reads,&#34;BRIDGE-{ACTION|SOUTH|SOUTH}.&#34;
/82 ; sust(room_82//82)
You are in a inversegrav lift that appears to move-but the sensation is so quick it is really difficult to tell.
/83 ; sust(room_83//83)
You are at the main control panel on the bridge. Through the massive domed glass you look out onto the ship stretched out before you into the distance. Vast fields of machinery run to the edge of your vision, and seemingly just visible on the horizon of metal you can see your own ship dwarved by the hull of the Quann Tulla. A large lever is before you.
/84 ; sust(room_84//84)
You are beneath a massive tinted dome overlooking the ship. The bridge is {ACTION|E|E}/{ACTION|W|W}.
/85 ; sust(room_85//85)
You are at the edge of the ships guidance pre-sets. Large open draws poke out as if they had been searched in vain, whilst 3 buttons wink on the panel:-White yellow and Blue.
/86 ; sust(room_86//86)
You are in an empty room-great clouds of dust rise from your feet as you enter..
/87 ; sust(room_87//87)
You are in an inert energy field
/88 ; sust(room_88//88)
You are at a junction with a massive, damaged shield-door.
/89 ; sust(room_89//89)
You are in a miniature room\nthat houses the main computer- the roof is so small you have\nto stoop. Lite-vids show exit {ACTION|SE|SE}.
/90 ; sust(room_90//90)
You are seated at the controls of a spacial fighter through the screen of which enemy fighters are visible approaching from a large planet!A large red button marked &#34;IGNITION&#34; is beside you!
/91 ; sust(room_91//91)
You are at the connection of the Ion tube to the drive motors- the roar of the engines shakes the floor beneath your feet.
/92 ; sust(room_92//92)
You are directly over the tube connector on a steel alloy bridge. West the full length of the ion tube becomes apparent as it runs the length of the ship- Eastwards the huge cones of the now inoperable boosterjets point into space far above your head.
/93 ; sust(room_93//93)
You are before the door to the Quann Tulla engine room!A large sign over the door reads,\nALL VISITORS BEYOND THIS POINT PLEASE SHOW IDENTIFICATION.
/94 ; sust(room_94//94)
You are at the edge of the 1st drive engine on a long metal pathway. The huge bays disappear far to the {ACTION|south|south} in a link of engines. The air is charged with static and the noise is deafening! 
/95 ; sust(room_95//95)
You stand before the second link of the huge Ion drive engines. Vibrations move horizontal angle from lesser to upper degrees
/96 ; sust(room_96//96)
YOu are at a small deck of motor guidance sensors-heavily shielded, they are nevertheless the weakest part of the engine drive units.
/97 ; sust(room_97//97)
You are at the heart of the engine guidance system. A large red door marked &#34;STRICTLY NO ADMITTANCE&#34;is east whilst the walkway curves {ACTION|West|West}.
/98 ; sust(room_98//98)
You are at a Greco-styled room emblazoned&#34;TELEPORT STATION 2&#34; by a glowing teleport computer
/99 ; sust(room_99//99)
You are on top of a high sand dune above a massive desert. The sun is high in the sky, the heat unbearable. You are on the edge of the desert whilst a tropical rain forest lies in a deep valley before you.
/100 ; sust(room_100//100)
You are on the edge of a desert above a thick tropical jungle. Through the heat haze a clearing is just visible by hacked path.
/101 ; sust(room_101//101)
You are on the edge of the clearing {ACTION|south|south} of a dust cloud
/102 ; sust(room_102//102)
You are on a cleared path through the jungle. A strange hut is {ACTION|west|west}.
/103 ; sust(room_103//103)
Much to your surprise the small hut is just a disguise for an Empire-style power generator.. odd piles of concealing cables cover the floor.
/104 ; sust(room_104//104)
You are further along the path that veers from {ACTION|N|N} to {ACTION|E|E}.
/105 ; sust(room_105//105)
You are at the end of the path over a large clearing. Smoke rises from the {ACTION|east|east}.
/106 ; sust(room_106//106)
You are on the edge of a tropical lake surrounded by trees. A steep bank falls into the water, from which comes a strange sheen
/107 ; sust(room_107//107)
You are in clouded water in a deep lake, floating down to the sea bed amongst rank weeds
/108 ; sust(room_108//108)
You are on the sea bed by a hidden underwater launch pad\nof Empire design.
/109 ; sust(room_109//109)
You are {ACTION|in|in} an airlock of a small aqua sphere. Water drips from above
/110 ; sust(room_110//110)
You are inside the aqua sphere. Harsh tube lights hang from a dank, claustrophobic corridor.
/111 ; sust(room_111//111)
You are in the cockpit of a well armoured Atmos-Pod. All controls seem operational-but the power supply needs insertion.
/112 ; sust(room_112//112)
You are floating in murky weed- clogged water above a small rock ledge.
/113 ; sust(room_113//113)
You are in a clearing covered with numerous Empire-style fibre tents.
/114 ; sust(room_114//114)
You are in a deserted Empire outpost:ashes lie recently scattered.
/115 ; sust(room_115//115)

/116 ; sust(room_116//116)
You are lost in a thickly- wooded rain forest.
/117 ; sust(room_117//117)
You are at the top of a high tree above the forest floor. A small rope walkway leads {ACTION|East|East}.
/118 ; sust(room_118//118)
You are clinging to a rocking rope walkway leading to a high cliff.
/119 ; sust(room_119//119)
You are on the edge of a tall granite cliff. Boulders litter the rock
/120 ; sust(room_120//120)
You are west of a large rocket launch pad. All is oddly silent
/121 ; sust(room_121//121)
You are on a dummy launch pad -similar from a distance to a real pad, but made of synthfibes.
/122 ; sust(room_122//122)
You are in an empty Aqua-store, standing before a large cupboard
/123 ; sust(room_123//123)
You are in a dummy ship made up to resemble a real ship-this must surely be a trap!A large ripped out panel swings open as you approach..
/124 ; sust(room_124//124)

/125 ; sust(room_125//125)

/126 ; sust(room_126//126)







/CON
/0 ; sust(room_0//0)
out 1 ; sust(room_1/1)

/1 ; sust(room_1//1)
e 2 ; sust(room_2/2)
in 0 ; sust(room_0/0)

/2 ; sust(room_2//2)
e 4 ; sust(room_4/4)
n 5 ; sust(room_5/5)
s 3 ; sust(room_3/3)
w 1 ; sust(room_1/1)

/3 ; sust(room_3//3)
n 2 ; sust(room_2/2)

/4 ; sust(room_4//4)
w 2 ; sust(room_2/2)

/5 ; sust(room_5//5)
n 6 ; sust(room_6/6)
s 2 ; sust(room_2/2)

/6 ; sust(room_6//6)
n 7 ; sust(room_7/7)
s 5 ; sust(room_5/5)

/7 ; sust(room_7//7)
n 8 ; sust(room_8/8)
s 6 ; sust(room_6/6)

/8 ; sust(room_8//8)
e 20 ; sust(room_20/20)
s 7 ; sust(room_7/7)
se 10 ; sust(room_10/10)

/9 ; sust(room_9//9)
e 12 ; sust(room_12/12)
n 10 ; sust(room_10/10)

/10 ; sust(room_10//10)
nw 8 ; sust(room_8/8)
s 9 ; sust(room_9/9)

/11 ; sust(room_11//11)

/12 ; sust(room_12//12)
ne 13 ; sust(room_13/13)
w 9 ; sust(room_9/9)

/13 ; sust(room_13//13)
s 16 ; sust(room_16/16)
sw 12 ; sust(room_12/12)

/14 ; sust(room_14//14)

/15 ; sust(room_15//15)

/16 ; sust(room_16//16)
e 18 ; sust(room_18/18)
n 13 ; sust(room_13/13)

/17 ; sust(room_17//17)
n 18 ; sust(room_18/18)
out 18 ; sust(room_18/18)

/18 ; sust(room_18//18)
in 17 ; sust(room_17/17)
s 17 ; sust(room_17/17)
w 16 ; sust(room_16/16)

/19 ; sust(room_19//19)
w 20 ; sust(room_20/20)

/20 ; sust(room_20//20)
e 19 ; sust(room_19/19)
n 21 ; sust(room_21/21)
w 8 ; sust(room_8/8)

/21 ; sust(room_21//21)
ne 28 ; sust(room_28/28)
s 20 ; sust(room_20/20)
w 22 ; sust(room_22/22)

/22 ; sust(room_22//22)
e 21 ; sust(room_21/21)
n 25 ; sust(room_25/25)
ne 27 ; sust(room_27/27)
w 23 ; sust(room_23/23)

/23 ; sust(room_23//23)
e 22 ; sust(room_22/22)

/24 ; sust(room_24//24)
se 26 ; sust(room_26/26)

/25 ; sust(room_25//25)
s 22 ; sust(room_22/22)

/26 ; sust(room_26//26)
d 22 ; sust(room_22/22)
nw 24 ; sust(room_24/24)

/27 ; sust(room_27//27)
sw 22 ; sust(room_22/22)

/28 ; sust(room_28//28)
sw 21 ; sust(room_21/21)

/29 ; sust(room_29//29)
e 30 ; sust(room_30/30)
w 21 ; sust(room_21/21)

/30 ; sust(room_30//30)
e 31 ; sust(room_31/31)
in 46 ; sust(room_46/46)
w 29 ; sust(room_29/29)

/31 ; sust(room_31//31)
e 32 ; sust(room_32/32)
ne 43 ; sust(room_43/43)
w 30 ; sust(room_30/30)

/32 ; sust(room_32//32)
e 42 ; sust(room_42/42)
w 31 ; sust(room_31/31)

/33 ; sust(room_33//33)
d 34 ; sust(room_34/34)

/34 ; sust(room_34//34)
e 35 ; sust(room_35/35)

/35 ; sust(room_35//35)
e 37 ; sust(room_37/37)
n 36 ; sust(room_36/36)
w 34 ; sust(room_34/34)

/36 ; sust(room_36//36)
s 35 ; sust(room_35/35)

/37 ; sust(room_37//37)
n 39 ; sust(room_39/39)
w 35 ; sust(room_35/35)

/38 ; sust(room_38//38)

/39 ; sust(room_39//39)
n 40 ; sust(room_40/40)
s 37 ; sust(room_37/37)

/40 ; sust(room_40//40)
n 55 ; sust(room_55/55)
s 39 ; sust(room_39/39)

/41 ; sust(room_41//41)

/42 ; sust(room_42//42)
n 43 ; sust(room_43/43)
w 32 ; sust(room_32/32)

/43 ; sust(room_43//43)
n 45 ; sust(room_45/45)
s 42 ; sust(room_42/42)
sw 31 ; sust(room_31/31)

/44 ; sust(room_44//44)
e 45 ; sust(room_45/45)

/45 ; sust(room_45//45)
n 47 ; sust(room_47/47)
s 43 ; sust(room_43/43)
w 44 ; sust(room_44/44)

/46 ; sust(room_46//46)
out 30 ; sust(room_30/30)

/47 ; sust(room_47//47)
e 52 ; sust(room_52/52)
s 45 ; sust(room_45/45)
w 48 ; sust(room_48/48)

/48 ; sust(room_48//48)
e 47 ; sust(room_47/47)
s 49 ; sust(room_49/49)

/49 ; sust(room_49//49)
n 48 ; sust(room_48/48)
s 50 ; sust(room_50/50)

/50 ; sust(room_50//50)
n 49 ; sust(room_49/49)
w 51 ; sust(room_51/51)

/51 ; sust(room_51//51)
e 50 ; sust(room_50/50)

/52 ; sust(room_52//52)
e 53 ; sust(room_53/53)
w 47 ; sust(room_47/47)

/53 ; sust(room_53//53)
s 54 ; sust(room_54/54)
w 52 ; sust(room_52/52)

/54 ; sust(room_54//54)
n 53 ; sust(room_53/53)
s 55 ; sust(room_55/55)

/55 ; sust(room_55//55)
n 54 ; sust(room_54/54)
s 40 ; sust(room_40/40)

/56 ; sust(room_56//56)
e 57 ; sust(room_57/57)
w 55 ; sust(room_55/55)

/57 ; sust(room_57//57)
e 58 ; sust(room_58/58)
w 56 ; sust(room_56/56)

/58 ; sust(room_58//58)
e 67 ; sust(room_67/67)
s 59 ; sust(room_59/59)
w 57 ; sust(room_57/57)

/59 ; sust(room_59//59)
e 60 ; sust(room_60/60)
n 58 ; sust(room_58/58)

/60 ; sust(room_60//60)
e 64 ; sust(room_64/64)
s 61 ; sust(room_61/61)
w 59 ; sust(room_59/59)

/61 ; sust(room_61//61)
n 60 ; sust(room_60/60)

/62 ; sust(room_62//62)
e 76 ; sust(room_76/76)
n 61 ; sust(room_61/61)

/63 ; sust(room_63//63)

/64 ; sust(room_64//64)
n 65 ; sust(room_65/65)
w 60 ; sust(room_60/60)

/65 ; sust(room_65//65)
e 66 ; sust(room_66/66)
s 64 ; sust(room_64/64)

/66 ; sust(room_66//66)
w 65 ; sust(room_65/65)

/67 ; sust(room_67//67)
n 68 ; sust(room_68/68)
w 58 ; sust(room_58/58)

/68 ; sust(room_68//68)
n 71 ; sust(room_71/71)
s 67 ; sust(room_67/67)

/69 ; sust(room_69//69)
w 70 ; sust(room_70/70)

/70 ; sust(room_70//70)
e 69 ; sust(room_69/69)
w 71 ; sust(room_71/71)

/71 ; sust(room_71//71)
e 70 ; sust(room_70/70)
s 68 ; sust(room_68/68)
w 72 ; sust(room_72/72)

/72 ; sust(room_72//72)
e 71 ; sust(room_71/71)
n 73 ; sust(room_73/73)

/73 ; sust(room_73//73)
n 74 ; sust(room_74/74)
s 72 ; sust(room_72/72)

/74 ; sust(room_74//74)
e 75 ; sust(room_75/75)
s 73 ; sust(room_73/73)

/75 ; sust(room_75//75)
w 74 ; sust(room_74/74)

/76 ; sust(room_76//76)
e 77 ; sust(room_77/77)
w 62 ; sust(room_62/62)

/77 ; sust(room_77//77)
e 78 ; sust(room_78/78)
w 76 ; sust(room_76/76)

/78 ; sust(room_78//78)
e 80 ; sust(room_80/80)
w 77 ; sust(room_77/77)

/79 ; sust(room_79//79)

/80 ; sust(room_80//80)
e 81 ; sust(room_81/81)
n 86 ; sust(room_86/86)
w 78 ; sust(room_78/78)

/81 ; sust(room_81//81)
s 82 ; sust(room_82/82)
w 80 ; sust(room_80/80)

/82 ; sust(room_82//82)
n 81 ; sust(room_81/81)
s 84 ; sust(room_84/84)

/83 ; sust(room_83//83)
w 84 ; sust(room_84/84)

/84 ; sust(room_84//84)
e 83 ; sust(room_83/83)
n 82 ; sust(room_82/82)
w 85 ; sust(room_85/85)

/85 ; sust(room_85//85)
e 84 ; sust(room_84/84)

/86 ; sust(room_86//86)
e 87 ; sust(room_87/87)
s 80 ; sust(room_80/80)

/87 ; sust(room_87//87)
w 86 ; sust(room_86/86)

/88 ; sust(room_88//88)
e 89 ; sust(room_89/89)
s 87 ; sust(room_87/87)

/89 ; sust(room_89//89)
se 91 ; sust(room_91/91)
w 88 ; sust(room_88/88)

/90 ; sust(room_90//90)

/91 ; sust(room_91//91)
nw 89 ; sust(room_89/89)
s 92 ; sust(room_92/92)

/92 ; sust(room_92//92)
n 91 ; sust(room_91/91)
s 93 ; sust(room_93/93)

/93 ; sust(room_93//93)
n 92 ; sust(room_92/92)
s 94 ; sust(room_94/94)

/94 ; sust(room_94//94)
n 93 ; sust(room_93/93)
s 95 ; sust(room_95/95)

/95 ; sust(room_95//95)
n 94 ; sust(room_94/94)
s 97 ; sust(room_97/97)

/96 ; sust(room_96//96)
w 97 ; sust(room_97/97)

/97 ; sust(room_97//97)
n 95 ; sust(room_95/95)
w 98 ; sust(room_98/98)

/98 ; sust(room_98//98)
e 97 ; sust(room_97/97)

/99 ; sust(room_99//99)
s 100 ; sust(room_100/100)

/100 ; sust(room_100//100)
e 100 ; sust(room_100/100)
n 99 ; sust(room_99/99)
s 101 ; sust(room_101/101)
w 100 ; sust(room_100/100)

/101 ; sust(room_101//101)
e 101 ; sust(room_101/101)
n 100 ; sust(room_100/100)
s 102 ; sust(room_102/102)
w 101 ; sust(room_101/101)

/102 ; sust(room_102//102)
n 101 ; sust(room_101/101)
s 104 ; sust(room_104/104)
w 103 ; sust(room_103/103)

/103 ; sust(room_103//103)
e 102 ; sust(room_102/102)

/104 ; sust(room_104//104)
e 105 ; sust(room_105/105)
n 102 ; sust(room_102/102)

/105 ; sust(room_105//105)
e 113 ; sust(room_113/113)
n 106 ; sust(room_106/106)
w 104 ; sust(room_104/104)

/106 ; sust(room_106//106)
in 107 ; sust(room_107/107)
s 105 ; sust(room_105/105)

/107 ; sust(room_107//107)
e 108 ; sust(room_108/108)

/108 ; sust(room_108//108)
e 109 ; sust(room_109/109)
w 107 ; sust(room_107/107)

/109 ; sust(room_109//109)
e 112 ; sust(room_112/112)
in 110 ; sust(room_110/110)
w 108 ; sust(room_108/108)

/110 ; sust(room_110//110)
n 111 ; sust(room_111/111)
s 109 ; sust(room_109/109)

/111 ; sust(room_111//111)
s 110 ; sust(room_110/110)

/112 ; sust(room_112//112)
w 109 ; sust(room_109/109)

/113 ; sust(room_113//113)
e 114 ; sust(room_114/114)
w 105 ; sust(room_105/105)

/114 ; sust(room_114//114)
e 116 ; sust(room_116/116)
w 113 ; sust(room_113/113)

/115 ; sust(room_115//115)

/116 ; sust(room_116//116)
e 116 ; sust(room_116/116)
n 116 ; sust(room_116/116)
s 116 ; sust(room_116/116)
u 117 ; sust(room_117/117)
w 114 ; sust(room_114/114)

/117 ; sust(room_117//117)
d 116 ; sust(room_116/116)
e 118 ; sust(room_118/118)

/118 ; sust(room_118//118)
e 119 ; sust(room_119/119)
w 117 ; sust(room_117/117)

/119 ; sust(room_119//119)
w 118 ; sust(room_118/118)

/120 ; sust(room_120//120)
e 121 ; sust(room_121/121)
s 119 ; sust(room_119/119)

/121 ; sust(room_121//121)
e 122 ; sust(room_122/122)
n 123 ; sust(room_123/123)
w 120 ; sust(room_120/120)

/122 ; sust(room_122//122)
w 121 ; sust(room_121/121)

/123 ; sust(room_123//123)
s 121 ; sust(room_121/121)

/124 ; sust(room_124//124)

/125 ; sust(room_125//125)

/126 ; sust(room_126//126)







/OBJ

/0  252      1    _    _       10000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_0_infr//0) ; sust(room_noncreated/252) ; sust(ght/ATTR)
/1  252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_1_ciga//1) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/2  252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_2_ligh//2) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/3               20      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_3//3) ; sust(room_20/20) ; sust(ATTR/ATTR)
/4           17      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_4_jet//4) ; sust(room_17/17) ; sust(ATTR/ATTR)
/5           24      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_5_key//5) ; sust(room_24/24) ; sust(ATTR/ATTR)
/6          19      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_6_ladd//6) ; sust(room_19/19) ; sust(ATTR/ATTR)
/7           28      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_7_tab//7) ; sust(room_28/28) ; sust(ATTR/ATTR)
/8           16      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_8_one//8) ; sust(room_16/16) ; sust(ATTR/ATTR)
/9          23      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_9_glue//9) ; sust(room_23/23) ; sust(ATTR/ATTR)
/10 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_10_matt//10) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/11 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_11_manu//11) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/12           9      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_12_gas//12) ; sust(room_9/9) ; sust(ATTR/ATTR)
/13         33      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_13_stat//13) ; sust(room_33/33) ; sust(ATTR/ATTR)
/14 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_14_batt//14) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/15         26      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_15_shoc//15) ; sust(room_26/26) ; sust(ATTR/ATTR)
/16         75      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_16_cuff//16) ; sust(room_75/75) ; sust(ATTR/ATTR)
/17 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_17_blow//17) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/18         54      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_18_boot//18) ; sust(room_54/54) ; sust(ATTR/ATTR)
/19 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_19_spon//19) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/20         66      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_20_brac//20) ; sust(room_66/66) ; sust(ATTR/ATTR)
/21           2      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_21_air//21) ; sust(room_2/2) ; sust(ATTR/ATTR)
/22 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_22_aqua//22) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/23          89      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_23_rem//23) ; sust(room_89/89) ; sust(ATTR/ATTR)
/24 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_24_shie//24) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/25  252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_25_sli//25) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/26 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_26_secu//26) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/27         96      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_27_wate//27) ; sust(room_96/96) ; sust(ATTR/ATTR)
/28         91      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_28_bomb//28) ; sust(room_91/91) ; sust(ATTR/ATTR)
/29  252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_29_gun//29) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/30         46      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_30_clip//30) ; sust(room_46/46) ; sust(ATTR/ATTR)
/31        112      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_31_phot//31) ; sust(room_112/112) ; sust(ATTR/ATTR)
/32              24      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_32//32) ; sust(room_24/24) ; sust(ATTR/ATTR)
/33              47      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_33//33) ; sust(room_47/47) ; sust(ATTR/ATTR)
/34              75      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_34//34) ; sust(room_75/75) ; sust(ATTR/ATTR)
/35              89      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_35//35) ; sust(room_89/89) ; sust(ATTR/ATTR)
/36              21      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_36//36) ; sust(room_21/21) ; sust(ATTR/ATTR)
/37              16      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_37//37) ; sust(room_16/16) ; sust(ATTR/ATTR)
/38              28      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_38//38) ; sust(room_28/28) ; sust(ATTR/ATTR)
/39              87      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_39//39) ; sust(room_87/87) ; sust(ATTR/ATTR)
/40              65      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_40//40) ; sust(room_65/65) ; sust(ATTR/ATTR)
/41              58      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_41//41) ; sust(room_58/58) ; sust(ATTR/ATTR)
/42              18      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_42//42) ; sust(room_18/18) ; sust(ATTR/ATTR)
/43              74      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_43//43) ; sust(room_74/74) ; sust(ATTR/ATTR)
/44              19      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_44//44) ; sust(room_19/19) ; sust(ATTR/ATTR)
/45      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_45//45) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/46      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_46//46) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/47      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_47//47) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/48             120      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_48//48) ; sust(room_120/120) ; sust(ATTR/ATTR)
/49             107      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_49//49) ; sust(room_107/107) ; sust(ATTR/ATTR)
/50              30      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_50//50) ; sust(room_30/30) ; sust(ATTR/ATTR)
/51              32      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_51//51) ; sust(room_32/32) ; sust(ATTR/ATTR)
/52              54      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_52//52) ; sust(room_54/54) ; sust(ATTR/ATTR)
/53 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_53_ciga//53) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/54      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_54//54) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/55      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_55//55) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/56 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_56_badg//56) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/57               3      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_57//57) ; sust(room_3/3) ; sust(ATTR/ATTR)
/58              21      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_58//58) ; sust(room_21/21) ; sust(ATTR/ATTR)
/59      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_59//59) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/60      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_60//60) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/61      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_61//61) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/62              55      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_62//62) ; sust(room_55/55) ; sust(ATTR/ATTR)
/63      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_63//63) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/64      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_64//64) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/65      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_65//65) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/66      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_66//66) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/67      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_67//67) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/68              66      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_68//68) ; sust(room_66/66) ; sust(ATTR/ATTR)
/69 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_69_disc//69) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)






/PRO 0








_ _
 hook    "RESPONSE_START"


_ _
 hook    "RESPONSE_USER"

n _
 at      87 ; sust(room_87/87)
 present 39 ; sust(object_39/39)
 writeln "The door is damaged-you are unable to move it!"
 done

n _
 at      87 ; sust(room_87/87)
 present 54 ; sust(object_54/54)
 writeln "The door is damaged-you are unable to move it!"
 done

n _
 at      87 ; sust(room_87/87)
 absent  54 ; sust(object_54/54)
 absent  39 ; sust(object_39/39)
 goto    88 ; sust(room_88/88)
 desc

n _
 at      119 ; sust(room_119/119)
 notcarr 29 ; sust(object_29_gun/29)
 writeln "You duck as enemy fire pins you down!"

n _
 at      119 ; sust(room_119/119)
 carried 29 ; sust(object_29_gun/29)
 goto    120 ; sust(room_120/120)
 desc

s ciga
 carried 1 ; sust(object_1_ciga/1)
 writeln "Hand-rolled"

s _
 at      61 ; sust(room_61/61)
 eq      24 0 ; sust(game_flag_24/24)
 writeln "The major drive system fires as the 4TH stage seperates-you are in direct line with the boosters"
 score
 turns
 end

s _
 at      61 ; sust(room_61/61)
 notzero 24 ; sust(game_flag_24/24)
 goto    62 ; sust(room_62/62)
 desc

e _
 at      21 ; sust(room_21/21)
 present 58 ; sust(object_58/58)
 writeln "Energy barriers block you!"

e _
 at      21 ; sust(room_21/21)
 absent  58 ; sust(object_58/58)
 goto    29 ; sust(room_29/29)
 desc

e _
 at      55 ; sust(room_55/55)
 present 62 ; sust(object_62/62)
 writeln "Energy barriers block you!"

e _
 at      55 ; sust(room_55/55)
 absent  62 ; sust(object_62/62)
 goto    56 ; sust(room_56/56)
 desc






u ladd
 at      22 ; sust(room_22/22)
 present 6 ; sust(object_6_ladd/6)
 writeln "You scramble up the ladder.."
 goto    26 ; sust(room_26/26)
 pause   150
 desc

u _
 at      22 ; sust(room_22/22)
 absent  6 ; sust(object_6_ladd/6)
 writeln "You can't quite reach the vent!"

u _
 at      22 ; sust(room_22/22)
 present 6 ; sust(object_6_ladd/6)
 notcarr 6 ; sust(object_6_ladd/6)
 writeln "You scramble up the ladder.."
 pause   150
 goto    26 ; sust(room_26/26)
 desc

ligh ciga
 present 12 ; sust(object_12_gas/12)
 present 1 ; sust(object_1_ciga/1)
 swap    1 53 ; sust(object_1_ciga/1) ; sust(object_53_ciga/53)
 ok

ligh ciga
 carried 1 ; sust(object_1_ciga/1)
 notcarr 12 ; sust(object_12_gas/12)
 writeln "With what?"

ligh ciga
 present 53 ; sust(object_53_ciga/53)
 writeln "It's already lit!"
 done

blow blowpipe
 at      65 ; sust(room_65/65)
 worn    24 ; sust(object_24_shie/24)
 carried 17 ; sust(object_17_blow/17)
 present 40 ; sust(object_40/40)
 writeln "The dart pierces the tank as destructor bolts wreck your shield-with a loud hiss the tanks burst, suffocating Erra Quann!Out of control the\nmachine skids off trailing smoke"
 destroy 40 ; sust(object_40/40)
 destroy 24 ; sust(object_24_shie/24)
 destroy 68 ; sust(object_68/68)
 plus    30 5 ; sust(total_player_score/30)

blow blowpipe
 notat   65 ; sust(room_65/65)
 carried 17 ; sust(object_17_blow/17)
 writeln "A small dart shoots out!"

drin wate
 carried 27 ; sust(object_27_wate/27)
 destroy 27 ; sust(object_27_wate/27)
 set     28 ; sust(pause_parameter/28)
 ok

pres butt
 at      4 ; sust(room_4/4)
 writeln "And which one might that be, mega-brain?"
 done

pres butt
 at      85 ; sust(room_85/85)
 writeln "And which one might that be, mega-brain?"
 done

pres red
 at      4 ; sust(room_4/4)

 writeln "The damaged controls spark into life-with a roar the plutonium based engines strain and in a huge explosion tear your ship apart!"
 turns
 score
 end
 done

pres red
 at      90 ; sust(room_90/90)
 writeln "The damaged engines ignite and the fighter erupts-a new star is suddenly visible from the planet surface!"
 turns
 score
 end
 done

pres blue
 at      4 ; sust(room_4/4)
 zero    21 ; sust(game_flag_21/21)
 create  56 ; sust(object_56_badg/56)
 set     21 ; sust(game_flag_21/21)
 writeln "A thin tube snakes out from the ship and docks with the Quann Tulla. A panel lights, indicating docking tube insecure!"
 pause   254

 plus    30 1 ; sust(total_player_score/30)
 desc
 done

pres blue
 at      4 ; sust(room_4/4)
 notzero 21 ; sust(game_flag_21/21)
 writeln "Nothing happens"
 done
 done

pres blue
 at      85 ; sust(room_85/85)

 writeln "QUANN TULLA IN ORBIT AROUND ORION &#35; 3"
 done

pres gree
 at      4 ; sust(room_4/4)

 writeln "The ships engines whine briefly but soon fade-the ship is beyond repair!"
 done

pres whit
 at      85 ; sust(room_85/85)

 writeln "The ships hyper-drive engages!"
 pause   254
 goto    27 ; sust(room_27/27)
 desc
 done

pres yell
 at      85 ; sust(room_85/85)

 writeln "ENEMY FIGHTERS APPROACHING SHIP!"
 done

pres _
 writeln "Oh well-the exercise will do you good!"

open door
 at      97 ; sust(room_97/97)
 notcarr 5 ; sust(object_5_key/5)
 writeln "The door is locked."

open door
 at      97 ; sust(room_97/97)
 carried 5 ; sust(object_5_key/5)
 writeln "The door unlocks and you enter."
 goto    96 ; sust(room_96/96)

open door
 at      23 ; sust(room_23/23)
 writeln "The door is damaged-you are unable to move it!"

open box
 at      3 ; sust(room_3/3)
 present 57 ; sust(object_57/57)
 present 5 ; sust(object_5_key/5)
 destroy 57 ; sust(object_57/57)
 create  0 ; sust(object_0_infr/0)
 create  1 ; sust(object_1_ciga/1)
 create  2 ; sust(object_2_ligh/2)
 writeln "The key vibrates the cupboard\nto wreckage!"
 plus    30 2 ; sust(total_player_score/30)
 done

open box
 at      3 ; sust(room_3/3)
 absent  5 ; sust(object_5_key/5)
 writeln "With what?"

open box
 at      3 ; sust(room_3/3)
 present 57 ; sust(object_57/57)
 present 5 ; sust(object_5_key/5)
 writeln "The key vibrates the cupboard\nto wreckage!"
 done

roll lead
 at      18 ; sust(room_18/18)
 carried 3 ; sust(object_3/3)
 present 42 ; sust(object_42/42)
 destroy 42 ; sust(object_42/42)
 destroy 3 ; sust(object_3/3)
 writeln "The crane lunges at the ball, but totally misjudging its weight it falls headlong into the waste chute, muttering\nloudly to itself!"
 plus    30 4 ; sust(total_player_score/30)

roll lead
 notat   18 ; sust(room_18/18)
 carried 3 ; sust(object_3/3)
 writeln "At what?"

sque glue
 carried 9 ; sust(object_9_glue/9)
 writeln "A thick blob of ultra-yuk III &#34;Superfast&#34;glue squirts out!"

sque glue
 at      16 ; sust(room_16/16)
 present 37 ; sust(object_37/37)
 carried 9 ; sust(object_9_glue/9)
 writeln "The glue covers the probe! &#34;NEAGH!&#34;it squeaks,&#34;I'M OFF!&#34;\nIt squelches loudly away.."
 destroy 37 ; sust(object_37/37)
 plus    30 4 ; sust(total_player_score/30)
 done

sque spon
 at      74 ; sust(room_74/74)
 present 43 ; sust(object_43/43)
 carried 19 ; sust(object_19_spon/19)
 destroy 43 ; sust(object_43/43)
 writeln "The light fades from the room briefly, and in the darkness the sharpshot is cornered by its own ricochets!There is a loud bang.."
 plus    30 5 ; sust(total_player_score/30)
 done

sque spon
 notat   74 ; sust(room_74/74)
 writeln "As you handle the sponge all the light around you seems to drain, but slowly fades back"
 done

smok ciga
 carried 53 ; sust(object_53_ciga/53)
 writeln "GOVT. WARNING-CIGARS CAN\nDAMAGE YOUR HEALTH!"

smok ciga
 carried 1 ; sust(object_1_ciga/1)
 writeln "It's not lit, dum-dum!"

give sli
 at      89 ; sust(room_89/89)
 present 25 ; sust(object_25_sli/25)
 destroy 35 ; sust(object_35/35)
 destroy 25 ; sust(object_25_sli/25)
 writeln "The computer examines the slip and with much consternation sets about the problem. After a few seconds it self-destructs, having proved to itself that it did not exist!"
 plus    30 11 ; sust(total_player_score/30)

give sli
 carried 25 ; sust(object_25_sli/25)
 notat   89 ; sust(room_89/89)
 writeln "Nobody here wants it!"

fire jet
 notat   58 ; sust(room_58/58)
 worn    4 ; sust(object_4_jet/4)
 writeln "The jets auto-lite, raising you briefly, but soon fade."

fire gun
 carried 29 ; sust(object_29_gun/29)
 writeln "A shot rings out!"

push droi
 at      87 ; sust(room_87/87)
 present 54 ; sust(object_54/54)
 worn    16 ; sust(object_16_cuff/16)
 writeln "The Droid rolls down the passage and hits the damaged doors-smoke pours up the corridor but as it clears you see the field has been destroyed!"
 destroy 54 ; sust(object_54/54)
 destroy 67 ; sust(object_67/67)
 plus    30 7 ; sust(total_player_score/30)
 done

push droi
 at      87 ; sust(room_87/87)
 present 39 ; sust(object_39/39)
 writeln "The droid is held in an inert field-you need a power supply!"
 done

push droi
 at      87 ; sust(room_87/87)
 present 54 ; sust(object_54/54)
 notworn 16 ; sust(object_16_cuff/16)
 writeln "You're not strong enough!"
 done

push _
 writeln "Oh well-the exercise will do you good!"

pull leve
 at      83 ; sust(room_83/83)
 writeln "You will insist in fiddling\nwith things, won't you?A trapdoor opens beneath your feet.."
 pause   254
 goto    90 ; sust(room_90/90)
 desc

inse infr
 at      24 ; sust(room_24/24)
 carried 0 ; sust(object_0_infr/0)
 present 32 ; sust(object_32/32)
 destroy 32 ; sust(object_32/32)
 writeln ">TERMINAL ONE ACCESSED\nQUANN DATAFILE LAST ENTRY.. massive negative power surge destroying crew-scanners show HighDome 1 source of energy- >DATALOCK"
 plus    30 4 ; sust(total_player_score/30)
 writeln "The computer sinks into a recess in the floor!"
 done

inse infr
 at      47 ; sust(room_47/47)
 present 33 ; sust(object_33/33)
 carried 0 ; sust(object_0_infr/0)
 destroy 33 ; sust(object_33/33)
 writeln ">TERMINAL 2 ACCESSED\n QUANN DATAFILE 2 HighDome 1 info stored-most advanced, self dependent fighting machine yet is also responsible for attack on Quann Tulla. Highdome moving west to present location. Operation manuals in library computer-code no. deleted from memory-engine units intact- >DATALOCK"
 destroy 62 ; sust(object_62/62)
 create  63 ; sust(object_63/63)
 clear   26 ; sust(game_flag_26/26)
 plus    30 2 ; sust(total_player_score/30)
 writeln "The computer sinks into a recess in the floor!"
 done

inse infr
 at      75 ; sust(room_75/75)
 carried 0 ; sust(object_0_infr/0)
 present 34 ; sust(object_34/34)
 destroy 34 ; sust(object_34/34)
 writeln ">TERMINAL 3 ACCESSED\nInterlock 3 doors switched off main computer housed N. of bridge{CLASS|center|         >DATALOCK}"
 plus    30 2 ; sust(total_player_score/30)
 set     24 ; sust(game_flag_24/24)
 writeln "The computer sinks into a recess in the floor!"
 done

inse infr
 carried 0 ; sust(object_0_infr/0)
 notat   24 ; sust(room_24/24)
 notat   47 ; sust(room_47/47)
 notat   75 ; sust(room_75/75)
 writeln "It does does not fit-and is hastily chewed up!"
 destroy 0 ; sust(object_0_infr/0)
 done

inse one
 at      21 ; sust(room_21/21)
 present 8 ; sust(object_8_one/8)
 present 36 ; sust(object_36/36)
 writeln "FUNCTION 1:INTERLOCK OPEN."
 pause   50
 destroy 8 ; sust(object_8_one/8)
 destroy 58 ; sust(object_58/58)
 create  59 ; sust(object_59/59)
 plus    30 4 ; sust(total_player_score/30)
 done

inse batt
 present 39 ; sust(object_39/39)
 carried 14 ; sust(object_14_batt/14)
 destroy 39 ; sust(object_39/39)
 create  54 ; sust(object_54/54)
 destroy 14 ; sust(object_14_batt/14)
 ok

inse rem
 carried 23 ; sust(object_23_rem/23)
 writeln "It does does not fit-and is hastily chewed up!"
 destroy 23 ; sust(object_23_rem/23)

inse secu
 writeln "It does does not fit-and is hastily chewed up!"
 destroy 26 ; sust(object_26_secu/26)

inse clip
 at      46 ; sust(room_46/46)
 carried 30 ; sust(object_30_clip/30)
 writeln "P-H-O-N-E--H-O-M-E-!"

inse phot
 at      111 ; sust(room_111/111)
 present 31 ; sust(object_31_phot/31)
 writeln "The photon pack slots into place easily and with a whoosh of spray the pod launches itself from the lake and into the higher atmosphere. Taking the controls you set the pod on course for home-WELL DONE!YOU HAVE ESCAPED!!"
 plus    30 10 ; sust(total_player_score/30)




 score
 turns
 end

sear tent
 at      113 ; sust(room_113/113)
 zero    20 ; sust(game_flag_20/20)
 create  29 ; sust(object_29_gun/29)
 set     20 ; sust(game_flag_20/20)
 plus    30 2 ; sust(total_player_score/30)
 desc

sear tent
 at      113 ; sust(room_113/113)
 notzero 20 ; sust(game_flag_20/20)
 writeln "You find nothing"
 done

sear pane
 at      103 ; sust(room_103/103)
 notzero 19 ; sust(game_flag_19/19)
 writeln "You find nothing"
 done

sear seat
 at      49 ; sust(room_49/49)
 zero    18 ; sust(game_flag_18/18)
 create  26 ; sust(object_26_secu/26)
 set     18 ; sust(game_flag_18/18)
 plus    30 2 ; sust(total_player_score/30)
 desc

sear seat
 at      49 ; sust(room_49/49)
 notzero 18 ; sust(game_flag_18/18)
 writeln "You find nothing"
 done

sear oil
 at      78 ; sust(room_78/78)
 zero    17 ; sust(game_flag_17/17)
 create  25 ; sust(object_25_sli/25)
 set     17 ; sust(game_flag_17/17)
 desc

sear oil
 at      78 ; sust(room_78/78)
 notzero 17 ; sust(game_flag_17/17)
 writeln "You find nothing"
 done

sear cant
 at      64 ; sust(room_64/64)
 zero    116 ; sust(game_flag_16/116)
 create  24 ; sust(object_24_shie/24)
 set     116 ; sust(game_flag_16/116)
 plus    30 2 ; sust(total_player_score/30)
 desc

sear cant
 at      64 ; sust(room_64/64)
 notzero 116 ; sust(game_flag_16/116)
 writeln "You find nothing"
 done

sear cupb
 at      122 ; sust(room_122/122)
 zero    115 ; sust(game_flag_15/115)
 create  22 ; sust(object_22_aqua/22)
 set     115 ; sust(game_flag_15/115)
 plus    30 2 ; sust(total_player_score/30)
 desc

sear cupb
 at      122 ; sust(room_122/122)
 notzero 115 ; sust(game_flag_15/115)
 writeln "You find nothing"
 done

sear dust
 at      69 ; sust(room_69/69)
 zero    114 ; sust(game_flag_14/114)
 create  19 ; sust(object_19_spon/19)
 set     114 ; sust(game_flag_14/114)
 desc

sear dust
 at      69 ; sust(room_69/69)
 notzero 114 ; sust(game_flag_14/114)
 writeln "You find nothing"
 done

sear draw
 at      85 ; sust(room_85/85)
 zero    113 ; sust(game_flag_13/113)
 create  17 ; sust(object_17_blow/17)
 set     113 ; sust(game_flag_13/113)
 desc

sear draw
 at      85 ; sust(room_85/85)
 notzero 113 ; sust(game_flag_13/113)
 writeln "You find nothing"
 done

sear cogs
 at      51 ; sust(room_51/51)
 zero    112 ; sust(game_flag_12/112)
 create  14 ; sust(object_14_batt/14)
 set     112 ; sust(game_flag_12/112)
 plus    30 2 ; sust(total_player_score/30)
 desc

sear cogs
 at      51 ; sust(room_51/51)
 notzero 112 ; sust(game_flag_12/112)
 writeln "You find nothing"
 done

sear lock
 at      44 ; sust(room_44/44)
 zero    111 ; sust(game_flag_11/111)
 create  10 ; sust(object_10_matt/10)
 set     111 ; sust(game_flag_11/111)
 plus    30 2 ; sust(total_player_score/30)
 desc

sear lock
 at      44 ; sust(room_44/44)
 notzero 111 ; sust(game_flag_11/111)
 writeln "You find nothing"
 done

sear _
 writeln "You find nothing"

exam tele
 present 45 ; sust(object_45/45)
 writeln "Vocal slave operant.( Comjump Ltd.)"

exam tele
 present 46 ; sust(object_46/46)
 writeln "Vocal slave operant.( Comjump Ltd.)"

exam infr
 present 0 ; sust(object_0_infr/0)
 writeln "A small plastic card of magnetic tape, it enables the owner to access certain computer systems"
 done

exam ciga
 present 1 ; sust(object_1_ciga/1)
 writeln "Hand-rolled"
 done

exam ciga
 present 53 ; sust(object_53_ciga/53)
 writeln "Hand-rolled"
 done

exam light
 present 2 ; sust(object_2_ligh/2)
 writeln "A long tube of volatile gas-it looks like some kind of weapon"
 done

exam light
 present 12 ; sust(object_12_gas/12)
 writeln "Large Red decals read,&#34;Quann Tulla Souvenir Shop&#34;"
 done

exam lead
 present 3 ; sust(object_3/3)
 writeln "It's lead."
 done

exam jet
 present 4 ; sust(object_4_jet/4)
 writeln "A twin-thruster hover-pack"
 done

exam key
 present 5 ; sust(object_5_key/5)
 writeln "An electronic key-suitable for most locks"
 done

exam ladd
 present 6 ; sust(object_6_ladd/6)
 writeln "They're very light!"
 done

exam tab
 present 7 ; sust(object_7_tab/7)
 writeln "A small wisp of acid rises as you handle it.."
 done

exam one
 present 8 ; sust(object_8_one/8)
 writeln "recently devalued to a 0.05 Cred piece."
 done

exam glue
 present 9 ; sust(object_9_glue/9)
 writeln "Amidst the &#34;Techno-flash&#34;adverts you can just about read, SQUEEZET O USE>."
 done

exam matt
 present 10 ; sust(object_10_matt/10)
 writeln "A small negapos emiter circuit it emits negative energy in huge waves-it appears to be switched to overload!"
 done

exam manu
 present 11 ; sust(object_11_manu/11)
 writeln "Microfilm code-lock protected it is marked&#34;ION DRIVE DESIGN&#34;"
 done

exam stat
 present 13 ; sust(object_13_stat/13)
 writeln "An anti-ion particle imploder. With a suitable detonator, and placed in the heart of the chain drive it will tear the Quann Tulla apart!"
 done

exam batt
 present 14 ; sust(object_14_batt/14)
 writeln "A standard Never-Ready +2"
 done

exam shoc
 present 15 ; sust(object_15_shoc/15)
 writeln "Insulation-lined to prevent most electric shocks."
 done

exam cuff
 present 16 ; sust(object_16_cuff/16)
 writeln "&#34;Muscle-enhancers to increase even the puniest wearers push&#34;"
 done

exam blowpipe
 present 17 ; sust(object_17_blow/17)
 writeln "A considerable advance on the Amazonian standard model-a full dart clip is loaded."
 done

exam boot
 present 18 ; sust(object_18_boot/18)
 writeln "Made to measure"
 done

exam spon
 present 19 ; sust(object_19_spon/19)
 writeln "As you handle the sponge all the light around you seems to drain, but slowly fades back"
 done

exam brac
 present 20 ; sust(object_20_brac/20)
 writeln "Voice-encoded, it allows the user to operate certain teleports"
 done

exam air
 present 21 ; sust(object_21_air/21)
 writeln "A small metal-rimmed &#34;breather&#34; tank it holds a full capacity of breathable air."
 done

exam aqua
 present 22 ; sust(object_22_aqua/22)
 writeln "For underwater use"
 done

exam rem
 present 23 ; sust(object_23_rem/23)
 writeln "It reads, &#34;Library code Operation Manuals, ENTER CODE XXXX&#34;"
 done

exam shie
 present 24 ; sust(object_24_shie/24)
 writeln "A personal deflector shield if worn"
 done

exam sli
 present 25 ; sust(object_25_sli/25)
 writeln "It holds a massive mathematical problem that seems to try and prove that 1+1=0?"
 done

exam secu
 present 26 ; sust(object_26_secu/26)
 writeln "For access to highly sensitive areas"
 done

exam wate
 present 27 ; sust(object_27_wate/27)
 writeln "Looks very dodgy!"
 done

exam bomb
 present 28 ; sust(object_28_bomb/28)
 writeln "With a suitable power field, it would make a very powerful bomb"
 done

exam gun
 present 29 ; sust(object_29_gun/29)
 writeln "A hand-held weapon"
 done

exam clip
 present 30 ; sust(object_30_clip/30)
 writeln "Sol-Telecom issue."
 done

exam phot
 present 31 ; sust(object_31_phot/31)
 writeln "The heart of the Empire-type cutter craft."
 done

exam sign
 at      45 ; sust(room_45/45)
 writeln "The sign-post has been switched!"
 done

exam quan
 present 32 ; sust(object_32/32)
 writeln "The first terminal of the Quann Tulla computer system"
 done

exam quan
 present 33 ; sust(object_33/33)
 writeln "The 2nd terminal of the Quann Tulla computer system"
 done

exam quan
 present 34 ; sust(object_34/34)
 writeln "The 3rd terminal of the Quann Tulla computer system"
 done

exam comp
 present 35 ; sust(object_35/35)
 writeln "The very heart of the ships operation modules"
 done

exam spea
 present 36 ; sust(object_36/36)
 writeln "The latest model of the Tete a' Tete systems"
 done

exam prob
 present 37 ; sust(object_37/37)
 writeln "A highly logical maintenance operant. It appears to be quite agitated by your appearance."
 done

exam robo
 present 38 ; sust(object_38/38)
 writeln "It has been thoroughly cleaned s mells faintly of Domesta +     b leach. It sniffs loudly at you, r unning an electric eye up and  d own.."
 done

exam droi
 present 39 ; sust(object_39/39)
 writeln "A purpose built all operation machine, it is in very poor shape"
 done

exam droi
 present 54 ; sust(object_54/54)
 writeln "A purpose built all operation machine, it is in very poor shape"
 done

exam high
 present 40 ; sust(object_40/40)
 writeln "A vacuum-sealed floatbubble of indeterminate armour plating it contains the very latest in all technological equipment. A tinted dome tops the bodywork and huge air tanks are housed at the rear -it buzzes alarmingly!"
 done

exam net
 present 41 ; sust(object_41/41)
 writeln "Nobody here wants it!"
 done

exam cran
 present 42 ; sust(object_42/42)
 writeln "A satellite loading arm"
 done

exam shar
 present 43 ; sust(object_43/43)
 writeln "A small box containing photo- 'lectric eye cells it scans the immediate area relentlessly"
 done

exam libr
 present 44 ; sust(object_44/44)
 writeln "An instant reference databank, it is code accessed for ease of use"
 done

exam guar
 present 47 ; sust(object_47/47)
 writeln "It's nothing special"
 done

exam guar
 present 48 ; sust(object_48/48)
 writeln "The sign-post has been switched!"
 done

exam forc
 present 49 ; sust(object_49/49)
 writeln "Random violence is not recommended!"
 done

exam forc
 present 52 ; sust(object_52/52)
 writeln "A catal deposit of the remaining anti-neg field that attacked the ship"
 done

exam box
 at      30 ; sust(room_30/30)
 writeln "Sol Telecom coin operated. Cheap rates at weekends."
 done

exam box
 at      3 ; sust(room_3/3)
 present 57 ; sust(object_57/57)
 writeln "It has been crushed during the voyage-an ordinary key won't open it!"
 done

exam gril
 present 51 ; sust(object_51/51)
 writeln "Thick metal cut bar"
 done

exam gas
 present 12 ; sust(object_12_gas/12)
 writeln "Large Red decals read,&#34;Quann Tulla Souvenir Shop&#34;"
 done

exam badg
 present 56 ; sust(object_56_badg/56)
 writeln "A special lightweight Antigrav device-it decreases your weight if worn!"
 done

exam disc
 present 69 ; sust(object_69_disc/69)
 writeln "It is running-just get it to the weakest engine chain and it will auto-arm!"
 done

exam _
 writeln "It's nothing special"

t infr
 get     0 ; sust(object_0_infr/0)
 ok

t infr
 get     0 ; sust(object_0_infr/0)
 ok

t ciga
 present 1 ; sust(object_1_ciga/1)
 get     1 ; sust(object_1_ciga/1)
 ok

t ciga
 present 53 ; sust(object_53_ciga/53)
 get     53 ; sust(object_53_ciga/53)
 ok

t light
 get     2 ; sust(object_2_ligh/2)
 ok

t lead
 get     3 ; sust(object_3/3)
 writeln "The ball shrinks!"

t jet
 get     4 ; sust(object_4_jet/4)
 ok

t key
 get     5 ; sust(object_5_key/5)
 ok

t ladd
 get     6 ; sust(object_6_ladd/6)
 ok

t tab
 get     7 ; sust(object_7_tab/7)
 ok

t one
 get     8 ; sust(object_8_one/8)
 ok

t glue
 get     9 ; sust(object_9_glue/9)
 ok

t matt
 get     10 ; sust(object_10_matt/10)
 ok

t manu
 get     11 ; sust(object_11_manu/11)
 ok

t stat
 get     13 ; sust(object_13_stat/13)
 ok

t batt
 get     14 ; sust(object_14_batt/14)
 ok

t shoc
 get     15 ; sust(object_15_shoc/15)
 ok

t cuff
 get     16 ; sust(object_16_cuff/16)
 ok

t blowpipe
 get     17 ; sust(object_17_blow/17)
 ok

t boot
 get     18 ; sust(object_18_boot/18)
 ok

t spon
 get     19 ; sust(object_19_spon/19)
 ok

t brac
 get     20 ; sust(object_20_brac/20)
 ok

t air
 get     21 ; sust(object_21_air/21)
 ok

t aqua
 get     22 ; sust(object_22_aqua/22)
 ok

t rem
 get     23 ; sust(object_23_rem/23)
 ok

t shie
 get     24 ; sust(object_24_shie/24)
 ok

t sli
 get     25 ; sust(object_25_sli/25)
 ok

t secu
 get     26 ; sust(object_26_secu/26)
 ok

t wate
 get     27 ; sust(object_27_wate/27)
 ok

t bomb
 get     28 ; sust(object_28_bomb/28)
 ok

t gun
 present 29 ; sust(object_29_gun/29)
 get     29 ; sust(object_29_gun/29)
 ok

t clip
 get     30 ; sust(object_30_clip/30)
 ok

t phot
 get     31 ; sust(object_31_phot/31)
 ok

t gas
 get     12 ; sust(object_12_gas/12)
 ok

t badg
 get     56 ; sust(object_56_badg/56)
 ok

t disc
 get     69 ; sust(object_69_disc/69)
 ok

t i
 inven

drop infr
 drop    0 ; sust(object_0_infr/0)
 ok

drop ciga
 present 53 ; sust(object_53_ciga/53)
 drop    53 ; sust(object_53_ciga/53)
 ok

drop ciga
 present 1 ; sust(object_1_ciga/1)
 drop    1 ; sust(object_1_ciga/1)
 ok

drop light
 eq      22 0 ; sust(game_flag_22/22)
 carried 2 ; sust(object_2_ligh/2)
 writeln "The lightstick explodes, engulfing you in flames!"
 score
 turns
 end

drop light
 carried 2 ; sust(object_2_ligh/2)
 notzero 22 ; sust(game_flag_22/22)
 writeln "The Empire guards fall upon you with cries of victory but the lightstick explodes as you fall! The troops take the full force of the blast&#59;shielding you!"
 destroy 2 ; sust(object_2_ligh/2)
 clear   22 ; sust(game_flag_22/22)
 plus    30 4 ; sust(total_player_score/30)

drop lead
 drop    3 ; sust(object_3/3)
 writeln "The ball grows!"

drop lead
 at      18 ; sust(room_18/18)
 present 42 ; sust(object_42/42)
 notcarr 3 ; sust(object_3/3)
 writeln "A massive auto-arm crane races from the shadows and with a\nloud rasp hurls you through a garbage chute into space.. (Weigh yourself down next time!)"
 turns
 score
 end

drop jet
 drop    4 ; sust(object_4_jet/4)
 ok

drop key
 drop    5 ; sust(object_5_key/5)
 ok

drop ladd
 drop    6 ; sust(object_6_ladd/6)
 ok

drop tab
 at      32 ; sust(room_32/32)
 carried 7 ; sust(object_7_tab/7)
 destroy 51 ; sust(object_51/51)
 destroy 7 ; sust(object_7_tab/7)
 writeln "The tablet destroys the grill!"
 plus    30 4 ; sust(total_player_score/30)
 done

drop tab
 drop    7 ; sust(object_7_tab/7)
 ok

drop one
 drop    8 ; sust(object_8_one/8)
 ok

drop glue
 drop    9 ; sust(object_9_glue/9)
 ok

drop matt
 drop    10 ; sust(object_10_matt/10)
 ok

drop manu
 drop    11 ; sust(object_11_manu/11)
 ok

drop stat
 drop    13 ; sust(object_13_stat/13)
 ok

drop batt
 drop    14 ; sust(object_14_batt/14)
 ok

drop shoc
 drop    15 ; sust(object_15_shoc/15)
 ok

drop cuff
 drop    16 ; sust(object_16_cuff/16)
 ok

drop blowpipe
 drop    17 ; sust(object_17_blow/17)
 ok

drop boot
 drop    18 ; sust(object_18_boot/18)
 ok

drop spon
 drop    19 ; sust(object_19_spon/19)
 ok

drop brac
 drop    20 ; sust(object_20_brac/20)
 ok

drop air
 drop    21 ; sust(object_21_air/21)
 ok

drop aqua
 drop    22 ; sust(object_22_aqua/22)
 ok

drop rem
 drop    23 ; sust(object_23_rem/23)
 ok

drop shie
 drop    24 ; sust(object_24_shie/24)
 ok

drop sli
 drop    25 ; sust(object_25_sli/25)
 ok

drop secu
 drop    26 ; sust(object_26_secu/26)
 ok

drop wate
 drop    27 ; sust(object_27_wate/27)
 ok

drop bomb
 drop    28 ; sust(object_28_bomb/28)
 writeln "Okay-but it's no good in it's present state!"

drop gun
 drop    29 ; sust(object_29_gun/29)
 ok

drop clip
 drop    30 ; sust(object_30_clip/30)
 ok

drop phot
 drop    31 ; sust(object_31_phot/31)
 ok

drop gas
 drop    12 ; sust(object_12_gas/12)
 ok

drop badg
 drop    56 ; sust(object_56_badg/56)
 ok

drop disc
 drop    69 ; sust(object_69_disc/69)
 ok

remo jet
 remove  4 ; sust(object_4_jet/4)
 ok

remo shoc
 remove  15 ; sust(object_15_shoc/15)
 ok

remo cuff
 remove  16 ; sust(object_16_cuff/16)
 ok

remo boot
 remove  18 ; sust(object_18_boot/18)
 ok

remo brac
 remove  20 ; sust(object_20_brac/20)
 ok

remo air
 remove  21 ; sust(object_21_air/21)
 ok

remo aqua
 remove  22 ; sust(object_22_aqua/22)
 ok

remo shie
 remove  24 ; sust(object_24_shie/24)
 ok

remo badg
 remove  56 ; sust(object_56_badg/56)
 ok

wear jet
 wear    4 ; sust(object_4_jet/4)
 ok

wear shoc
 wear    15 ; sust(object_15_shoc/15)
 ok

wear cuff
 wear    16 ; sust(object_16_cuff/16)
 ok

wear boot
 wear    18 ; sust(object_18_boot/18)
 ok

wear brac
 wear    20 ; sust(object_20_brac/20)
 ok

wear air
 zero    29 ; sust(bitset_graphics_status/29)
 wear    21 ; sust(object_21_air/21)
 clear   9 ; sust(countdown_player_input_dark/9)
 ok

wear aqua
 wear    22 ; sust(object_22_aqua/22)
 ok

wear shie
 wear    24 ; sust(object_24_shie/24)
 ok

wear badg
 wear    56 ; sust(object_56_badg/56)
 ok

i _
 inven

r _
 desc

quit _
 quit
 turns
 end

save _
 save

re _
 load

say tele
 at      98 ; sust(room_98/98)
 worn    20 ; sust(object_20_brac/20)
 notzero 25 ; sust(game_flag_25/25)
 goto    99 ; sust(room_99/99)
 desc
 done

say tele
 at      36 ; sust(room_36/36)
 worn    20 ; sust(object_20_brac/20)
 writeln "The teleport is damaged.. you are torn apart, atom by atom!"
 turns
 score
 end
 done

say tele
 at      36 ; sust(room_36/36)
 notworn 20 ; sust(object_20_brac/20)
 writeln "You can't teleport without wearing a working teleport bracelet!"
 done

say tele
 at      98 ; sust(room_98/98)
 notworn 20 ; sust(object_20_brac/20)
 writeln "You can't teleport without wearing a working teleport bracelet!"
 done

say tele
 at      98 ; sust(room_98/98)
 eq      25 0 ; sust(game_flag_25/25)
 writeln "DEFENCE SYSTEMS REMAIN INTACT- TELEPORT SELF-DESTRUCT FUNCTION TO BE FULFILLED- Statlasers shoot out!"
 turns
 score
 end
 done

say xxxx
 at      19 ; sust(room_19/19)
 present 44 ; sust(object_44/44)
 create  11 ; sust(object_11_manu/11)
 destroy 44 ; sust(object_44/44)
 writeln "The computer sinks into a recess in the floor!"
 pause   250
 desc
 done


_ _
 hook    "RESPONSE_DEFAULT_START"

say _
 writeln "Mumble.. Mumble.. Talking to our- selves now, are we?"
 done

atta _
 writeln "Random violence is not recommended!"
 done

help _
 writeln "Prime directive 4/3:Random violence is not recommended!"
 done

scor _
 score

fuck _
 writeln "Hygiene clone II appears from nowhere with a bar of soap to clean out your mouth!"
 done

turn _
 turns

read _
 writeln "Without a portacube de-coder\nthe code is quite beyond you!"
 done

wash _
 writeln "How wonderfully hygenic!"
 done


_ _
 hook    "RESPONSE_DEFAULT_END"







/PRO 1


_ _
 hook "PRO1"

_ _
 at     0
 bclear 12 5                      ; Set language to English

_ _
 islight
 listobj                        ; Lists present objects
 listnpc @38                    ; Lists present NPCs







/PRO 2


_ _
 eq      31 0 ; sust(total_turns_lower/31)
 eq      32 0 ; sust(total_turns_higher/32)
 ability 4 4


_ _
 hook    "PRO2"

_ _
 at      30 ; sust(room_30/30)
 eq      27 0 ; sust(game_flag_27/27)
 set     26 ; sust(game_flag_26/26)
 writeln "The temperature is being lowered as a defensive measure by Quann terminal 2-It must be turned off immediately!"
 set     27 ; sust(game_flag_27/27)

_ _
 notzero 26 ; sust(game_flag_26/26)
 minus   26 1 ; sust(game_flag_26/26)
 writeln "You are freezing cold!"

_ _
 eq      26 246 ; sust(game_flag_26/26)
 writeln "You freeze to death!"
 score
 turns
 end

_ _
 eq      9 5 ; sust(countdown_player_input_dark/9)
 writeln "You have suffocated!"
 score
 turns
 end

_ _
 at      32 ; sust(room_32/32)
 absent  51 ; sust(object_51/51)
 writeln "You fall through the hole!"
 pause   150
 goto    33 ; sust(room_33/33)
 desc

_ _
 at      107 ; sust(room_107/107)
 present 21 ; sust(object_21_air/21)
 writeln "The force field is attracted to the metal mask--you are blasted!"
 score
 turns
 end

_ _
 at      107 ; sust(room_107/107)
 present 49 ; sust(object_49/49)
 notworn 22 ; sust(object_22_aqua/22)
 writeln "You cannot breathe beneath the water--unable to reach the surface you drown!"
 score
 turns
 end

_ _
 at      107 ; sust(room_107/107)
 worn    22 ; sust(object_22_aqua/22)
 absent  21 ; sust(object_21_air/21)
 writeln "You sink slowly beneath the surface to the sea-bed."

_ _
 at      62 ; sust(room_62/62)
 present 65 ; sust(object_65/65)
 turns
 score
 end

_ _
 at      65 ; sust(room_65/65)
 notworn 24 ; sust(object_24_shie/24)
 present 40 ; sust(object_40/40)
 chance  90
 writeln "A score of destructor bolts shoot out!"
 score
 turns
 end

_ _
 at      65 ; sust(room_65/65)
 worn    24 ; sust(object_24_shie/24)
 present 40 ; sust(object_40/40)
 writeln "Destructor bolts are deflected by the shield, but the sheer weight of them forces you to the ground!"

_ _
 at      66 ; sust(room_66/66)
 present 68 ; sust(object_68/68)
 score
 turns
 end

_ _
 atgt    98 ; sust(room_98/98)
 zero    28 ; sust(pause_parameter/28)
 writeln "You are very thirsty-the teleport has dehydrated you--you desperately need water!"

_ _
 atgt    101 ; sust(room_101/101)
 zero    28 ; sust(pause_parameter/28)
 writeln "Without water, you die of thirst!"
 score
 turns
 end

_ _
 atgt    124 ; sust(room_124/124)
 set     28 ; sust(pause_parameter/28)
 done

_ _
 at      18 ; sust(room_18/18)
 notcarr 3 ; sust(object_3/3)
 present 42 ; sust(object_42/42)
 writeln "A massive auto-arm crane races from the shadows and with a\nloud rasp hurls you through a garbage chute into space.. (Weigh yourself down next time!)"
 score
 turns
 end

_ _
 at      74 ; sust(room_74/74)
 notcarr 19 ; sust(object_19_spon/19)
 present 43 ; sust(object_43/43)
 writeln "Blinding lights dazzle you as\na sharpshot hunter Tracer fires a volley of tracer shots-deadly accurate, every one hits!"
 turns
 score
 end

_ _
 at      74 ; sust(room_74/74)
 carried 19 ; sust(object_19_spon/19)
 present 43 ; sust(object_43/43)
 writeln "The sharpshoot pauses briefly, considering its aim.."

_ _
 at      74 ; sust(room_74/74)
 carried 19 ; sust(object_19_spon/19)
 present 43 ; sust(object_43/43)
 present 16 ; sust(object_16_cuff/16)
 writeln "The sharpshot, having taken aim blasts a volley at you as you enter!"
 turns
 score
 end

_ _
 at      28 ; sust(room_28/28)
 carried 53 ; sust(object_53_ciga/53)
 present 38 ; sust(object_38/38)
 destroy 38 ; sust(object_38/38)
 writeln "The creature sees the thick clouds of smoke and turns a\ndark shade of green-&#34;UGH!&#34;\nit yells,&#34;HUMANS!&#34;-and swiftly disappears."
 plus    30 4 ; sust(total_player_score/30)

_ _
 at      94 ; sust(room_94/94)
 notcarr 26 ; sust(object_26_secu/26)
 writeln "A remaining securi-robot rushes to you and seeing you have no securi-card carries out it sole purpose. It shoots you."
 turns
 score
 end

_ _
 at      94 ; sust(room_94/94)
 carried 26 ; sust(object_26_secu/26)
 writeln "A securi-robot rushes up to you and examines the card.&#34;THAT'LL DO NICELY!&#34;it clicks, and zooms away!"

_ _
 at      58 ; sust(room_58/58)
 worn    4 ; sust(object_4_jet/4)
 present 41 ; sust(object_41/41)
 destroy 41 ; sust(object_41/41)
 destroy 4 ; sust(object_4_jet/4)
 writeln "The boosterpak auto-fires as a light net rises from the floor! The jets raise you above the net of energy, but the resulting explosion destroys both the trap and the boosterpak, throwing you to the ground!"
 plus    30 5 ; sust(total_player_score/30)
 done

_ _
 at      58 ; sust(room_58/58)
 notworn 4 ; sust(object_4_jet/4)
 present 41 ; sust(object_41/41)
 writeln "A latticed energy net shoots across the room at your feet--there is no time to act!"
 score
 turns
 end

_ _
 at      120 ; sust(room_120/120)
 notcarr 29 ; sust(object_29_gun/29)
 present 48 ; sust(object_48/48)
 writeln "The temperature is being lowered as a defensive measure by Quann terminal 2--It must be turned off immediately!"
 score
 turns
 end

_ _
 at      120 ; sust(room_120/120)
 carried 29 ; sust(object_29_gun/29)
 present 48 ; sust(object_48/48)
 writeln "The sight of the gun throws the troops into panic-they stage a tactical withdrawal.."
 destroy 48 ; sust(object_48/48)

_ _
 at      28 ; sust(room_28/28)
 present 53 ; sust(object_53_ciga/53)
 present 38 ; sust(object_38/38)
 writeln "The robot produces an enormous scrubbing brush-&#34;that's your lot!&#34;it shouts-&#34;stand by for a scrubbing!&#34;.. a mass of metal bristles engulf you…"
 turns
 score
 end

_ _
 at      28 ; sust(room_28/28)
 present 38 ; sust(object_38/38)
 carried 7 ; sust(object_7_tab/7)
 writeln "The robot produces an enormous scrubbing brush-&#34;that's your lot!&#34;it shouts-&#34;stand by for a scrubbing!&#34;.. a mass of metal bristles engulf you…"
 turns
 score
 end

_ _
 at      16 ; sust(room_16/16)
 carried 8 ; sust(object_8_one/8)
 present 37 ; sust(object_37/37)
 writeln "The probe descends upon you in\na rage-&#34;THEY'RE MINE!&#34;it yells, carrying you off to the reproc. vats!"
 turns
 score
 end

_ _
 carried 13 ; sust(object_13_stat/13)
 carried 28 ; sust(object_28_bomb/28)
 destroy 13 ; sust(object_13_stat/13)
 destroy 28 ; sust(object_28_bomb/28)
 create  69 ; sust(object_69_disc/69)
 get     69 ; sust(object_69_disc/69)
 writeln "The static disrupter and limpet bomb lock together to create an Emploder disc!"

_ _
 at      18 ; sust(room_18/18)
 present 42 ; sust(object_42/42)
 carried 3 ; sust(object_3/3)
 writeln "The Crane arm stops in mid-lift, watching you carefully, eyeing the lead ball.."

_ _
 at      54 ; sust(room_54/54)
 carried 10 ; sust(object_10_matt/10)
 present 52 ; sust(object_52/52)
 writeln "The matter displacer disrupts the displacer field and in a surge of anti-matter the room explodes!"
 score
 turns
 end

_ _
 atlt    99 ; sust(room_99/99)
 eq      5 1 ; sust(countdown_player_input_1/5)
 writeln "With a huge explosion the ship is torn apart!You are successful but fail to escape!"
 score
 turns
 end
 done

_ _
 at      96 ; sust(room_96/96)
 carried 69 ; sust(object_69_disc/69)
 destroy 69 ; sust(object_69_disc/69)
 writeln "The Emploder flies from your arms and in a flurry of sparks disappears into the engine drive!You have started a chain reaction-abandon ship!The Quann Tulla is about to explode!"
 plus    30 10 ; sust(total_player_score/30)
 let     5 11 ; sust(countdown_player_input_1/5)
 set     25 ; sust(game_flag_25/25)

_ _
 atlt    99 ; sust(room_99/99)
 eq      5 5 ; sust(countdown_player_input_1/5)
 writeln "QUANN TULLA SELF DESTRUCTION IMMINENT!"

_ _
 atlt    99 ; sust(room_99/99)
 eq      5 2 ; sust(countdown_player_input_1/5)
 writeln "The entire keel of the ship shifts below you-An enormous wave of energy rises from every direction, shredding the ship as it rushes to engulf you!"

_ _
 at      9 ; sust(room_9/9)
 notworn 15 ; sust(object_15_shoc/15)
 writeln "Massive bolts of electricity shoot from the cables, electrocuting you instantly!"
 turns
 score
 end

_ _
 at      9 ; sust(room_9/9)
 worn    15 ; sust(object_15_shoc/15)
 writeln "Massive bolts of electricity shoot from the cables but the ShocCape insulates you!"

_ _
 at      71 ; sust(room_71/71)
 notworn 18 ; sust(object_18_boot/18)
 writeln "Without the correct footwear\nthe rolling of the damaged ship pitches you into space.."
 score
 turns
 end

_ _
 at      71 ; sust(room_71/71)
 worn    18 ; sust(object_18_boot/18)
 writeln "The magnet boots hold you firm."

_ _
 atgt    6 ; sust(room_6/6)
 notzero 29 ; sust(bitset_graphics_status/29)
 writeln "You have suffocated!"
 turns
 score
 end

_ _
 atgt    98 ; sust(room_98/98)
 clear   29 ; sust(bitset_graphics_status/29)

_ _
 atgt    6 ; sust(room_6/6)
 atlt    99 ; sust(room_99/99)
 notworn 21 ; sust(object_21_air/21)
 set     29 ; sust(bitset_graphics_status/29)

_ _
 atlt    8 ; sust(room_8/8)
 notworn 21 ; sust(object_21_air/21)
 writeln "You can't breathe!"
 plus    9 1 ; sust(countdown_player_input_dark/9)

_ _
 at      6 ; sust(room_6/6)
 notworn 56 ; sust(object_56_badg/56)
 writeln "You are too heavy for the tube! With a loud tearing of fabric it collapses beneath you, hurling you into space.."
 turns
 score
 end

_ _
 at      6 ; sust(room_6/6)
 worn    56 ; sust(object_56_badg/56)
 writeln "The badge lowers your weight sufficiently enough to let you cross!"

_ _
 at      105 ; sust(room_105/105)
 eq      23 0 ; sust(game_flag_23/23)
 set     22 ; sust(game_flag_22/22)
 set     23 ; sust(game_flag_23/23)
 writeln "Enemy guards leap upon you from the undergrowth!"

_ _
 eq      22 250 ; sust(game_flag_22/22)
 writeln "Empire guards fall upon you with cries of victory!"
 score
 turns
 end

_ _
 notzero 22 ; sust(game_flag_22/22)
 minus   22 1 ; sust(game_flag_22/22)
 writeln "Enemy guards are closing in!"
