# Quann Tulla
© 1984 by Mike Smith and Gary Kelbrick

Genre: Science Fiction

YUID: 0l9ysoqk0sxn4pk8

IFDB: https://ifdb.org/viewgame?id=0l9ysoqk0sxn4pk8

## Info
Tools: UnQuill, The Inker, ngPAWS
- N Game fully tested?
- Y Missing images?
- Y Errors rendering images?
- Y Modern code for ngPAWS?
- 1 Edit txp (1) or sce (2)

## Test ngPAWS port

https://interactivefiction.gitlab.io/Quann_Tulla-1984-Mike_Smith-Gary_Kelbrick
